<?php
#####################################
# * @Author: lehieu008              #
# * @Email: lehieu008@gmail.com     #
# * @Copyright: 2010 - 2011         #
#####################################
if(!defined('BASEPATH'))exit('No direct script access allowed');
?>
<script type="text/javascript" src="<?php echo base_url() ;?>templates/editor/tiny/tiny_mce.js"></script>
<script type="text/javascript">
	tinyMCE.init({
		mode : "exact",
        elements : "txtContent, about_shop",
		theme : "advanced",
		skin : "o2k7",
        width : "350",
        height : "300",
		plugins : "autolink,lists,pagebreak,style,layer,table,save,advhr,advimage,advlink,emotions,iespell,insertdatetime,preview,media,searchreplace,print,contextmenu,paste,directionality,fullscreen,noneditable,visualchars,nonbreaking,xhtmlxtras,template,inlinepopups,autosave",
		theme_advanced_buttons1 : "newdocument,|,bold,italic,underline,strikethrough,|,justifyleft,justifycenter,justifyright,justifyfull,|,fontselect,fontsizeselect",
		theme_advanced_buttons2 : "pastetext,|,bullist,numlist,|,outdent,indent,blockquote,|,undo,redo,|,link,unlink,image,cleanup,|,forecolor,backcolor,emotions,fullscreen",
		theme_advanced_buttons3 : "tablecontrols,|,hr,|,sub,sup,|,charmap",
		theme_advanced_toolbar_location : "top",
		theme_advanced_toolbar_align : "left",
		theme_advanced_statusbar_location : "bottom",
		theme_advanced_resizing : true
	});
</script>