<?php
#****************************************#
# * @Author: lehieu008                   #
# * @Email: lehieu008@gmail.com          #
# * @Website: http://www.iscvietnam.net  #
# * @Copyright: 2008 - 2009              #
#****************************************#
#BEGIN: Mail
$lang['title_mail'] = 'Gởi mail';
$lang['to_mail'] = 'Người nhận';
$lang['list_mail'] = 'Danh sách Email';
$lang['from_mail'] = 'Người gởi';
$lang['subject_mail'] = 'Tiêu đề';
$lang['content_mail'] = 'Nội dung';
$lang['send_mail'] = 'Gởi mail';
$lang['list_tip_help_mail'] = '&bull Nhấp chọn E-Mail của thành viên mà bạn muốn gởi mail<br>&bull Bạn chỉ nên gởi tối đa tới 50 email/1 lần gởi';
$lang['to_mail_label_mail'] = 'Email người nhận';
$lang['from_mail_label_mail'] = 'Email người gởi';
$lang['subject_mail_label_mail'] = 'Tiêu đề';
$lang['txtcontent_label_mail'] = 'Nội dung';
$lang['useragen_mail'] = 'muabanoto24h.vn';
$lang['success_send'] = 'Email đã được gởi thành công';
#END Mail