<!--BEGIN: LEFT-->
<td bgcolor="#DFF3FE" width="201" valign="top">
    <table width="201" border="0" cellpadding="0" cellspacing="0">
        <?php $this->load->view('home/common/menu'); ?>
        <tr>
            <td class="tieude_trai"><?php echo $this->lang->line('title_info_left'); ?></td>
        </tr>
        <tr>
            <td bgcolor="#DFF3FE">
                <!--BEGIN: Popup Information-->
                <link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>templates/home/css/information.css" />
                <script language="javascript" src="<?php echo base_url(); ?>templates/home/js/information.js"></script>
                <!--[if ie 6]>
                	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>templates/home/css/ie6.css" />
                <![endif]-->
                <div id="hideshow" style="visibility: hidden;">
                	<div id="fade"></div>
                	<div class="popup_block">
                		<div class="popup">
                			<a onclick="hideInformation()" style="cursor:pointer;"><img src="<?php echo base_url(); ?>templates/home/images/icon_close_information.png" class="cntrl" title="Close"/></a>
                            <div id="DivInnerHTML"></div>
                		</div>
                	</div>
                </div>
                <!--END Popup Information-->
                <table width="201" border="0" cellpadding="0" cellspacing="0">
                    <tr>
                        <td width="30" height="30"><div><img src="<?php echo base_url(); ?>templates/home/images/icon_new.gif" style="width:25px; margin-left:5px;" /></div></td>
                        <td width="171" height="30" valign="middle"><div><a href="<?php echo base_url(); ?>templates/guide/data/maudangkycuahang.doc" class="menu"><?php echo $this->lang->line('guide_shop_info_left'); ?></a></div></td>
                    </tr>
                    <tr>
                        <td width="30" height="30"><div><img src="<?php echo base_url(); ?>templates/home/images/icon_ngoaite.jpg" /></div></td>
                        <td width="171" height="30" valign="middle"><div><a href="#Information" onclick="showInformation(2, '<?php echo base_url(); ?>', '<?php echo md5(date('dmY')); ?>')" class="menu"><?php echo $this->lang->line('exchange_info_left'); ?></a></div></td>
                    </tr>
                    <tr>
                        <td width="30" height="30"><div><img src="<?php echo base_url(); ?>templates/home/images/icon_chungkhoang.jpg" /></div></td>
                        <td width="171" height="30" valign="middle"><div><a href="#Information" onclick="showInformation(3, '<?php echo base_url(); ?>', '<?php echo md5(date('dmY')); ?>')" class="menu"><?php echo $this->lang->line('ck_info_left'); ?></a></div></td>
                    </tr>
                    <tr>
                        <td width="30" height="30"><div><img src="<?php echo base_url(); ?>templates/home/images/icon_thoitiet.jpg" /></div></td>
                        <td width="171" height="30" valign="middle"><div><a href="#Information" onclick="showInformation(4, '<?php echo base_url(); ?>', '<?php echo md5(date('dmY')); ?>')" class="menu"><?php echo $this->lang->line('weather_info_left'); ?></a></div></td>
                    </tr>
                    <tr>
                        <td width="30" height="30"><div><img src="<?php echo base_url(); ?>templates/home/images/icon_lichchieuphim.jpg" /></div></td>
                        <td width="171" height="30" valign="middle"><div><a href="#Information" onclick="showInformation(5, '<?php echo base_url(); ?>', '<?php echo md5(date('dmY')); ?>')" class="menu"><?php echo $this->lang->line('film_info_left'); ?></a></div></td>
                    </tr>
                </table>
            </td>
        </tr>
        <?php if(isset($advertisePage) && $advertisePage != 'account'){ ?>
        <tr>
            <td height="10"></td>
        </tr>
        <tr>
            <td class="tieude_trai"><?php echo $this->lang->line('title_advertise_left'); ?></td>
        </tr>
        <tr>
            <td height="5"></td>
        </tr>
        <?php $this->load->view('home/advertise/left'); ?>
        <?php } ?>
    </table>
</td>
<!--END LEFT-->