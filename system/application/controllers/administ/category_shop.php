<?php
#****************************************#
# * @Author: lehieu008                   #
# * @Email: lehieu008@gmail.com          #
# * @Website: http://www.iscvietnam.net  #
# * @Copyright: 2008 - 2009              #
#****************************************#
class Category_shop extends Controller
{
	function __construct()
	{
		parent::Controller();
		#BEGIN: CHECK LOGIN
		if(!$this->check->is_logined($this->session->userdata('sessionUserAdmin'), $this->session->userdata('sessionGroupAdmin')))
		{
			redirect(base_url().'administ', 'location');
			die();
		}
		#END CHECK LOGIN
		#Load language
		$this->lang->load('admin/common');
		$this->lang->load('admin/category');
		#Load model
		$this->load->model('category_shop_model');
	}
	
	function index()
	{
        #BEGIN: Delete
		if($this->input->post('checkone') && is_array($this->input->post('checkone')) && count($this->input->post('checkone')) > 0)
		{
            $idCategoryShop = implode(',', $this->input->post('checkone'));
            $this->category_shop_model->delete($idCategoryShop, "cas_id");
            $this->category_shop_model->delete($idCategoryShop, "cas_parent");
			redirect(base_url().trim(uri_string(), '/'), 'location');
		}
		#END Delete
        #Define url for $getVar
		$action = array('search', 'keyword', 'sort', 'by', 'status', 'id', 'page');
		$getVar = $this->uri->uri_to_assoc(3, $action);
        #BEGIN: Search & sort
		$where = "";
		$sort = 'cas_id';
		$by = 'DESC';
		$sortUrl = '';
		$pageSort = '';
		$pageUrl = '';
		$keyword = '';
		if($getVar['search'] != FALSE && trim($getVar['search']) != '' && $getVar['keyword'] != FALSE && trim($getVar['keyword']) != '')
		{
            $keyword = $this->filter->html($getVar['keyword']);
			switch(strtolower($getVar['search']))
			{
				case 'name':
				    $sortUrl .= '/search/name/keyword/'.$getVar['keyword'];
				    $pageUrl .= '/search/name/keyword/'.$getVar['keyword'];
				    $where .= "cas_name LIKE '%".$this->filter->injection_html($getVar['keyword'])."%'";
				    break;
			}
		}
		#If have sort
		if($getVar['sort'] != FALSE && trim($getVar['sort']) != '')
		{
			switch(strtolower($getVar['sort']))
			{
				case 'name':
				    $pageUrl .= '/sort/name';
				    $sort = "cas_name";
				    break;
                case 'parent':
				    $pageUrl .= '/sort/parent';
				    $sort = "cas_parent";
				    break;
				default:
				    $pageUrl .= '/sort/id';
				    $sort = "cas_id";
			}
			if($getVar['by'] != FALSE && strtolower($getVar['by']) == 'desc')
			{
                $pageUrl .= '/by/desc';
				$by = "DESC";
			}
			else
			{
                $pageUrl .= '/by/asc';
				$by = "ASC";
			}
		}
		#If have page
		if($getVar['page'] != FALSE && (int)$getVar['page'] > 0)
		{
			$start = (int)$getVar['page'];
			$pageSort .= '/page/'.$start;
		}
		else
		{
			$start = 0;
		}
		#END Search & sort
		#Keyword
		$data['keyword'] = $keyword;
		#BEGIN: Create link sort
		$data['sortUrl'] = base_url().'administ/category_shop'.$sortUrl.'/sort/';
		$data['pageSort'] = $pageSort;
		#END Create link sort
        #BEGIN: Status
		$statusUrl = $pageUrl.$pageSort;
		$data['statusUrl'] = base_url().'administ/category_shop'.$statusUrl;
		if($getVar['status'] != FALSE && trim($getVar['status']) != '' && $getVar['id'] != FALSE && (int)$getVar['id'] > 0)
		{
			switch(strtolower($getVar['status']))
			{
				case 'active':
				    $this->category_shop_model->update(array('cas_status'=>1), "cas_id = ".(int)$getVar['id']." AND cas_user = ".(int)$this->session->userdata('sessionUser'));
					break;
				case 'deactive':
				    $this->category_shop_model->update(array('cas_status'=>0), "cas_id = ".(int)$getVar['id']." AND cas_user = ".(int)$this->session->userdata('sessionUser'));
					break;
			}
			redirect($data['statusUrl'], 'location');
		}
		#END Status
		#BEGIN: Pagination
		$this->load->library('pagination');
		#Count total record
		$totalRecord = count($this->category_shop_model->fetch("cas_id", $where, "", ""));
        $config['base_url'] = base_url().'administ/category_shop'.$pageUrl.'/page/';
		$config['total_rows'] = $totalRecord;
		$config['per_page'] = Setting::settingOtherAdmin;
		$config['num_links'] = 5;
		$config['cur_page'] = $start;
		$this->pagination->initialize($config);
		$data['linkPage'] = $this->pagination->create_links();
		#END Pagination
		#sTT - So thu tu
		$data['sTT'] = $start + 1;
		#Fetch record
		$select = "*";
		$limit = Setting::settingOtherAdmin;
		$data['category'] = $this->category_shop_model->fetch($select, $where, $sort, $by, $start, $limit);
		#Load view
        $data['categoryParent'] = $this->category_shop_model->fetch("*", "");
		$this->load->view('admin/category_shop/defaults', $data);
	}
}