<?php
#****************************************#
# * @Author: lehieu008                   #
# * @Email: lehieu008@gmail.com          #
# * @Website: http://www.iscvietnam.net  #
# * @Copyright: 2008 - 2009              #
#****************************************#
#BEGIN: Login
$lang['username_login'] = 'Tài khoản';
$lang['password_login'] = 'Mật khẩu';
$lang['login_login'] = 'Đăng nhập';
$lang['valid_message_login'] = 'Sai tài khoản hoặc mật khẩu';
#END Login