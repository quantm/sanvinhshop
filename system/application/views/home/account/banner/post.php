<?php $this->load->view('home/common/header'); ?>
<?php $this->load->view('home/common/left'); ?>
<script language="javascript" src="<?php echo base_url(); ?>templates/home/js/check_email.js"></script>
<!--BEGIN: RIGHT-->
<td width="803" valign="top">
    <table width="803" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td background="<?php echo base_url(); ?>templates/home/images/bg_topkhung_ac.jpg" height="30">
                <div class="tile_modules"><?php echo 'THÊM BANNER QUẢNG CÁO'; ?></div>
            </td>
        </tr>
        <tr>
            <td background="<?php echo base_url(); ?>templates/home/images/bg_mainkhung_ac.jpg" valign="top" >
                <?php if($successPostBannerAccount == false){ ?>
                <div class="notepost_account">
                    <img src="<?php echo base_url(); ?>templates/home/images/note_post.gif" border="0" width="20" height="20" />&nbsp;
                    <b><font color="#FD5942"><?php echo $this->lang->line('note_help'); ?>:</font></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font color="#FF0000"><b>*</b></font>&nbsp;&nbsp;<?php echo $this->lang->line('must_input_help'); ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="<?php echo base_url(); ?>templates/home/images/help_post.gif" />&nbsp;&nbsp;<?php echo $this->lang->line('input_help'); ?>
                </div>
                <?php } ?>
                <table width="585" class="post_main" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td colspan="2" height="30" class="post_top"></td>
                    </tr>
                    <?php if($successPostBannerAccount == false){ ?>
                    <form name="frmPostBanner" method="post" enctype="multipart/form-data">
                    <tr>
                        <td width="150" valign="top" class="list_post"><font color="#FF0000"><b>*</b></font> <?php echo 'Tiêu đề banner'; ?>:</td>
                        <td>
                            <input type="text" name="title_banner" id="title_banner" value="<?php if(isset($title_banner)){echo $title_banner;} ?>" maxlength="80" class="input_formpost" onkeyup="BlockChar(this,'SpecialChar');" onfocus="ChangeStyle('title_banner',1)" onblur="ChangeStyle('title_banner',2)" />
                            <?php echo form_error('title_banner'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><?php echo 'Banner'; ?>:</td>
                        <td>
                            <input type="file" name="banner" id="banner" class="inputimage_formpost" />
                            <img src="<?php echo base_url(); ?>templates/home/images/help_post.gif" onmouseover="ddrivetip('<?php echo '&bull; W x H < 200 x 800 (px).<br>&bull; .JPG, .GIF, .PNG, .SWF'; ?>',270,'#F0F8FF');" onmouseout="hideddrivetip();" class="img_helppost" />
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><font color="#FF0000"><b>*</b></font> <?php echo 'Link'; ?>:</td>
                        <td>
                            <input type="text" name="link_banner" id="link_banner" value="<?php if(isset($link_banner)){echo $link_banner;} ?>" maxlength="80" class="input_formpost" onfocus="ChangeStyle('link_banner',1)" onblur="ChangeStyle('link_banner',2)" />
                            <?php echo form_error('link_banner'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><font color="#FF0000"><b>*</b></font> <?php echo 'Vị trí'; ?>:</td>
                        <td>
                            <select name="position_banner" id="position_banner" class="selectcategory_formpost">
                                <option value="3" <?php if(isset($position_banner) && $position_banner == 3){echo 'selected="selected"';} ?>>Left</option>
                                <option value="4" <?php if(isset($position_banner) && $position_banner == 4){echo 'selected="selected"';} ?>>Right</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><?php echo 'Nhà quảng cáo'; ?>:</td>
                        <td>
                            <input type="text" name="fullname_banner" id="title_banner" value="<?php if(isset($fullname_banner)){echo $fullname_banner;} ?>" maxlength="80" class="input_formpost" onkeyup="" onfocus="ChangeStyle('fullname_banner',1)" onblur="ChangeStyle('fullname_banner',2)" />
                            <?php echo form_error('fullname_banner'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><?php echo 'Email'; ?>:</td>
                        <td>
                            <input type="text" name="email_banner" id="email_banner" value="<?php if(isset($email_banner)){echo $email_banner;} ?>" maxlength="80" class="input_formpost" onkeyup="" onfocus="ChangeStyle('email_banner',1)" onblur="ChangeStyle('email_banner',2)" />
                            <?php echo form_error('email_banner'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><?php echo 'Thứ tự'; ?>:</td>
                        <td>
                            <input type="text" name="order_banner" id="order_banner" value="<?php if(isset($order_banner)){echo $order_banner;} ?>" maxlength="80" class="input_formpost" style="width:40px; font-weight:bold; text-align:center;" onkeyup="BlockChar(this,'SpecialChar')" onfocus="ChangeStyle('order_banner',1)" onblur="ChangeStyle('order_banner',2)" />
                            <?php echo form_error('order_banner'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" class="list_post"><?php echo 'Kích hoạt'; ?>:</td>
                        <td>
                            <input type="checkbox" name="status_banner" id="status_banner" value="1" <?php if(isset($status_banner) && (int)$status_banner == 1){echo 'checked="checked"';} ?> />
                        </td>
                    </tr>
                    <?php if(isset($imageCaptchaPostBannerAccount)){ ?>
                    <tr>
                        <td width="150" valign="middle" class="list_post"><font color="#FF0000"><b>*</b></font> <?php echo $this->lang->line('captcha_main'); ?>:</td>
                        <td align="left" style="padding-top:7px;">
                            <img src="<?php echo base_url().$imageCaptchaPostBannerAccount; ?>" width="151" height="30" /><br />
                            <input type="text" name="captcha_banner" id="captcha_banner" value="" maxlength="10" class="inputcaptcha_form" onfocus="ChangeStyle('captcha_banner',1);" onblur="ChangeStyle('captcha_banner',2);" />
                            <?php echo form_error('captcha_banner'); ?>
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td width="150"></td>
                        <td height="30" valign="bottom" align="center">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3" height="25"></td>
                                </tr>
                                <tr>
                                    <td><input type="submit" onclick="" name="submit_postshopcategory" value="<?php echo 'Đồng ý'; ?>" class="button_form" /></td>
                                    <td width="15"></td>
                                    <td><input type="button" name="cancle_postshopcategory" value="<?php echo 'Hủy bỏ'; ?>" onclick="ActionLink('<?php echo base_url(); ?>account/banner')" class="button_form" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <input type="hidden" name="isPostBannerAccount" value="1" />
                    </form>
                    <?php }else{ ?>
                    <tr>
                        <td class="success_post" style="padding-top: 10px;">
                            <meta http-equiv=refresh content="5; url=<?php echo base_url(); ?>account/banner/post">
                            <?php echo 'Banner quảng cáo đã được thêm thành công'; ?>
						</td>
					</tr>
                    <?php } ?>
                    <tr>
                        <td colspan="2" height="30" class="post_bottom"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="<?php echo base_url(); ?>templates/home/images/bg_bottomkhung_ac.jpg" height="16" ></td>
        </tr>
    </table>	
</td>					
<!--END RIGHT-->
<?php $this->load->view('home/common/footer'); ?>