<?php
#****************************************#
# * @Author: lehieu008                   #
# * @Email: lehieu008@gmail.com          #
# * @Website: http://www.iscvietnam.net  #
# * @Copyright: 2008 - 2009              #
#****************************************#
#BEGIN: Defaults
$lang['title_defaults'] = 'Giỏ hàng';
$lang['name_search_defaults'] = 'Tên sản phẩm';
$lang['cost_search_defaults'] = 'Giá bán';
$lang['buyer_search_defaults'] = 'Người mua';
$lang['buydate_search_defaults'] = 'Ngày mua';
$lang['process_search_defaults'] = 'Đã xử lý';
$lang['notprocess_search_defaults'] = 'Chưa xử lý';
$lang['email_tip_defaults'] = '<b>Email:</b>';
#END Defaults