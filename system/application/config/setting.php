<?php
if(!defined('BASEPATH'))exit('No direct script access allowed');
/**
 *Class Setting: Luu tat ca cac cau hinh
**/
class Setting
{
#Thong tin website
const settingTitle = 'Website Thương Mại Điện Tử | Geate';
const settingDescr = 'source, code, ma nguon, shop, geate';
const settingKeyword = 'source, code, ma nguon, shop, geate';
const settingEmail_1 = 'lehieu008@gmail.com';
const settingEmail_2 = 'lehieu008@gmail.com';
const settingAddress_1 = 'Hồ Chí Minh';
const settingAddress_2 = 'Hồ Chí Minh';
const settingPhone = '0985687885';
const settingMobile = '0985687885';
const settingYahoo_1 = 'lehieu008';
const settingYahoo_2 = 'lehieu008';
const settingSkype_1 = 'lehieu008';
const settingSkype_2 = 'lehieu008';
#Cau hinh chung
const settingTimePost = 15;
const settingLockAccount = 180;
const settingTimeSession = 900;
const settingTimeCache = 15;
const settingStopSite = '0';
const settingActiveAccount = '0';
const settingStopRegister = '0';
const settingStopRegisterVip = '0';
const settingStopRegisterShop = '1';
const settingExchange = 20800;
#Hien thi san pham
const settingProductNew_Home = 16;
const settingProductReliable_Home = 16;
const settingProductNew_Category = 20;
const settingProductReliable_Category = 16;
const settingProductSaleoff = 30;
const settingProductNew_Top = 10;
const settingProductSaleoff_Top = 10;
const settingProductBuyest_Top = 10;
const settingProductUser = 15;
const settingProductCategory = 10;
#Hien thi rao vat
const settingAdsNew_Home = 15;
const settingAdsReliable_Category = 20;
const settingAdsNew_Category = 40;
const settingAdsShop = 60;
const settingAdsReliable_Top = 15;
const settingAdsNew_Top = 10;
const settingAdsViewest_Top = 15;
const settingAdsShop_Top = 10;
const settingAdsUser = 10;
const settingAdsCategory = 5;
#Hien thi tin tuyen dung, tim viec
const settingJobInterest = 20;
const settingJobNew = 40;
const settingJob24Gio_J_Top = 10;
const settingJob24Gio_E_Top = 10;
const settingJobUser = 5;
const settingJobField = 5;
#Hien thi cua hang
const settingShopInterest = 12;
const settingShopInterest_Category = 12;
const settingShopNew_Category = 20;
const settingShopSaleoff = 30;
const settingShopNew_Top = 10;
const settingShopSaleoff_Top = 10;
const settingShopProductest_Top = 15;
#Hien thi shopping
const settingShoppingInterest_Home = 12;
const settingShoppingNew_Home = 20;
const settingShoppingSaleoff_Home = 20;
const settingShoppingNew_List = 20;
const settingShoppingSaleoff_List = 20;
const settingShoppingAdsNew = 40;
const settingShoppingProductNew_Top = 10;
const settingShoppingAdsNew_Top = 10;
const settingShoppingSearch = 20;
#Hien thi tim kiem
const settingSearchProduct = 20;
const settingSearchAds = 40;
const settingSearchJob = 40;
const settingSearchShop = 20;
#Cau hinh hien thi khac
const settingOtherAccount = 20;
const settingOtherAdmin = 30;
const settingOtherShowcart = 20;
}