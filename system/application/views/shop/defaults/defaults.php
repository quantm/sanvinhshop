<?php $this->load->view('shop/common/header'); ?>
<?php $this->load->view('shop/common/left'); ?>
<?php if(isset($siteGlobal)){ ?>
<script language="javascript" src="<?php echo base_url(); ?>templates/shop/<?php echo $siteGlobal->sho_style; ?>/js/jquery.js"></script>
<script language="javascript" src="<?php echo base_url(); ?>templates/shop/<?php echo $siteGlobal->sho_style; ?>/js/ajax.js"></script>
<!--BEGIN: Center-->
<td width="602" valign="top" align="center">
<div id="DivContent">
    <?php $this->load->view('home/advertise/top_shop'); ?>
    <?php $this->load->view('shop/common/top'); ?>
    <table width="594" class="table_module" style="margin-top:5px;" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td height="28" class="title_module"><?php echo $this->lang->line('title_interest_detail_defaults'); ?></td>
        </tr>
        <tr>
            <td class="main_module" valign="top" align="center" id="DivInterest"></td>
        </tr>
        <tr>
            <td height="10" class="bottom_module"></td>
        </tr>
    </table>
    <script>getProduct(1, 'DivInterest', 'DivInterestBox_', '<?php echo $this->hash->create($siteGlobal->sho_link, $this->input->user_agent(), 'sha256md5'); ?>', '<?php echo $siteGlobal->sho_link; ?>', '<?php echo $siteGlobal->sho_style; ?>', '<?php echo base_url(); ?>');</script>
    <table width="594" class="table_module" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td height="28" class="title_module"><a class="menu_3" href="<?php echo base_url().$siteGlobal->sho_link; ?>/product"><?php echo $this->lang->line('title_new_detail_defaults'); ?> >></a></td>
        </tr>
        <tr>
            <td class="main_module" valign="top" align="center" id="DivNew"></td>
        </tr>
        <tr>
            <td height="10" class="bottom_module"></td>
        </tr>
    </table>
    <script>getProduct(2, 'DivNew', 'DivNewBox_', '<?php echo $this->hash->create($siteGlobal->sho_link, $this->input->user_agent(), 'sha256md5'); ?>', '<?php echo $siteGlobal->sho_link; ?>', '<?php echo $siteGlobal->sho_style; ?>', '<?php echo base_url(); ?>');</script>
    <table id="TableSaleoff" width="594" class="table_module" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td height="28" class="title_module"><a class="menu_3" href="<?php echo base_url().$siteGlobal->sho_link; ?>/product/saleoff"><?php echo $this->lang->line('title_saleoff_detail_defaults'); ?> >></a></td>
        </tr>
        <tr>
            <td class="main_module" valign="top" align="center" id="DivSaleoff"></td>
        </tr>
        <tr>
            <td height="10" class="bottom_module"></td>
        </tr>
    </table>
    <script>getProduct(3, 'DivSaleoff', 'DivSaleoffBox_', '<?php echo $this->hash->create($siteGlobal->sho_link, $this->input->user_agent(), 'sha256md5'); ?>', '<?php echo $siteGlobal->sho_link; ?>', '<?php echo $siteGlobal->sho_style; ?>', '<?php echo base_url(); ?>');</script>
    
    <?php if(isset($category) && isset($product)): ?>
        <?php foreach($category as $categoryArray): ?>
            <table width="594" class="table_module" style="margin-top:10px;" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td height="28" class="title_module" style="text-transform:uppercase;"><?php echo $categoryArray->cas_name; ?></td>
                </tr>
                <tr>
                    <td class="main_module" valign="top" align="center" id="DivCategory">
                        <?php foreach($product as $productArray): ?>
                            <?php if($productArray->pro_category_shop == $categoryArray->cas_id): ?>
                                <div onmouseout="ChangeStyleBox('DivCategoryBox_<?php echo $categoryArray->cas_id; ?>',2)" onmouseover="ChangeStyleBox('DivCategoryBox_<?php echo $categoryArray->cas_id; ?>',1)" id="DivCategoryBox_<?php echo $categoryArray->cas_id; ?>" class="showbox_1" style="border: 1px solid rgb(212, 237, 255);">
                                    <a title="<?php echo $productArray->pro_name; ?>" href="<?php echo base_url().$siteGlobal->sho_link; ?>/product/detail/<?php echo $productArray->pro_id; ?>" class="menu_1">
                                        <img class="image_showbox_1" src="<?php echo base_url(); ?>media/images/product/<?php echo $productArray->pro_dir; ?>/<?php echo show_thumbnail($productArray->pro_dir, $productArray->pro_image, 2); ?>">
                                        <div class="name_showbox_1">
                                            <span id="DivName_DivCategoryBox_<?php echo $categoryArray->cas_id; ?>"><?php echo sub($productArray->pro_name, 30); ?></span>
                                        </div>
                                    </a>
                                    <div class="cost_showbox">
                                        <span id="DivCost_DivCategoryBox_<?php echo $categoryArray->cas_id; ?>"></span>&nbsp;<?php echo $productArray->pro_currency; ?>
                                        <script>FormatCost('<?php echo $productArray->pro_cost; ?>', 'DivCost_DivCategoryBox_<?php echo $categoryArray->cas_id; ?>');</script>
                                    </div>
                                </div>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    </td>
                </tr>
                <tr>
                    <td height="10" class="bottom_module"></td>
                </tr>
            </table>
        <?php endforeach; ?>
    <?php endif; ?>
    
</div>
<div id="DivSearch">
    <?php $this->load->view('shop/common/search'); ?>
</div>
<script>OpenSearch(0);</script>
</td>
<!--END Center-->
<?php } ?>
<?php $this->load->view('shop/common/right'); ?>
<?php $this->load->view('shop/common/footer'); ?>