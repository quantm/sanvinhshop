<?php if(isset($siteGlobal)){ ?>
<!--BEGIN: Left-->
<td width="201" valign="top">
	<?php if(count($categoryShop) > 0): ?>
	    <table width="201" border="0" cellpadding="0" cellspacing="0">
	        <tr>
	            <td class="title"><?php echo 'DANH MỤC'; ?></td>
	        </tr>
	        <tr>
	            <td height="5" style="background:#DFF3FE;"></td>
	        </tr>
	        <tr>
	        	<td class="hMenu" style="padding-bottom:3px; padding-top: 0px; background:#DFF3FE;">
	        		<?php echo html_category_shop($categoryShop, 0, 0, 0, $menuShopSelected, $menuShopType); ?>
				</td>
			</tr>
	    </table>
    <?php endif; ?>
    <table width="201" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td class="title"><?php echo $this->lang->line('title_advertise_left_detail_global'); ?></td>
        </tr>
        <tr>
            <td height="5"></td>
        </tr>
        <?php $this->load->view('home/advertise/left'); ?>
    </table>
</td>
<!--END Left-->
<?php } ?>