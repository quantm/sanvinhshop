-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jul 19, 2012 at 10:12 PM
-- Server version: 5.5.16
-- PHP Version: 5.2.9-2

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `project_shop`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_ads`
--

CREATE TABLE IF NOT EXISTS `tbtt_ads` (
  `ads_id` int(10) NOT NULL AUTO_INCREMENT,
  `ads_title` varchar(100) NOT NULL,
  `ads_descr` varchar(100) NOT NULL,
  `ads_province` int(3) NOT NULL,
  `ads_category` int(3) NOT NULL,
  `ads_category_shop` int(11) NOT NULL,
  `ads_begindate` int(11) NOT NULL,
  `ads_enddate` int(11) NOT NULL,
  `ads_detail` text NOT NULL,
  `ads_image` varchar(150) NOT NULL,
  `ads_dir` varchar(150) NOT NULL,
  `ads_user` int(10) NOT NULL,
  `ads_poster` varchar(100) NOT NULL,
  `ads_address` varchar(100) NOT NULL,
  `ads_phone` varchar(20) NOT NULL,
  `ads_mobile` varchar(20) NOT NULL,
  `ads_email` varchar(50) NOT NULL,
  `ads_yahoo` varchar(50) NOT NULL,
  `ads_skype` varchar(50) NOT NULL,
  `ads_status` int(1) NOT NULL,
  `ads_view` int(10) NOT NULL,
  `ads_comment` int(5) NOT NULL,
  `ads_reliable` int(1) NOT NULL,
  `ads_is_shop` int(1) NOT NULL,
  PRIMARY KEY (`ads_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1436 ;

--
-- Dumping data for table `tbtt_ads`
--

INSERT INTO `tbtt_ads` (`ads_id`, `ads_title`, `ads_descr`, `ads_province`, `ads_category`, `ads_category_shop`, `ads_begindate`, `ads_enddate`, `ads_detail`, `ads_image`, `ads_dir`, `ads_user`, `ads_poster`, `ads_address`, `ads_phone`, `ads_mobile`, `ads_email`, `ads_yahoo`, `ads_skype`, `ads_status`, `ads_view`, `ads_comment`, `ads_reliable`, `ads_is_shop`) VALUES
(1423, 'Chào mừng các bạn đến với Thị Trường 37', '', 84, 432, 0, 1323104400, 1325782800, '<p>Ch&agrave;o mừng c&aacute;c bạn đến với Thị Trường 37. Mong được hợp t&aacute;c với tất cả c&aacute;c bạn!</p>', 'none.gif', 'default', 547, 'Phan Minh Châu', '', '', '0904683465', 'administrator@thitruong37.com', '', '', 1, 21, 0, 0, 0),
(1426, 'Bán 319M2(10,5x30) đã có nhà 1,5Tầng lối 2 đường Hồng Bàng giá 2tỷ ./', '', 82, 425, 0, 1323968400, 1355590800, '<p><strong><span style="color: black;"><span style="color: black;"><span style="font-size: small;">Vị tr&iacute; khối T&acirc;n Phong - L&ecirc; Mao - TP Vinh (lối 2 đường Hồng B&agrave;ng,tam gi&aacute;c quỷ đi lại 70m rẽ v&agrave;o ng&otilde;), gần chợ C4, Quang Trung.Đất rộng 320m2 c&oacute; b&igrave;a đỏ ch&iacute;nh chủ. K&iacute;ch thước: 10,5 m x 30m(Nở hậu sau). Hiện tại đ&atilde; c&oacute; nh&agrave; 1,5 tầng rộng 80m2 đầy đủ tiện nghi.Nh&agrave; hướng nam chếch t&acirc;y,c&oacute; thể chia th&agrave;nh 2 l&ocirc; đất để b&aacute;n(Rộng 5m v&agrave; rộng 6m,d&agrave;i 30m).Trong lối chỉ c&oacute; 3 ng&ocirc;i nh&agrave; n&ecirc;n rất y&ecirc;n tĩnh,vườn rộng.V&igrave; đang cần b&aacute;n gấp n&ecirc;n để tr&aacute;nh mất thời gian cho kh&aacute;ch h&agrave;ng hiện trạng đường trước mặt chỉ đủ cho d&ograve;ng xe matiz hay Yaris v&agrave;o được,từ ngo&agrave;i đường HB đi v&agrave;o nh&agrave; khoảng 50m.Mong muốn li&ecirc;n hệ với kh&aacute;ch c&oacute; nhu cầu mua thật sự.Gi&aacute; b&aacute;n hữu nghị 2tỷ.Cần b&aacute;n gấp!<br /> Li&ecirc;n hệ: Mr Tuấn 0972 745 777 &ndash; 0943 352 356./</span></span></span></strong></p>', 'none.gif', 'default', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 6, 0, 1, 1),
(1427, 'Bán đất tại K.Tân Tiến – P.Hưng Bình – TP Vinh./', '', 82, 433, 0, 1323968400, 1333036800, '<p><strong><span style="font-size: small;">T&ocirc;i c&oacute; nhu cầu b&aacute;n 1 l&ocirc; đất diện t&iacute;ch 130m2 tại K.T&acirc;n Tiến &ndash; P.Hưng B&igrave;nh ( Tam gi&aacute;c quỷ đi v&agrave;o đường Kim Đồng rẽ ng&otilde; thứ 4 b&ecirc;n tay tr&aacute;i, bia hơi 19 Kim Đồng đi qu&aacute; 1 t&yacute;, rẽ v&agrave;o đi hết đường rẽ phải l&agrave; thấy mảnh đất b&ecirc;n phải ) k&iacute;ch thước 5,2m x 25m, hướng t&acirc;y nam tại phường Hưng B&igrave;nh, đường trước mặt rộng 5m. Mảnh đất th&ocirc;ng tho&aacute;ng thuận tiện đi lại.<br /> Sổ đỏ ch&iacute;nh chủ, gi&aacute; b&aacute;n thương lượng.<br /> Ai thật sự c&oacute; nhu cầu li&ecirc;n hệ với Mr Tuấn qua số điện thoại 0972 745 777 &ndash; 0943 352 356 ./</span></strong></p>', 'none.gif', 'default', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 5, 0, 1, 1),
(1428, '101m2 đất ở K.Hiếu Hạp - P.Nghi Thu - TX.Cửa Lò', '', 82, 433, 0, 1323968400, 1330102800, '<p><strong><span style="font-size: small;">T&ocirc;i đang cần b&aacute;n 1 L&ocirc; đất trong d&acirc;n ở khối Hiếu Hạp &ndash; p Nghi Thu &ndash; TX Cửa L&ograve; k&iacute;ch thước 8m x 13m hướng bắc chếch t&acirc;y,nở hậu,đường trước mặt 12m c&oacute; gi&aacute; b&aacute;n mảnh đất n&agrave;y l&agrave; 520 triệu, sổ đỏ ch&iacute;nh chủ,ở khu vực đ&ocirc;ng d&acirc;n cư, trung t&acirc;m. Ai thật sự c&oacute; nhu mua th&igrave; li&ecirc;n hệ với Mr Tuấn 0972 745 777 &ndash; 0943 352 356.</span></strong></p>', 'none.gif', 'default', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 6, 0, 1, 1),
(1429, 'Đất Trung tâm Cửa Lò thích hợp để xây Khách sạn./', '', 82, 433, 0, 1323968400, 1330016400, '<p><span style="font-size: medium;">T&ocirc;i c&oacute; 2 mảnh đất ở Trung t&acirc;m TX Cửa L&ograve; th&iacute;ch hợp để XD Kh&aacute;ch sạn:<br /> 1.Sau lưng kh&aacute;ch sạn Sun and Sea S=130m2( 6,5m x 20m ) gi&aacute; b&aacute;n 11 triệu/m2.<br /> 2.Mặt đường ngang số 2 S=261m2( 13m x 20m ) gi&aacute; b&aacute;n 8 triệu/m2.<br /> Ai c&oacute; nhu cầu mua đất để ở hoặc XD kh&aacute;ch sạn th&igrave; li&ecirc;n hệ với t&ocirc;i qua số đt 0972 745 777.<br /> Cảm ơn đ&atilde; đọc tin!</span></p>', 'none.gif', 'default', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 7, 0, 1, 1),
(1430, 'Bán đất nghi phú cạnh Dự án Tecco Nghi Phú./', '', 82, 433, 0, 1323968400, 1356886800, '<p>154m2 x&oacute;m 18 Nghi Ph&uacute; cạnh dự &aacute;n TECCO - Nghi Ph&uacute;.</p>\n<p>&nbsp;</p>\n<p>T&ocirc;i c&oacute; nhu cầu cần b&aacute;n 1 l&ocirc; đất k&iacute;ch thước 9,5m x 14m ( rộng sau 10,86 nở hậu ) hướng đ&ocirc;ng ở x&oacute;m 18 Nghi Ph&uacute; cạnh dự &aacute;n TECCO - Nghi Ph&uacute; ( x&oacute;m 18 ) cạnh đại lộ LeeNin 32, thuộc v&ugrave;ng quy hoạch gần đường 72m, đất 2 mặt đường. Đất ch&iacute;nh chủ sổ đỏ hợp ph&aacute;p. Gi&aacute; b&aacute;n 12tr/m2 ai c&oacute; nhu cầu mua xin li&ecirc;n hệ sdt : 0972.745.777 ( tuấn )</p>', 'none.gif', 'default', 569, 'Trần Anh Tuấn', '', '', '0972.745.777', 'tuantran.qh590@yahoo.com', '', '', 1, 5, 0, 1, 1),
(1431, 'Cho thuê căn hộ Chung cư', '', 82, 433, 13, 1323968400, 1355936400, '<p>T&ocirc;i c&oacute; nhu cầu cho thu&ecirc; căn hộ số 616 - khu chung cư T&acirc;n Ph&uacute;c thuộc phường Vinh T&acirc;n( C&ocirc;ng vi&ecirc;n Trung T&acirc;m-đường L&ecirc; Mao mở rộng đi xuống) gần chợ, bệnh viện, trường học cấp 1 v&agrave; 2 v&agrave; c&ocirc;ng vi&ecirc;n trung t&acirc;m.Diện t&iacute;ch 63m2 bao gồm 1 p ngủ, 1 p kh&aacute;ch( ph&ograve;ng kh&aacute;ch c&oacute; thể ngăn để th&ecirc;m 1 ph&ograve;ng ngủ), bếp v&agrave; wc.Gi&aacute; 3,3triệu đồng /th&aacute;ng .L&agrave;m hợp đồng v&agrave; thoả thuận trực tiếp.Li&ecirc;n hệ Mr Tuấn 0972 745 777 &ndash; 0977 624 678./</p>\n<p>Cảm ơn đ&atilde; đọc tin!!!</p>', 'none.gif', 'default', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 5, 0, 1, 1),
(1432, 'Bán đất K.5 - P.Lê Lợi - TP Vinh./', '', 82, 433, 0, 1324054800, 1355677200, '<p><span class="xanhxam">T&ocirc;i c&oacute; nhu cầu muốn b&aacute;n 100,7 m2 đất tại K5 &ndash; P.L&ecirc; Lợi &ndash; TP Vinh. Rộng trước b&aacute;m đường 5,2m chiều s&acirc;u 20m Hướng ch&iacute;nh nam.Đường d&acirc;n cư rộng 5m. Sổ đỏ ch&iacute;nh chủ. Ai c&oacute; nhu cầu mua li&ecirc;n hệ Mr Tuấn 0972 745 777 - 0943 352 356. Cảm ơn đ&atilde; đọc tin! </span></p>', 'none.gif', 'default', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 1, 0, 0, 1),
(1433, 'Khuyến Mãi Hấp Dẫn Máy Tính Laptop Tại thaihoaipc.com', '', 84, 418, 0, 1324054800, 1326733200, '<p style="text-align: justify;"><span style="background-color: #ffffff; color: #ffff00;"><strong>Th<span style="color: #ff0000;">&aacute;i Ho&agrave;i computer khuyến m&atilde;i đặc biệt đ&acirc;y (thaihoaipc.com)</span></strong></span><br /><span style="background-color: #ffffff; color: #ff0000;"><strong>Miễn 100% ph&iacute; bảo tr&igrave;, v&agrave; c&agrave;i đặt !</strong></span><br /><span style="background-color: #ffffff; color: #ff0000;"><strong>C&Agrave;I ĐẶT HĐH: Windows XP (SP2, SP3, Anh, Việt 32bit-64bit)</strong></span><br /><span style="background-color: #ffffff; color: #ff0000;"><strong>Windows Vista (Ultimate, Home Basic, Home Premium, B&uacute;iness, SP1 2009, SP2 32bit-64bit, Việt h&oacute;a)</strong></span><br /><span style="background-color: #ffffff; color: #ff0000;"><strong>ĐẶC BIỆT: C&agrave;i c&aacute;c phần mềm tiện &iacute;ch, Game, Nhạc theo y&ecirc;u cầu qu&yacute; kh&aacute;ch</strong></span><br /><span style="background-color: #ffffff; color: #ff0000;"><strong>Chỉ &aacute;p dụng khi qu&yacute; kh&aacute;ch h&agrave;ng mang c&aacute;c thiết bị của m&igrave;nh tới trụ sở c&ocirc;ng ty : Số 01 - Ng&otilde; 47 - Đường H&agrave; Huy Tập - TP.Vinh - NA . </strong></span><br /><span style="background-color: #ffffff; color: #ff0000;"><strong>Qu&yacute; kh&aacute;ch h&agrave;ng sẽ được giảm tới 30% ph&iacute; dịch vụ c&agrave;i đặt, sửa chữa, n&acirc;ng cấp, bảo tr&igrave; m&aacute;y t&iacute;nh v&agrave; hệ thống mạng tận nơi.</strong></span><br /><span style="background-color: #ffffff; color: #ff0000;"><strong>(Thời gian &aacute;p dụng: từ ng&agrave;y 07/12 /2011 đến 07 /01/2012 )</strong></span></p>', 'none.gif', 'default', 568, 'Thái Lương Hoài', '', '', '0945911088', 'thaihoaipc@gmail.com', '', '', 1, 3, 0, 0, 0),
(1434, 'cần mua samsung t919', '', 84, 417, 0, 1324054800, 1326733200, '<p>ai c&oacute; lh em qua sdt 01644366971 nha.cho em gi&aacute; cả lu&ocirc;n ạ</p>', 'none.gif', 'default', 0, '', '', '', '01644366971', '', '', '', 1, 3, 0, 0, 0),
(1435, 'Tìm kiêm đối tác kinh doanh các sản phẩm của công ty', '', 84, 423, 0, 1324054800, 1355677200, '<p>Hiện tại do nhu cầu ph&aacute;t triển mạnh mẽ của c&ocirc;ng ty n&ecirc;n ch&uacute;ng t&ocirc;i đang t&igrave;m kiếm đối t&aacute;c kinh doanh, ph&acirc;n phối sản phẩm của c&ocirc;ng ty ch&uacute;ng t&ocirc;i, th&ocirc;ng tin chi tiết xem tại <a href="http://www.dautunhanh.tk">www.dautunhanh.tk</a></p>\n<p>rất vui được hợp t&aacute;c&nbsp;</p>', 'none.gif', 'default', 573, 'Hồ Sỹ Châu', '', '', '0917310655', 'chauhs87@gmail.com', '', '', 1, 2, 0, 1, 1),
(1395, 'KIA 1.4 TẤN THÙNG KÍN', 'XE Sản Xuất 2005', 79, 232, 0, 1308502800, 1311094800, '[blue]Cần bán xe KIA 1.4 TẤN THÙNG KÍN, MÀU XANH, ĐỜI 2005.[/blue]\nXE GIA ĐÌNH SỬ DỤNG KỸ. \nQUÝ KHÁCH CÓ NHU CẦU VUI LÒNG LIÊN HỆ: MR TỨ: 0932 088 838', '367ff5aab165ece509fd5cf9e447c999.jpg', '20062011', 535, 'Bùi Xuân Tứ', 'Biên Hòa - Đồng Nai', '0932088838', '', 'xuantu59@yahoo.com.vn', 'xuantu59', '', 1, 11, 0, 1, 1),
(1392, 'Isuzu 1.9T thùng dài 4.7m !', 'Xe tải', 79, 302, 0, 1307552400, 1310403600, 'Cần bán Isuzu 1.9T, đời 2007, thùng kín, dài 4.7m, xe đẹp, nước sơn zin, máy lạnh cực lạnh, xe chạy lợi dầu, phù hợp với các tuyến đường nội thành, giá 375tr. Để biết thêm thông tin chi tiết xin vui lòng liên lạc về số điện thoại trên.', 'none.gif', 'default', 538, 'Phạm Ngọc Thạch', '441/442 - Tân Biên - Biên Hòa', '0933009924', '0932733466', 'phamtran2223@yahoo.com', 'phamtran2223', '', 1, 20, 0, 1, 1),
(1393, 'Nhận hướng dẫn kỹ năng thực hành lái ô tô !', 'Dạy kèm theo nhu cầu', 79, 412, 0, 1307552400, 1312131600, 'Chúng tôi có một đội ngũ chuyên viên hướng dẫn kỹ năng thực hành lái xe ô tô, với nhiều năm kinh nghiệm. Đảm bảo thực hành đậu 100&permil;, ngoài ra chúng tôi còn nhận hướng dẫn theo nhu cầu của từng học viên. Mọi chi tiết xin liên hệ trực tiếp với chúng tôi.', 'none.gif', 'default', 538, 'Phạm Ngọc Thạch', '441/442 - Tân Biên - Biên Hòa', '0933009924', '0932733466', 'phamtran2223@yahoo.com', 'phamtran2223', '', 1, 18, 0, 1, 1),
(1394, 'Spark van đã lên hàng ghế sau cần đổi chủ', 'Chevrolet Spark Van', 79, 251, 0, 1307552400, 1310662800, 'Cần bán Chvrolet Spark Van bán tải, đã lên 05 ghế, màu xanh cốm, xe đẹp, máy móc hoàn hảo. Mọi chi tiết xin liên hệ số điện thoại trên. Xin chân thành cảm ơn!', 'none.gif', 'default', 538, 'Phạm Ngọc Thạch', '441/442 - Tân Biên - Biên Hòa', '0933009924', '', 'phamtran2223@yahoo.com', 'phamtran2223', '', 1, 23, 0, 1, 1),
(1396, 'HONDA CIVIC', 'Xe đời cuối 2007', 79, 313, 0, 1308502800, 1311094800, 'Bán xe Honda Civic 2.0 số tự động, màu đen đời cuối 2007.\nQuý khách có nhu cầu vui lòng liên hệ: Mr Hùng 0949 434 202', 'c5f8cb55b2e4f752bc39396590370c09.jpg', '20062011', 535, 'Mr Hùng', 'Biên Hòa - Đồng Nai', '0949434202', '', 'xuanhung79@yahoo.com', 'hung79', '', 1, 12, 0, 1, 1),
(1398, 'Cần bán hyundai 1.25t !', 'xe tải', 81, 232, 0, 1308848400, 1311440400, 'Cần bán hyundai 1.25t, thùng kèo mui bạt, biển số Đồng Nai, xe chưa cấn đụng, đời 2008, máy khỏe, giá 285tr. Mọi chi tiết xin liên hệ : 0933 00.99.24', 'none.gif', 'default', 538, 'Phạm Ngọc Thạch', '441/442 - Tân Biên - Biên Hòa', '0933009924', '', 'phamtran2223@yahoo.com', 'phamtran2223', '', 1, 10, 0, 1, 1),
(1399, 'Bán xe chevrolet Vivan CDX AT', 'Xe đời 2008, màu bạc', 81, 229, 0, 1309107600, 1343322000, 'Can tien bán gâp xe còn moi mâm duc ghế da tháng ABS túi khí , gương điện , xe gia đình ít sử dụng mới đi 13 .000km, không va quệt sơn zin Động cơ 2.0', '519eb4d97bd197a2d124c7d35e4b4e45.jpg', '27062011', 535, 'Trần Thị Tuyết', 'Thủ Đức - Tp. Hcm', '0982286051', '', 'trantuyet@yahoo.com', '', '', 1, 12, 0, 1, 1),
(1400, 'GM CHEVROLET DONG NAI', 'XE MOI 100', 81, 216, 0, 1309971600, 1325264400, 'Chuyên kinh doanh các loại xe : CAPTIVA, LACETTI, CRUZE, GENTRA, VIVANT và  SPARK.\n Bán trả thẳng, trả góp thông qua ngân hàng với lãi suất thấp, thủ tục nhanh gọn. \n  Công ty hỗ trợ làm thủ  tục đăng ký, đăng kiểm và giao xe tại nhà\n\nƯu đãi đặc biệt cho khách hàng mua xe trong tháng 07/2011, hãy liên hệ ngay với chúng tôi để có giá tốt nhất. \nMs THẮM  ĐT: 0987.177.118   0916.51.29.51\nEmail: phamtham_kinhdoanh@tailocphat.com', 'none.gif', 'default', 545, 'Pham Tham', 'E22 Xa Lộ Hà Nội. P Long Bình, Tp Biên Hòa .t. Đồng Nai', '03.86505614', '098.7177118', 'phamtham_kinhdoanh@tailocphat.com', 'chuotchu2603', '', 1, 0, 0, 0, 0),
(1401, 'Rao bán xe máy Yamaha', 'Rao bán xe máy Yamaha', 81, 234, 4, 1310832000, 1313510400, '&lt;p&gt;&amp;nbsp; &amp;bull;&amp;nbsp;&amp;nbsp;Rao vặt được đăng c&amp;oacute; k&amp;egrave;m theo ảnh rao vặt sẽ được kh&amp;aacute;ch gh&amp;eacute; thăm quan t&amp;acirc;m hơn. V&amp;igrave; vậy, bạn n&amp;ecirc;n đăng k&amp;egrave;m theo một ảnh rao vặt để l&amp;agrave;m cho tin đăng của bạn th&amp;ecirc;m nổi bật.&lt;br /&gt; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;bull;&amp;nbsp;&amp;nbsp;Tin tuyển dụng, tin t&amp;igrave;m việc bạn cung cấp c&amp;agrave;ng nhiều th&amp;ocirc;ng tin th&amp;igrave; tin đăng của bạn sẽ c&amp;agrave;ng được nhiều người quan t&amp;acirc;m. Đặc biệt, bạn c&amp;oacute; thể li&amp;ecirc;n hệ với ch&amp;uacute;ng t&amp;ocirc;i để y&amp;ecirc;u cầu đ&amp;aacute;nh dấu tin tuyển dụng hay tin t&amp;igrave;m việc của bạn th&amp;agrave;nh tin &lt;span style=&quot;color: &curren;ff0000;&quot;&gt;&lt;strong&gt;đ&amp;atilde; được x&amp;aacute;c thực&lt;/strong&gt;&lt;/span&gt; (tin đ&amp;aacute;ng tin cậy).&lt;br /&gt; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;bull;&amp;nbsp;&amp;nbsp;Khi đăng sản phẩm, rao vặt, tin tuyển dụng, tin t&amp;igrave;m việc, bạn n&amp;ecirc;n nhập đầy đủ th&amp;ocirc;ng tin, c&amp;oacute; như vậy sản phẩm hay tin đăng của bạn mới dễ d&amp;agrave;ng tiếp cận với kh&amp;aacute;ch gh&amp;eacute; thăm.&lt;br /&gt; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;bull;&amp;nbsp;&amp;nbsp;Khi đăng sản phẩm, rao vặt, tin tuyển dụng, tin t&amp;igrave;m việc, bạn n&amp;ecirc;n đọc trước những hướng dẫn &lt;img src=&quot;../templates/home/images/help_post.gif&quot; alt=&quot;&quot; border=&quot;0&quot; /&gt; k&amp;egrave;m theo để việc đăng sản phẩm, rao vặt, tin tuyển dụng, tin t&amp;igrave;m việc của bạn được nhanh ch&amp;oacute;ng.&lt;br /&gt; &amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;nbsp;&amp;bull;&amp;nbsp;&amp;nbsp;Nếu bạn gặp kh&amp;oacute; khăn trong qu&amp;aacute; tr&amp;igrave;nh đăng sản phẩm, rao vặt, tin tuyển dụng, tin t&amp;igrave;m việc, bạn vui l&amp;ograve;ng li&amp;ecirc;n hệ với ch&amp;uacute;ng t&amp;ocirc;i để được hỗ trợ.&lt;/p&gt;', 'none.gif', 'default', 544, 'Phạm Kim Khánh', '91 Cách Mạng Tháng Tám, Phường An Thới, Quận Bình Thủy, Tp. Cần Thơ', '0932738288', '0932738288', 'plphamkimkhanh@yahoo.com', 'plphamkimkhanh', '', 1, 1, 0, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_ads_bad`
--

CREATE TABLE IF NOT EXISTS `tbtt_ads_bad` (
  `adb_id` int(10) NOT NULL AUTO_INCREMENT,
  `adb_title` varchar(100) NOT NULL,
  `adb_detail` text NOT NULL,
  `adb_email` varchar(50) NOT NULL,
  `adb_ads` int(10) NOT NULL,
  `adb_date` int(11) NOT NULL,
  PRIMARY KEY (`adb_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_ads_comment`
--

CREATE TABLE IF NOT EXISTS `tbtt_ads_comment` (
  `adc_id` int(10) NOT NULL AUTO_INCREMENT,
  `adc_title` varchar(100) NOT NULL,
  `adc_comment` text NOT NULL,
  `adc_ads` int(10) NOT NULL,
  `adc_user` int(10) NOT NULL,
  `adc_date` int(11) NOT NULL,
  PRIMARY KEY (`adc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=4 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_ads_favorite`
--

CREATE TABLE IF NOT EXISTS `tbtt_ads_favorite` (
  `adf_id` int(10) NOT NULL AUTO_INCREMENT,
  `adf_ads` int(10) NOT NULL,
  `adf_user` int(10) NOT NULL,
  `adf_date` int(11) NOT NULL,
  PRIMARY KEY (`adf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_advertise`
--

CREATE TABLE IF NOT EXISTS `tbtt_advertise` (
  `adv_id` int(10) NOT NULL AUTO_INCREMENT,
  `adv_user` int(11) NOT NULL,
  `adv_title` varchar(100) NOT NULL,
  `adv_banner` varchar(150) NOT NULL,
  `adv_dir` varchar(150) NOT NULL,
  `adv_link` varchar(100) NOT NULL,
  `adv_fullname` varchar(100) NOT NULL,
  `adv_address` varchar(100) NOT NULL,
  `adv_email` varchar(50) NOT NULL,
  `adv_phone` varchar(20) NOT NULL,
  `adv_mobile` varchar(20) NOT NULL,
  `adv_begindate` int(11) NOT NULL,
  `adv_enddate` int(11) NOT NULL,
  `adv_status` int(1) NOT NULL,
  `adv_position` int(1) NOT NULL,
  `adv_page` text NOT NULL,
  `adv_shop` text NOT NULL,
  `adv_order` int(10) NOT NULL,
  PRIMARY KEY (`adv_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=46 ;

--
-- Dumping data for table `tbtt_advertise`
--

INSERT INTO `tbtt_advertise` (`adv_id`, `adv_user`, `adv_title`, `adv_banner`, `adv_dir`, `adv_link`, `adv_fullname`, `adv_address`, `adv_email`, `adv_phone`, `adv_mobile`, `adv_begindate`, `adv_enddate`, `adv_status`, `adv_position`, `adv_page`, `adv_shop`, `adv_order`) VALUES
(23, 0, 'quag cao gian hang', 'f6717cd223284512522b624e4aed376e.jpg', '31052011', 'muabanoto24h.vn/otovip', 'Otovip', 'Bien Hòa', 'hoangtuanltd@yahoo.com.vn', '56763432432', '', 1306774800, 1306861200, 0, 3, 'shop_detail', '', 1),
(24, 0, 'Quảng cáo Thị Trường 37', '7b870bf0595c26193aacf66047ebcc83.swf', '01062011', 'thitruong37.com/login', 'Thị Trường 37', 'Nghệ An', 'info@thitruong37.com', '0904683465', '', 1306861200, 1341075600, 1, 1, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub,job_detail', '', 2),
(26, 0, 'quang cao trượt bên trái', 'cea501a41090440b54ccb89d16d509c8.swf', '07062011', 'otovip.com.vn', 'O Tô Vip', 'Biên Hòa', 'xuantu59@yahoo.com.vn', '0616292486', '', 1307379600, 1341075600, 0, 7, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub,job_detail', '', 5),
(27, 0, 'banner doc phai', '869d9cf6f16506c6c1e2db89280ed4f4.swf', '07062011', 'otovip.com.vn', 'Otovip', 'Bien Hòa', 'xuantu59@yahoo.com.vn', '0616292486', '', 1307379600, 1341075600, 0, 8, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index', '', 6),
(31, 0, 'qcgianhang', '94fd9a3b8659455e2531483988085b73.gif', '16062011', 'muabanoto24h.vn/otovip', 'Otovip', 'Biên Hòa', 'hoangtuanltd@yahoo.com.vn', '0985587997', '', 1308157200, 1341075600, 0, 4, 'shop_index,shop_sub,shop_detail', '', 1),
(29, 0, 'Chuyên huyndai nhập khẩu nguyên chiếc', '99a17aa9882ee06b8e50c9fd0b5843e5.swf', '08062011', 'otovip.com.vn', 'Otovip', 'Biên Hòa', 'xuantu59@yahoo.com.vn', '0616292486', '', 1307466000, 1341075600, 0, 5, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index', '', 1),
(30, 0, 'Chuyên xe kia', '2894b9bba0fef44d3a949e17069d5ecf.swf', '08062011', 'otovip.com.vn', 'Otovip', 'Biên Hòa', 'phamtran2223@yahoo.com', '0616292486', '0933009924', 1307466000, 1341075600, 0, 4, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub', '', 2),
(32, 0, 'toyota_vip', 'a2c7d36640d0f4c3e329d45be4dbd1d9.swf', '20062011', 'otovip.com.vn', 'Toyota', 'Biên Hòa', 'xuantu59@yahoo.com.vn', '0919755776', '', 1308502800, 1404147600, 0, 3, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub,job_detail', '', 2),
(33, 0, 'toyota1', 'eaa7bf116052dfff3fde573aa5a1457f.swf', '20062011', 'toyotavn.com.vn', 'Toyota Viet Nam', 'Biên Hòa', 'xuantu359@yahoo.com.vn', '0938088838', '', 1308502800, 1341075600, 0, 6, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub', '', 1),
(34, 0, 'người đẹp và xe', '5260b5e4e919321fbfc734e2c89c2801.jpg', '20062011', 'otobienhoa.com.vn', 'O Tô Biên Hòa', 'Biên Hòa', 'xuantu59@yahoo.com.vn', '0616292486', '', 1308502800, 1341072000, 0, 11, 'shop_detail', '63,69', 3),
(35, 0, 'xe máy công trình', 'acda9a10fadc70183ebed143d7987cf5.gif', '20062011', 'otobienhoa.com.vn', 'O Tô Biên Hòa', 'Biên Hòa', 'hoangtuanltd@yahoo.com.vn', '0616292486', '', 1308502800, 1341075600, 0, 3, 'home,product_sub,product_detail,ads_index,ads_sub', '', 2),
(36, 0, 'chuyên chevrolet', 'eecdf0d093fad542cae27c2fd33b81c4.swf', '20062011', 'otovip.com.vn', 'Otovip', 'Biên Hòa - Đồng Nai', 'xuantu59@yahoo.com.vn', '0616292486', '', 1308502800, 1404147600, 0, 4, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub,job_detail,employ_index,employ_sub,employ_detail', '', 3),
(37, 0, 'bán xe trả góp', '99e1a08471ea15569c81d6a7f59cf7b5.jpg', '20062011', 'otovip.com.vn', 'Otovip', 'Biên Hòa - Đồng Nai', 'xuantu59@yahoo.com.vn', '0616292486', '', 1308502800, 1372611600, 0, 4, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub,job_detail', '', 4),
(38, 0, 'xe tải trả góp', 'a4fb91763842a50b59de5e5d185352c0.jpg', '20062011', 'otovip.com.vn', 'Otovip', 'Biên Hòa - Đồng Nai', 'xuantu59@yahoo.com.vn', '0616292486', '', 1308502800, 1404147600, 0, 3, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub,job_detail', '', 4),
(39, 0, 'kinh doanh các dòng xe nhập khẩu nguyên chiếc', 'fcefbc4af5a2041e68bfec7be37af49a.gif', '20062011', 'otovip.com.vn', 'Otovip', 'Biên Hòa - Đồng Nai', 'xuantu59@yahoo.com.vn', '0616292486', '', 1308502800, 1341075600, 0, 3, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index', '', 3),
(42, 544, 'Banner bên trái', 'ca6a0347f30db10f29f1f00736630215.jpg', '15072011', 'http://abc.com.vn', 'Lê Trung Hiếu', '', 'hieu.lt@fibo.vn', '', '', 1310744143, 1626278400, 0, 3, 'shop_detail', '', 2),
(43, 0, 'Thị trường 24 giờ', '0e07fcf6abc88464e358f989da146699.swf', '14122011', 'thitruong24gio.com', 'Lê Trung Hiếu', 'Sg', 'lehieu008@yahoo.com', '(08).7300.7795', '', 1323795600, 1341075600, 1, 4, 'home', '', 1),
(45, 0, 'Mẫu đăng ký cửa hàng ảo', '67839a6ab0e8ed93709044a1c48d8705.swf', '18122011', 'thitruong37.com/templates/guide/data/maucungcapthongtin.doc', 'Thi Trường 37', 'Vinh', 'administrator@thitruong37.com', '0904683465', '', 1324141200, 1356973200, 1, 10, 'home,product_sub,product_detail,ads_index,ads_sub,ads_detail,job_index,job_sub,job_detail,employ_index,employ_sub,employ_detail,shop_index,shop_sub,shop_detail,search,showcart', '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_category`
--

CREATE TABLE IF NOT EXISTS `tbtt_category` (
  `cat_id` int(3) NOT NULL AUTO_INCREMENT,
  `cat_name` varchar(50) NOT NULL,
  `cat_descr` varchar(100) NOT NULL,
  `cat_image` varchar(150) NOT NULL,
  `cat_order` int(3) NOT NULL,
  `cat_status` int(1) NOT NULL,
  `cat_parent` int(5) NOT NULL,
  PRIMARY KEY (`cat_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=441 ;

--
-- Dumping data for table `tbtt_category`
--

INSERT INTO `tbtt_category` (`cat_id`, `cat_name`, `cat_descr`, `cat_image`, `cat_order`, `cat_status`, `cat_parent`) VALUES
(425, 'Nội thất, Ngoại thất', 'Các sản phẩm, tin rao về lĩnh vực Nội thất, Ngoại thất', 'icon_field_36.gif 	', 9, 1, 0),
(424, 'Công nghiệp, xây dựng', 'Các sản phẩm, tin rao về lĩnh vực Công nghiệp, xây dựng', '', 8, 1, 0),
(423, 'Ô tô, Xe máy, Xe đạp', 'Các sản phẩm, tin rao về lĩnh vực Ô tô, Xe máy, Xe đạp', '', 7, 1, 0),
(421, 'Điện tử, Nhạc cụ', 'Các sản phẩm, tin rao về lĩnh vực Điện tử, Nhạc cụ', '', 5, 1, 0),
(420, 'Sách vở, Đồ văn phòng', 'Các sản phẩm, tin rao về lĩnh vực Sách vở, Đồ văn phòng', '', 4, 1, 0),
(418, 'Máy tính, Linh kiện', 'Các sản phẩm, tin rao về lĩnh vực Máy tính, Linh kiện', '', 1, 1, 0),
(419, 'Website, Phần mềm', 'Các sản phẩm, tin rao về lĩnh vực Website, Phần mềm', '', 3, 1, 0),
(422, 'Máy ảnh, Máy quay', 'Các sản phẩm, tin rao về lĩnh vực Đồ điện, Điện tử', '', 6, 1, 0),
(417, 'Điện thoại, Viễn thông', 'Các sản phẩm, tin rao về lĩnh vực Điện thoại, Viễn thông', '', 2, 1, 0),
(432, 'Linh tinh khác', 'Các sản phẩm, tin rao về lĩnh vực Linh tinh khác', '', 16, 1, 0),
(431, 'Giải trí, Dịch vụ', 'Các sản phẩm, tin rao về lĩnh vực Giải trí, Dịch vụ', '', 15, 1, 0),
(430, 'Thuốc chữa bệnh', 'Các sản phẩm, tin rao về lĩnh vực Thuốc chữa bệnh', '', 14, 1, 0),
(429, 'Thực phẩm, Đồ uống', 'Các sản phẩm, tin rao về lĩnh vực Thực phẩm, Đồ uống', '', 13, 1, 0),
(428, 'Thời trang', 'Các sản phẩm, tin rao về lĩnh vực Thời trang', '', 12, 1, 0),
(427, 'Mỹ phẩm, Sắc đẹp', 'Các sản phẩm, tin rao về lĩnh vực Mỹ phẩm, Sắc đẹp', '', 11, 1, 0),
(426, 'Hoa, Quà tặng', 'Các sản phẩm, tin rao về lĩnh vực Hoa, Quà tặng', '', 10, 1, 0),
(433, 'Bất động sản', 'Các lĩnh vực kinh doanh về bất động sản', '', 9, 1, 0),
(434, 'Đồ dùng sinh hoạt', 'Tất cả các sản phẩm Đồ dùng sinh hoạt', '', 14, 1, 0),
(435, 'SamSung', 'SamSung', '', 1, 1, 418),
(436, 'Sony', 'Sony', '', 1, 1, 418),
(437, 'MacBook Pro', 'MacBook Pro', '', 1, 1, 418),
(438, 'Asus', 'Asus', '', 1, 1, 418),
(439, 'Nokia', 'Nokia', '', 1, 1, 417),
(440, 'LG', 'LG', '', 1, 1, 417);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_category_shop`
--

CREATE TABLE IF NOT EXISTS `tbtt_category_shop` (
  `cas_id` int(11) NOT NULL AUTO_INCREMENT,
  `cas_parent` int(11) NOT NULL,
  `cas_user` int(11) NOT NULL,
  `cas_name` varchar(100) NOT NULL,
  `cas_descr` varchar(100) NOT NULL,
  `cas_order` int(11) NOT NULL,
  `cas_status` tinyint(1) NOT NULL,
  PRIMARY KEY (`cas_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=17 ;

--
-- Dumping data for table `tbtt_category_shop`
--

INSERT INTO `tbtt_category_shop` (`cas_id`, `cas_parent`, `cas_user`, `cas_name`, `cas_descr`, `cas_order`, `cas_status`) VALUES
(6, 0, 569, 'Bất động sản', 'Môi giới - Nhận kí gửi', 0, 1),
(7, 0, 569, 'Xe máy', 'Mua bán xe máy', 0, 1),
(8, 0, 569, 'Lattop', 'Mua - Bán Lattop', 0, 1),
(9, 0, 569, 'Điện thoại', 'Mua - Bán Điện thoại', 0, 1),
(10, 0, 569, 'Sim số đẹp', 'Mua - Bán Sim số đẹp', 0, 1),
(11, 6, 569, 'Đất cần bán', 'Cần bán Đất - Nhà đất', 1, 1),
(12, 6, 569, 'Đất cần mua', 'Cần mua Đất - Nhà đất', 0, 1),
(13, 6, 569, 'Nhà cần bán', 'Cần thuê Đất - Nhà - Căn hộ CC', 3, 1),
(14, 6, 569, 'Nhà cần mua', '', 0, 1),
(15, 6, 569, 'Căn hộ CC cần bán', '', 0, 1),
(16, 6, 569, 'Căn hộ CC cần thuê', '', 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_contact`
--

CREATE TABLE IF NOT EXISTS `tbtt_contact` (
  `con_id` int(10) NOT NULL AUTO_INCREMENT,
  `con_title` varchar(100) NOT NULL,
  `con_detail` text NOT NULL,
  `con_position` int(1) NOT NULL,
  `con_user` int(10) NOT NULL,
  `con_date_contact` int(11) NOT NULL,
  `con_date_reply` int(11) NOT NULL,
  `con_view` int(1) NOT NULL,
  `con_reply` int(1) NOT NULL,
  `con_status` int(1) NOT NULL,
  PRIMARY KEY (`con_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_counter`
--

CREATE TABLE IF NOT EXISTS `tbtt_counter` (
  `cou_id` int(1) NOT NULL DEFAULT '1',
  `cou_counter` int(15) NOT NULL,
  PRIMARY KEY (`cou_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `tbtt_counter`
--

INSERT INTO `tbtt_counter` (`cou_id`, `cou_counter`) VALUES
(1, 5140);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_employ`
--

CREATE TABLE IF NOT EXISTS `tbtt_employ` (
  `emp_id` int(10) NOT NULL AUTO_INCREMENT,
  `emp_title` varchar(100) NOT NULL,
  `emp_field` int(3) NOT NULL,
  `emp_position` varchar(150) NOT NULL,
  `emp_province` int(3) NOT NULL,
  `emp_time_job` varchar(50) NOT NULL,
  `emp_salary` varchar(50) NOT NULL,
  `emp_detail` text NOT NULL,
  `emp_fullname` varchar(100) NOT NULL,
  `emp_age` int(2) NOT NULL,
  `emp_sex` int(1) NOT NULL,
  `emp_level` varchar(150) NOT NULL,
  `emp_foreign_language` varchar(150) NOT NULL,
  `emp_computer` varchar(150) NOT NULL,
  `emp_exper` int(2) NOT NULL,
  `emp_address` varchar(100) NOT NULL,
  `emp_phone` varchar(20) NOT NULL,
  `emp_mobile` varchar(20) NOT NULL,
  `emp_email` varchar(50) NOT NULL,
  `emp_yahoo` varchar(50) NOT NULL,
  `emp_skype` varchar(50) NOT NULL,
  `emp_begindate` int(11) NOT NULL,
  `emp_enddate` int(11) NOT NULL,
  `emp_user` int(10) NOT NULL,
  `emp_status` int(1) NOT NULL,
  `emp_reliable` int(1) NOT NULL,
  `emp_view` int(10) NOT NULL,
  PRIMARY KEY (`emp_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=108 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_employ_bad`
--

CREATE TABLE IF NOT EXISTS `tbtt_employ_bad` (
  `emb_id` int(10) NOT NULL AUTO_INCREMENT,
  `emb_title` varchar(100) NOT NULL,
  `emb_detail` text NOT NULL,
  `emb_email` varchar(50) NOT NULL,
  `emb_employ` int(10) NOT NULL,
  `emb_date` int(11) NOT NULL,
  PRIMARY KEY (`emb_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_employ_favorite`
--

CREATE TABLE IF NOT EXISTS `tbtt_employ_favorite` (
  `emf_id` int(10) NOT NULL AUTO_INCREMENT,
  `emf_employ` int(10) NOT NULL,
  `emf_user` int(10) NOT NULL,
  `emf_date` int(11) NOT NULL,
  PRIMARY KEY (`emf_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_field`
--

CREATE TABLE IF NOT EXISTS `tbtt_field` (
  `fie_id` int(3) NOT NULL AUTO_INCREMENT,
  `fie_name` varchar(50) NOT NULL,
  `fie_descr` varchar(100) NOT NULL,
  `fie_image` varchar(150) NOT NULL,
  `fie_order` int(3) NOT NULL,
  `fie_status` int(1) NOT NULL,
  PRIMARY KEY (`fie_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=42 ;

--
-- Dumping data for table `tbtt_field`
--

INSERT INTO `tbtt_field` (`fie_id`, `fie_name`, `fie_descr`, `fie_image`, `fie_order`, `fie_status`) VALUES
(1, 'Công nghệ thông tin', 'Công nghệ thông tin', 'icon_field_36.gif', 9, 1),
(2, 'Giáo dục - Đào tạo', 'Giáo dục - Đào tạo', 'icon_field_16.gif', 15, 1),
(3, 'Báo chí - Truyền hình', 'Báo chí - Truyền hình', 'icon_field_10.gif', 1, 1),
(4, 'Bảo hiểm', 'Bảo hiểm', 'icon_field_08.gif', 2, 1),
(5, 'Bất động sản', 'Bất động sản', 'icon_field_37.gif', 3, 1),
(6, 'Biên - Phiên dịch', 'Biên - Phiên dịch', 'icon_field_27.gif', 4, 1),
(7, 'Dịch vụ an ninh', 'Dịch vụ an ninh', 'icon_field_38.gif', 12, 1),
(8, 'Điện - Điện tử - Điện lạnh', 'Điện - Điện tử - Điện lạnh', 'icon_field_34.gif', 13, 1),
(9, 'Du lịch - Nhà hàng - Khách sạn', 'Du lịch - Nhà hàng - Khách sạn', 'icon_field_18.gif', 14, 1),
(10, 'Y tế - Dược', 'Y tế - Dược', 'icon_field_06.gif', 39, 1),
(11, 'Làm bán thời gian', 'Làm bán thời gian', 'icon_field_20.gif', 22, 1),
(13, 'In ấn - Xuất bản', 'In ấn - Xuất bản', 'icon_field_04.gif', 18, 1),
(14, 'Luật - Pháp lý', 'Luật - Pháp lý', 'icon_field_39.gif', 24, 1),
(15, 'Hành chính - Văn phòng', 'Hành chính - Văn phòng', 'icon_field_13.gif', 16, 1),
(16, 'Quản lý - Điều hành', 'Quản lý - Điều hành', 'icon_field_07.gif', 28, 1),
(17, 'Kế toán - Kiểm toán', 'Kế toán - Kiểm toán', 'icon_field_15.gif', 19, 1),
(18, 'Kinh doanh - Bán hàng', 'Kinh doanh - Bán hàng', 'icon_field_26.gif', 21, 1),
(19, 'Marketing - PR', 'Marketing - PR', 'icon_field_14.gif', 25, 1),
(20, 'Lao động phổ thông', 'Lao động phổ thông', 'icon_field_01.gif', 23, 1),
(21, 'Dầu khí - Địa chất', 'Dầu khí - Địa chất', 'icon_field_17.gif', 10, 1),
(22, 'Cơ khí - Chế tạo', 'Cơ khí - Chế tạo', 'icon_field_30.gif', 8, 1),
(23, 'Dệt may - Da giày', 'Dệt may - Da giày', 'icon_field_02.gif', 11, 1),
(24, 'Hóa học - Sinh học', 'Hóa học - Sinh học', 'icon_field_31.gif', 17, 1),
(25, 'Kiến trúc - Nội thất', 'Kiến trúc - Nội thất', 'icon_field_19.gif', 20, 1),
(26, 'Thiết kế - Mỹ thuật', 'Thiết kế - Mỹ thuật', 'icon_field_28.gif', 31, 1),
(27, 'Chứng khoán - Vàng', 'Chứng khoán - Vàng', 'icon_field_03.gif', 7, 1),
(28, 'Tài chính, Ngân hàng', 'Tài chính, Ngân hàng', 'icon_field_11.gif', 29, 1),
(29, 'Người giúp việc', 'Người giúp việc', 'icon_field_29.gif', 26, 1),
(30, 'Nông - Lâm - Ngư nghiệp', 'Nông - Lâm - Ngư nghiệp', 'icon_field_05.gif', 27, 1),
(31, 'Vận tải - Giao nhận', 'Vận tải - Giao nhận', 'icon_field_09.gif', 36, 1),
(32, 'Xây dựng', 'Xây dựng', 'icon_field_32.gif', 37, 1),
(33, 'Thủ công mỹ nghệ', 'Thủ công mỹ nghệ', 'icon_field_35.gif', 32, 1),
(34, 'Ngành nghề khác', 'Ngành nghề khác', 'icon_field_24.gif', 40, 1),
(35, 'Bưu chính viễn thông', 'Bưu chính viễn thông', 'icon_field_40.gif', 5, 1),
(36, 'Chăm sóc khách hàng', 'Chăm sóc khách hàng', 'icon_field_22.gif', 6, 1),
(37, 'Thẩm định - Giám định', 'Thẩm định - Giám định', 'icon_field_12.gif', 30, 1),
(38, 'Thư ký - Trợ lý', 'Thư ký - Trợ lý', 'icon_field_23.gif', 33, 1),
(39, 'Thực tập sinh', 'Thực tập sinh', 'icon_field_21.gif', 34, 1),
(40, 'Tư vấn', 'Tư vấn', 'icon_field_25.gif', 35, 1),
(41, 'Xuất - Nhập khẩu', 'Xuất - Nhập khẩu', 'icon_field_33.gif', 38, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_forgot`
--

CREATE TABLE IF NOT EXISTS `tbtt_forgot` (
  `for_id` int(10) NOT NULL AUTO_INCREMENT,
  `for_password` varchar(35) NOT NULL,
  `for_salt` varchar(10) NOT NULL,
  `for_email` varchar(50) NOT NULL,
  `for_key` varchar(150) NOT NULL,
  PRIMARY KEY (`for_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=18 ;

--
-- Dumping data for table `tbtt_forgot`
--

INSERT INTO `tbtt_forgot` (`for_id`, `for_password`, `for_salt`, `for_email`, `for_key`) VALUES
(17, 'da6cbdbd7ce910e8433c0643386825b8', 'JZwrNBy4', 'chauict@gmail.com', '6aea5309ba982157bc132a125baf560bc95522b09b3eaf47289b509416274c8f');

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_group`
--

CREATE TABLE IF NOT EXISTS `tbtt_group` (
  `gro_id` int(3) NOT NULL AUTO_INCREMENT,
  `gro_name` varchar(50) NOT NULL,
  `gro_descr` varchar(100) NOT NULL,
  `gro_permission` text NOT NULL,
  `gro_order` int(3) NOT NULL,
  `gro_status` int(1) NOT NULL,
  PRIMARY KEY (`gro_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=6 ;

--
-- Dumping data for table `tbtt_group`
--

INSERT INTO `tbtt_group` (`gro_id`, `gro_name`, `gro_descr`, `gro_permission`, `gro_order`, `gro_status`) VALUES
(1, 'Bình thường', 'Bình thường', 'none', 1, 1),
(2, 'Thành viên VIP', 'Thành viên VIP', 'none', 2, 1),
(3, 'Chủ cửa hàng', 'Chủ cửa hàng', 'none', 3, 1),
(4, 'Quản trị', 'Quản trị', 'all', 4, 1),
(5, 'Khách', 'Khách vãng lai', 'config_view,user_view,group_view,category_view,field_view,province_view,product_view,ads_view,job_view,employ_view,shop_view,advertise_view,contact_view,notify_view,menu_view,showcart_view', 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_job`
--

CREATE TABLE IF NOT EXISTS `tbtt_job` (
  `job_id` int(10) NOT NULL AUTO_INCREMENT,
  `job_title` varchar(100) NOT NULL,
  `job_field` int(3) NOT NULL,
  `job_position` varchar(150) NOT NULL,
  `job_level` varchar(150) NOT NULL,
  `job_foreign_language` varchar(150) NOT NULL,
  `job_computer` varchar(150) NOT NULL,
  `job_age` varchar(10) NOT NULL,
  `job_sex` int(1) NOT NULL,
  `job_require` text NOT NULL,
  `job_exper` int(2) NOT NULL,
  `job_province` int(3) NOT NULL,
  `job_time_job` varchar(50) NOT NULL,
  `job_salary` varchar(50) NOT NULL,
  `job_timetry` varchar(50) NOT NULL,
  `job_interest` text NOT NULL,
  `job_quantity` int(5) NOT NULL,
  `job_record` text NOT NULL,
  `job_time_surrend` int(11) NOT NULL,
  `job_detail` text NOT NULL,
  `job_jober` varchar(100) NOT NULL,
  `job_address` varchar(100) NOT NULL,
  `job_phone` varchar(20) NOT NULL,
  `job_mobile` varchar(20) NOT NULL,
  `job_email` varchar(50) NOT NULL,
  `job_website` varchar(100) NOT NULL,
  `job_name_contact` varchar(100) NOT NULL,
  `job_address_contact` varchar(100) NOT NULL,
  `job_phone_contact` varchar(20) NOT NULL,
  `job_mobile_contact` varchar(20) NOT NULL,
  `job_email_contact` varchar(50) NOT NULL,
  `job_yahoo` varchar(50) NOT NULL,
  `job_skype` varchar(50) NOT NULL,
  `job_best_contact` varchar(50) NOT NULL,
  `job_begindate` int(11) NOT NULL,
  `job_enddate` int(11) NOT NULL,
  `job_user` int(10) NOT NULL,
  `job_status` int(1) NOT NULL,
  `job_reliable` int(1) NOT NULL,
  `job_view` int(10) NOT NULL,
  PRIMARY KEY (`job_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=328 ;

--
-- Dumping data for table `tbtt_job`
--

INSERT INTO `tbtt_job` (`job_id`, `job_title`, `job_field`, `job_position`, `job_level`, `job_foreign_language`, `job_computer`, `job_age`, `job_sex`, `job_require`, `job_exper`, `job_province`, `job_time_job`, `job_salary`, `job_timetry`, `job_interest`, `job_quantity`, `job_record`, `job_time_surrend`, `job_detail`, `job_jober`, `job_address`, `job_phone`, `job_mobile`, `job_email`, `job_website`, `job_name_contact`, `job_address_contact`, `job_phone_contact`, `job_mobile_contact`, `job_email_contact`, `job_yahoo`, `job_skype`, `job_best_contact`, `job_begindate`, `job_enddate`, `job_user`, `job_status`, `job_reliable`, `job_view`) VALUES
(311, 'Tuyển nhân viên khai thác khách hàng', 21, '', '', '', '', '0-0', 0, 'Tìm kiếm, khai thác, quản lý khách hàng và thị trường nguyên liệu đầu vào cho nhà máy.', 0, 83, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 3, 'Trình độ yêu cầu: Trung cấp các ngành kinh tế trở lên', 1323622800, '<p>Thu nhập trung b&igrave;nh : 5-8 triệu đồng/th&aacute;ng.<br /> Nội dung c&ocirc;ng việc ch&iacute;nh: T&igrave;m kiếm, khai th&aacute;c, quản l&yacute; kh&aacute;ch h&agrave;ng v&agrave; thị trường nguy&ecirc;n liệu đầu v&agrave;o cho nh&agrave; m&aacute;y.<br /> Nếu ở xa c&ocirc;ng ty bố tr&iacute; chổ ở <br /> Tr&igrave;nh độ y&ecirc;u cầu: Trung cấp c&aacute;c ng&agrave;nh kinh tế trở l&ecirc;n<br /> Được tham gia bảo hiểm theo quy định</p>', 'Công Ty Cổ Phần Đầu Tư Khoáng Sản Thái An - Chi Nhánh Tại Hà Tĩnh', 'Lô A2 Khu Công Nghiệp Vũng Áng I - Kỳ Thịnh - Kỳ Anh - Hà Tĩnh', '', '', '', '', 'Anh Chiến', '', '', '0973992499', 'khoangsanthaian@gmail.com', '', '', 'Gọi điện thoại', 1323190800, 1325869200, 547, 1, 0, 18),
(315, 'Tuyển nhân viên kinh doanh', 18, '', '', '', '', '0-0', 0, '- Nhanh nhẹn, tự tin, trung thực, có khả năng giao tiếp, thuyết phục người khác.\n- Có chí tiến thủ, mong muốn được gắn bó lâu dài\n- Ưu tiên những ứng ', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 5, '', 1325869200, '<p>- Chưa biết việc sẽ được đ&agrave;o tạo<br /> - Được l&agrave;m việc trong m&ocirc;i trường chuy&ecirc;n nghiệp<br /> - Thu nhập hấp dẫn: gồm lương căn bản + % doanh thu + thưởng<br /> - Được hưởng đầy đủ c&aacute;c ch&iacute;nh s&aacute;ch theo luật lao động hiện h&agrave;nh<br /> thời gian l&agrave;m việc : s&aacute;ng 8h30<br /> chiều 2h<br /> mức lương cơ bản của nh&acirc;n vi&ecirc;n kinh doanh 2.000.000vnd<br /> doanh số mỗi th&aacute;ng đạt 60 chai<br /> tr&ecirc;n 60 chai được hưởng 10.000vnd một n&uacute;t chai thu về ( c&ocirc;ng nợ thu về)</p>', 'Công Ty Hưng Sơn Hà', '80 Nguyễn Thái Học, Tp Vinh', '', '', '', '', 'Anh Thỏa', '', '', '0985821741', 'administrator@thitruong37.com', '', '', 'Gọi điện thoại', 1323190800, 1325869200, 547, 1, 0, 12),
(312, 'Tuyển nhân viên kế toán', 17, '', '', '', '', '0-0', 0, '- Giới tính: Nam hoặc nữ\n- Trình độ: Trung cấp trở lên\n- Hình thức: dễ nhìn\n- Có kinh nghiệm kế toán trên 1 năm', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 1, '', 1325869200, '<p>Mức lương sẽ thỏa thuận khi phỏng vấn</p>', 'Công Ty Tnhh Quảng Cáo Trẻ Nghệ An', '46 Trần Phú - Thành Phố Vinh', '', '', '', '', 'Phan Minh Châu', '', '', '0383.844.015', 'administrator@thitruong37.com', '', '', 'Gặp trực tiếp', 1323190800, 1325869200, 547, 1, 0, 11),
(313, 'Công Ty TNHH Đại Nam Nguyên tuyển dụng mùa tết 2012', 10, '', '', '', '', '0-0', 0, '- Có kinh nghiệm làm trình dược viên\n- Chịu khó, trung thực, có chí tiền thủ, gắn bó với công ty.', 0, 84, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 2, '', 1325869200, '<p>ứng vi&ecirc;n được tuyển dụng được k&yacute; hợp đồng v&agrave; hưởng đầy đủ c&aacute;c chế độ BHXH, BHYT, KPCĐ, BH thất nghiệp, v&agrave; c&aacute;c chế độ kh&aacute;c của c&ocirc;ng ty.<br /> Mức lương ( chưa thưởng ) l&agrave; 5.000.000đ + phụ cấp<br /> *Ưu ti&ecirc;n c&aacute;c ứng vi&ecirc;n đ&atilde; c&oacute; kinh nghiệm về lĩnh vực dược phẩm.<br /> * Chỉ Tuyển C&aacute;c ứng vi&ecirc;n l&agrave; "Nam Giới" ( do y&ecirc;u cầu c&ocirc;ng việc )</p>', 'Công Ty Tnhh Đại Nam Nguyên', 'Xóm 22, Nghi Trung, Nghi Lộc, Nghệ An', '', '', '', '', 'Vũ Hồ Tây', '', '', '0978411868', '', '', '', 'Gọi điện thoại', 1323190800, 1325869200, 547, 1, 1, 9),
(314, 'Công ty Quốc tế Phong Việt tuyển dụng', 19, '', '', '', '', '0-0', 0, '+ Tốt nghiệp Đại học, Cao Đẳng chính quy chuyên ngành Quản trị kinh doanh, marketing\n+ Ưu tiên các ứng viên đã có kinh nghiệm làm trong lĩnh vực tuyển', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 2, '- Bản khai lý lịch tự thuật (theo mẫu hồ sơ quy định): Có xác nhận của Chính quyền địa phương nơi cư trú.\n- Đơn xin làm việc theo mẫu hoặc viết tay)\n- Bằng tốt nghiệp Đại học hoặc Cao Đẳng :bản sao có công chứng.\n- Giấy chứng nhận sức khỏe: Do bệnh viện huyện hoặc trung tâm y tế cấp huyện xác nhận.\n- Giấy khai sinh (01 bản), CMTND: Bản sao có công chứng.\n- 10 ảnh thẻ được chụp cách thời gian tuyển dụng không quá 06 tháng, trong đó: 04 ảnh cỡ 3x4, 02 ảnh cỡ 4x6.', 1325869200, '<p>+ Được đ&agrave;o tạo nghiệp vụ v&agrave; c&aacute;c kỹ năng phụ trợ chuy&ecirc;n s&acirc;u với chương tr&igrave;nh đ&agrave;o tạo kỹ năng t&iacute;nh nhẩm nhanh bằng b&agrave;n t&iacute;nh số học tr&iacute; tuệ UCMAS ( Chi ph&iacute; đ&agrave;o tạo của hướng dẫn vi&ecirc;n được C&ocirc;ng ty Đ&agrave;i thọ ).<br /> + Sau khi kết th&uacute;c kh&oacute;a học đạt kết quả thi tốt được cấp chứng chỉ quốc tế của UCMAS<br /> + Mức lương: thỏa thuận<br /> <br /> + Bảo hiểm x&atilde; hội, thu nhập ngo&agrave;i lương, ph&uacute;c lợi theo quy định của C&ocirc;ng ty .<br /> <br /> + M&ocirc;i trường l&agrave;m việc hiện đại, năng động, th&acirc;n thiện<br /> + C&oacute; l&yacute; lịch r&otilde; r&agrave;ng, trung thực, c&oacute; đạo đức tốt.</p>', 'Công Ty Tnhh Quốc Tế Phong Việt', 'Số 21-đường Ngư Hải-phường Lê Mao-tpvinh-nghệ An.', '', '', '', '', '', '', '', '01272576688', 'ucmasphongviet@gmail.com', '', '', 'Gặp trực tiếp', 1323190800, 1323882000, 547, 1, 0, 6),
(316, 'Công ty thiết kế nội thất MHT tuyển nhân viên kinh doanh', 18, '', '', '', '', '0-0', 0, 'nhanh nhẹn, có khả năng làm việc độc lập,chủ động trong công việc,chịu khó.\ntốt nghiệp từ trung cấp trở lên', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 3, '', 1325869200, '<p><strong>Mức lương: theo thoả thuận.chế độ thưởng n&oacute;ng hấp dẫn.<br /> Thời gian thử việc:3 th&aacute;ng, nếu l&agrave;m xuất sắc th&igrave; thời gian thử việc c&oacute; thể r&uacute;t ngắn xuống 1 hay 2 th&aacute;ng.C&oacute; nhiều cơ hội thăng tiến v&agrave; ph&aacute;t triển.<br /> Thời gian l&agrave;m việc: tự do</strong></p>', 'Công Ty Thiết Kế Nội Thất Mht', '', '', '', '', '', 'Ms Trang', '', '', '0916.008.777', 'trang.ntt@thietkenoithatinfo.com', '', '', 'Gửi Email', 1323190800, 1325869200, 547, 1, 0, 5),
(318, 'Công ty CP Lữ hành Du lịch Quốc tế Phúc Lợi tuyển nhân viên.', 18, '', '', '', '', '0-0', 0, '- Nhanh nhẹn, hoạt bát, cần cù\n- Khả năng giao tiếp tốt\nTrình độ: Tốt nghiệp Trung cấp trở lên ưu tiên ứng viên tốt nghiệp chuyên nghành du lịch, văn hóa, sử dụng vi tính thành thạo, biết sử dụng internet.\n- Hình thức ưa nhìn', 0, 84, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 10, '', 1326042000, '<p>Kinh nghiệm: Kh&ocirc;ng y&ecirc;u cầu&nbsp;<br />Giới t&iacute;nh: Nữ<br />H&igrave;nh thức l&agrave;m việc: L&agrave;m việc theo ca<br />Mức lương: 2.000.000đ (Hai triệu đồng tr&ecirc;n th&aacute;ng)<br />- Được đ&agrave;o tạo kỹ năng nghề nghiệp ban đầu.&nbsp;<br />- Được hưởng hoa hồng b&aacute;n v&eacute; nếu vượt qu&aacute; chỉ ti&ecirc;u.<br />- Được hưởng chế độ thưởng hấp dẫn nếu l&agrave;m việc đạt hiệu quả cao.<br />- Được đ&oacute;ng BHXH,BHYT theo quy định&nbsp;<br />- Được l&agrave;m việc trong m&ocirc;i trường chuy&ecirc;n nghiệp, năng động. C&oacute; cơ hội ph&aacute;t triển bản th&acirc;n.&nbsp;<br />- Mọi chi tiết sẽ trao đổi cụ thể khi phỏng vấn.&nbsp;</p>', 'Công Ty Cp Lữ Hành Du Lịch Quốc Tế Phúc Lợi', 'Số 1 - Đ. Mai Thúc Loan - P. Nghi Thu - Tx Cửa Lò - Nghệ An', '', '', '', '', 'Anh Dũng', '', '', '0979.080.555', '', '', '', 'Gọi điện thoại', 1323363600, 1326042000, 547, 1, 0, 7),
(317, 'Tuyển Gấp 30 Công Nhân Sản Xuất Cửa Nhựa Đi Làm Ngay', 20, '', '', '', '', '0-0', 0, '- Lao động phổ thông, có sức khỏe, đạo đức tốt;\n-Ưu tiên ứng viên có tay nghề: Cơ khí-Điện-Mộc-khung nhôm cửa kính hoặc đã có kinh nghiệm làm cửa nhựa', 0, 84, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 30, '- Sơ yếu lý lịch tự thuật có dán ảnh giáp lai và xác nhận của địa phương. - Đơn xin việc (viết tay)\n- Bảng tóm tắt kinh nghiệm làm việc (nếu có)\n- Chứng minh nhân dân (photo công chứng 01 chiếc)\n- Giấy khám sức khoẻ của bệnh viện. - Bản sao giấy khai sinh có xác nhận của địa phương. - Sổ hộ khẩu (photo công chứng 01 bản).', 1325869200, '<p>Tr&igrave;nh độ: Lao động phổ th&ocirc;ng <br /> Kinh nghiệm: Chưa c&oacute; kinh nghiệm <br /> Giới t&iacute;nh: Kh&ocirc;ng y&ecirc;u cầu <br /> H&igrave;nh thức l&agrave;m việc: Nh&acirc;n vi&ecirc;n ch&iacute;nh thức <br /> Mức lương: 3-4 triệu <br /> Thời gian thử việc: 01 th&aacute;ng<br /> Nhận việc ngay <br /> C&aacute;c chế độ kh&aacute;c:<br /> <br /> -Chế độ lương thưởng theo tay nghề chuy&ecirc;n m&ocirc;n;<br /> -Hợp đồng lao động v&agrave; c&aacute;c chế độ đ&atilde;i ngộ kh&aacute;c; <br /> C&aacute;c quyền lợi theo Luật lao động quy định</p>', '', '', '', '', '', '', 'Mr Quang', '', '', '0388.600.789', 'hosiquang@gmail.com', '', '', 'Mọi hình thức', 1323190800, 1325869200, 547, 1, 0, 15),
(319, 'Tuyển kế toán thuế', 17, '', '', '', '', '0-0', 0, '', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 1, '', 1326042000, '<p>Mọi th&ocirc;ng tin chi tiết li&ecirc;n hệ qua điện thoại</p>', 'Công Ty Cp Truyền Thông Gos', '115 Lê Hong Phong - Vinh- Nghệ An', '', '', '', '', '', '', '', '0386250456', '', '', '', 'Gọi điện thoại', 1323363600, 1326042000, 547, 1, 0, 12),
(320, 'Coffe Google Tuyển Nhân Viên', 9, '', '', '', '', '0-0', 0, '-Biết làm những đồ uống cơ bản.\n-Nhiệt tình với công việc.\n-Fulltime or Parttime tùy sắp xếp lịch thời gian của bạn.', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 0, '', 1326042000, '<p>Th&ocirc;ng tin chi tiết li&ecirc;n hệ qua điện thoại</p>\n<p>Lương theo thỏa thuận.</p>', 'Coffe Google', 'Số 94 Đường Nguyễn Viết Xuân - Vinh', '', '', '', '', '', '', '', '098.222.1990', '', '', '', 'Gọi điện thoại', 1323363600, 1326042000, 547, 1, 0, 7),
(321, 'CÔNG TY KHAI THÁC ĐÁ VÔI YABASHI VIỆT NAM', 22, '', '', '', '', '0-0', 0, 'Công việc làm: Gia công cơ khí\nMột tháng được nghỉ 06 ngày: 02 ngày thứ 7 và 04 ngày Chủ Nhật; và các ngày nghỉ lễ theo quy định hiện hành.\nThưởng và tăng lương: 01 lần/năm dựa theo năng lực và cống hiến.\nThời gian thử việc: 01 tháng (70% lương cơ bản).', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 14, 'Bản sơ yếu lý lịch (bản chính có xác nhận UBND xã).\nBằng tốt ngiệp, bảng điểm.(Bản sao có công chứng).\nGiấy khai sinh (Bản sao).\nĐơn xin việc (bản chính có xác nhận).\nGiấy khám sức khỏe.\nẢnh 2 tấm (4cm x 6cm).\nVà số điện thoại có thể liên lạc được.', 1326042000, '<p>Thu nhập:&nbsp;<strong>Khoảng tr&ecirc;n 2.200.000 VNĐ/Th&aacute;ng (chỗ ở C&ocirc;ng ty lo)</strong><br />(Chưa bao gồm: Trợ cấp tiền ăn, Tiền l&agrave;m th&ecirc;m-tăng ca, trợ cấp xăng xe đi l&agrave;m, phụ cấp tr&aacute;ch nhiệm, phụ cấp tiếng Nhật, chi ph&iacute; bảo hiểm x&atilde; hội).</p>\n<ul>\n<li>Lương khởi điểm: tr&ecirc;n 2.000.000 VNĐ/th (Chưa gồm phụ cấp, bảo hiểm).</li>\n<li>Phụ cấp đi l&agrave;m chuy&ecirc;n cần 50,000 VN(Nếu đi đủ c&ocirc;ng trong 01 th&aacute;ng).</li>\n<li>Phụ cấp tr&aacute;ch nhiệm (khi c&oacute; chức vụ)</li>\n<li>Trợ cấp cơm ca (Ăn tại nh&agrave; bếp c&ocirc;ng ty; c&aacute; nh&acirc;n chịu 5.000 VNĐ; C&ocirc;ng ty Chịu 5.000VNĐ/xuất ăn).</li>\n<li>Phụ cấp tiếng Nhật (C&oacute; thể đọc viết được tiếng Nhật hoặc học đến khi đọc viết được tiếng Nhật).</li>\n<li>Phụ cấp khuyến kh&iacute;ch l&agrave;m việc h&agrave;ng th&aacute;ng 150.000 VNĐ/th&aacute;ng (T&ugrave;y theo kết quả đ&aacute;nh gi&aacute; của c&aacute; nh&acirc;n c&oacute; thể tăng l&ecirc;n hoặc giảm đi).</li>\n<li>Tiền l&agrave;m th&ecirc;m, tăng ca: T&iacute;nh theo luật lao động Việt Nam</li>\n<li>Trợ cấp đi l&agrave;m (Được thanh to&aacute;n tiền xăng xe thực tế sử dụng từ chỗ ở đến chỗ l&agrave;m việc bằng xe Wave của Honda).</li>\n<li>Được đ&oacute;ng bảo hiểm x&atilde; hội, thất nghiệp, ytế v&agrave; c&aacute;c chế độ kh&aacute;c(Theo quy định hiện h&agrave;nh; C&ocirc;ng ty chịu 20% c&aacute; nh&acirc;n chịu 8.5%)</li>\n<li>Nơi l&agrave;m việc: X&atilde; Ch&acirc;u Cường, huyện Quỳ Hợp, tỉnh Nghệ An</li>\n</ul>', '0383.981180', 'Xã Châu Cường - Huyện Quỳ Hợp', '', '', '', '', 'Trần Hữu Lâm', '', '', '0383.981180', 'tran-huulam@vnn.vn', '', '', 'Mọi hình thức', 1323363600, 1326042000, 547, 1, 0, 4),
(322, 'TUYỂN DỤNG NHÂN VIÊN KINH DOANH', 19, '', '', '', '', '0-0', 0, '- Tốt nghiệp Trung cấp,cao Đẳng-Đại học các trường kinh tế, chuyên ngành quản trị kinh doanh, marketting, cntt (ưu tiên tốt nghiệp ngành Thương Mại Điện Tử )\n- Giao tiếp tốt nhanh nhẹn, năng động nhiệt tình, yêu thích công vệc kinh doanh\n- Có hoài bão, ý chí vươn lên không ngừng phát triển cùng công ty.\n- Hiểu biết về internet, sử dụng thành thạo diễn đàn, facebook, blog... (ưu tiên có kinh nghiệm kinh doanh gian hàng online)\n- Sử dụng thành thạo google, các phần mềm office,\n- Có khả năng tìm kiếm thông tin trên mạng Internet\n- Có khả năng làm việc độc lập, và làm việc nhóm\n- Chu đáo trong công việc', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 5, '', 1326042000, '<p>- Cơ hội l&agrave;m việc với c&aacute;c dự &aacute;n của c&ocirc;ng ty với c&aacute;c đối t&aacute;c lớn.&nbsp;<br />- Hỗ trợ ph&iacute; sử dụng mobile, xăng xe đi lại.<br />- Được đ&oacute;ng BHXH, BHYT theo quy định Nh&agrave; nước sau 3 th&aacute;ng l&agrave;m việc.<br />- L&agrave;m việc trong m&ocirc;i trường chuy&ecirc;n nghiệp, nhiều cơ hội học tập, ph&aacute;t triển, v&agrave; được hưởng đ&atilde;i ngộ ph&ugrave; hợp với năng lực.<br />- C&oacute; cơ hội tham gia c&aacute;c kh&oacute;a đ&agrave;o tạo, huấn luyện chuy&ecirc;n m&ocirc;n.<br />- Được đ&agrave;o tạo về kỹ năng, cảm nhận về kinh doanh, c&aacute;c kiến thức chuy&ecirc;n s&acirc;u về thương mại điện tử, internet.<br />- Lương, thưởng thỏa đ&aacute;ng, hấp dẫn theo c&ocirc;ng sức đ&oacute;ng g&oacute;p cho sự ph&aacute;t triển lớn mạnh của C&ocirc;ng ty.</p>', 'Công Ty Phát Triển Công Nghệ An Khánh', 'Phòng Dự Án - Tầng 2 - Tòa Nhà Huệ Lộc - Đ. Nguyễn Sỹ Sách - Tp Vinh - Nghệ An', '', '', '', '', '', '', '', '0388686577', 'info@ankhanhgroup.com', '', '', 'Mọi hình thức', 1323363600, 1326042000, 547, 1, 0, 5),
(323, 'Công ty cổ phần bất động sản Fadin tuyển nhân viên', 5, '', '', '', '', '0-0', 0, '', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 0, '', 1326042000, '<p><span style="font-size: medium;"><span style="color: blue;"><strong><span style="color: black;"><span>1. Trưởng bộ phận điều h&agrave;nh s&agrave;n giao dịch bất động sản</span></span></strong></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Số lượng: 01 người</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Y&ecirc;u cầu:</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Nam nh&acirc;n vi&ecirc;n, tuổi 25 - 35 tuổi.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Tốt nghiệp Cao đẳng - Đại học c&aacute;c trường kinh tế, thương mại chuy&ecirc;n ngh&agrave;nh quản trị kinh doanh, kinh doanh bất động sản trở l&ecirc;n.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) C&oacute; chứng chỉ quản l&yacute; s&agrave;n BĐS v&agrave; c&aacute;c chứng chỉ kh&aacute;c li&ecirc;n quan.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Ngoại h&igrave;nh ưa nh&igrave;n, khả năng giao tiếp tốt.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Ưu ti&ecirc;n : Đ&atilde; c&oacute; kinh nghiệm trong lĩch vực bất động sản.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Mức lương : theo thoả thuận.</span></span></span><br /><br /><span style="font-size: medium;"><span style="color: blue;"><strong><span style="color: black;"><span>2. Nh&acirc;n vi&ecirc;n kinh doanh bất động sản</span></span></strong></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Số lượng: 02 người</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Y&ecirc;u cầu:</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Nam, nữ nh&acirc;n vi&ecirc;n, tuổi 22 - 30 tuổi.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Tốt nghiệp Trung cấp, Cao đẳng - Đại học c&aacute;c trường kinh tế, thương mại chuy&ecirc;n ngh&agrave;nh quản trị kinh doanh, kinh doanh bất động sản trở l&ecirc;n.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) C&oacute; chứng chỉ kinh doanh, m&ocirc;i giới, định gi&aacute; BĐS.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Ngoại h&igrave;nh ưa nh&igrave;n, khả năng giao tiếp tốt.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) C&oacute; khả năng l&agrave;m việc độc lập</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Ưu ti&ecirc;n : Đ&atilde; c&oacute; kinh nghiệm trong lĩch vực bất động sản.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Mức lương : theo thoả thuận.</span></span></span><br /><br /><span style="font-size: medium;"><span style="color: blue;"><strong><span style="color: black;"><span>3. Chuy&ecirc;n vi&ecirc;n kinh doanh tư vấn t&agrave;i ch&iacute;nh Bất động sản</span></span></strong></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Số lượng: 10 người</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Y&ecirc;u cầu:</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Nam, nữ nh&acirc;n vi&ecirc;n, tuổi 22 - 30 tuổi.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Tốt nghiệp Trung cấp, Cao đẳng - Đại học c&aacute;c trường kinh tế, thương mại, t&agrave;i ch&iacute;nh ng&acirc;n h&agrave;ng, marketing ...</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) Ngoại h&igrave;nh ưa nh&igrave;n, khả năng giao tiếp tốt, nhiệt t&igrave;nh trong c&ocirc;ng việc.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>+) C&oacute; khả năng l&agrave;m việc độc lập, &aacute;p lực c&ocirc;ng việc.</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Ưu ti&ecirc;n : Sinh vi&ecirc;n mới ra trường hoặc c&oacute; kinh nghi&ecirc;m giao dịch với ng&acirc;n h&agrave;ng.&nbsp;</span></span></span><br /><span style="font-size: medium;"><span style="color: black;"><span>- Mức lương : Theo thoả thuận</span></span></span><br /><br /><br /><span style="font-size: medium;"><strong><span style="color: black;"><span>Hồ sơ tham gia ứng tuyển c&oacute; 2 c&aacute;ch thực hiện sau:</span></span></strong></span><br /><span style="font-size: medium;"><span style="color: black;"><span>1). Nộp hồ sơ trực tiếp tại văn ph&ograve;ng c&ocirc;ng ty theo địa chỉ tr&ecirc;n. (gặp chị Ng&agrave;)</span></span></span><br /><span style="font-size: medium;"><em><span style="color: black;"><span>( ch&uacute; &yacute;: kh&ocirc;ng li&ecirc;n lạc qua điện thoại).</span></span></em></span><br /><span style="font-size: medium;"><span style="color: black;"><span>2). Khai b&aacute;o đầy đủ th&ocirc;ng tin, ch&iacute;nh x&aacute;c v&agrave; trung thực theo mẫu sau:</span></span></span><br /><span style="color: black;"><span><span style="font-size: medium;"><br /></span></span></span><br /><span style="color: black;"><span><a href="http://www.mediafire.com/?cs2ctbszcmayqzm" target="_blank">http://www.mediafire.com/?cs2ctbszcmayqzm</a></span></span></p>\n<p><span style="color: black;"><span><span style="font-size: medium;"><span style="color: black;"><span>v&agrave; gửi về</span></span><em><span style="text-decoration: underline;"><span style="color: blue;"><span>Email:&nbsp;<a href="mailto:FadinLand.CEO@gmail.com">FadinLand.CEO@gmail.com</a></span></span></span></em></span><br /><br /><span style="font-size: medium;"><em><span style="color: black;"><span>(ch&uacute; &yacute;: Hồ sơ chứng thực sẽ tiếp nhận khi ứng vi&ecirc;n được nhận v&agrave;o l&agrave;m việc)</span></span></em></span></span></span></p>', 'Công Ty Cổ Phần Bất Động Sản Fadin', 'Lô 3b - Khu Liền Kề Vinaconex 9 - Đại Lộ Lênin - Tp Vinh', '', '', '', '', 'Chị Ngà', '', '', '038.8.688366', 'FadinLand.CEO@gmail.com', '', '', 'Mọi hình thức', 1323363600, 1326042000, 547, 1, 0, 7),
(325, 'Tuyển PG, PB làm việc bán thời gian cho Vinhpro club', 19, '', '', '', '', '0-0', 0, '- Gương mặt khả ái.\n- Nữ cao trên 1m60 từ 17 đến 25 tuổi ,Nam cao trên 1m70 từ 17-25 tuổi \n- Nhiệt tình, năng động, muốn kiếm thêm thu nhập theo hình thức bán thời gian', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 10, '', 1331226000, '<p>Th&ocirc;ng tin chung:&nbsp;<br />Họ v&agrave; t&ecirc;n :&nbsp;<br />Ng&agrave;y th&aacute;ng năm sinh :&nbsp;<br />Hộ khẩu thường tr&uacute;:<br />Số CMND :................................ ng&agrave;y cấp.......... nơi cấp.................Điện thoại li&ecirc;n hệ...............<br />Yahoo :.....................<br />Facebook:..............<br />Nghề nghiệp hiện tại:<br />C&aacute;c th&ocirc;ng tin li&ecirc;n quan :<br />- Chiều cao :<br />- C&acirc;n nặng :<br />- Số đo 3 v&ograve;ng :<br />- Size giầy :&hellip;&hellip;........<br />- Size &aacute;o :&hellip;&hellip;&hellip;&hellip;&hellip;.- Size quần :&hellip;&hellip;&hellip;&hellip;&hellip;.<br />- Ngoại ngữ :&nbsp;<br />- Kinh nghiệm đ&atilde; tham gia c&aacute;c chương tr&igrave;nh:....................................<br /><br />Giới thiệu về bản th&acirc;n :<br />- Gửi k&egrave;m theo mail tối thiểu l&agrave; 05 ảnh tự do m&agrave; m&igrave;nh y&ecirc;u th&iacute;ch (B&aacute;n th&acirc;n v&agrave; to&agrave;n th&acirc;n)</p>\n<p>Th&ocirc;ng tin c&aacute; nh&acirc;n th&iacute; sinh như: Số điện thoại, Nick yahoo, Email sẽ được ch&uacute;ng t&ocirc;i tuyệt đối bảo mật.<br />Ch&uacute;ng t&ocirc;i sẽ k&yacute; hợp đồng với bạn nhằm bảo vệ quyền lợi của bạn v&agrave; nghĩa vụ củabạn đối với ch&uacute;ng t&ocirc;i. Đừng ngần ngại nếu bạn thực sự y&ecirc;u th&iacute;ch với c&ocirc;ng việc PG, PB, MC, Lễ t&acirc;n, Người mẫu...</p>', '', '', '', '', '', '', '', '', '', '0388.905.955', 'vinhpro@vinhpro.vn', '', '', 'Gọi điện thoại', 1323363600, 1331226000, 547, 1, 0, 10),
(327, 'Tuyển NVKD', 18, '', '', '', '', '0-0', 0, '- Tham gia phát triển mạng lưới khách hàng có nhu cầu\n- Không bị áp chỉ tiêu doanh số\n- Chi tiết công việc trao đổi tại buổi phỏng vấn', 0, 82, 'Thời gian khác', '0|VND/Tháng', '0 Tháng', '', 3, '1 bộ Hồ Sơ đầy đủ, chấp nhận HS photo', 1326733200, '<ul>\n<li style="text-align: justify;"><span style="font-size: medium;"><em><span style="font-family: Times New Roman;">Đối với nh&acirc;n vi&ecirc;n kinh doanh:</span></em></span></li>\n</ul>\n<p style="text-align: justify;"><span style="font-size: medium;"><span style="font-family: Times New Roman;">&bull;&nbsp;&nbsp;&nbsp;</span>T&igrave;m kiếm c&aacute;c th&ocirc;ng tin về kh&aacute;ch h&agrave;ng, t&igrave;m kiếm v&agrave; sử dụng c&aacute;c mối quan hệ để tiếp cận&nbsp;&nbsp; kh&aacute;ch h&agrave;ng tiềm năng.</span></p>\n<p style="text-align: justify;"><span style="font-size: medium;"><span style="font-family: Times New Roman;">&bull;&nbsp;&nbsp;</span>Tư vấn, giải th&iacute;ch cho kh&aacute;ch h&agrave;ng về c&aacute;c dịch vụ do c&ocirc;ng ty đang cung cấp gồm&nbsp;&nbsp;&nbsp;<span style="font-family: times new roman,times;">(bảo tr&igrave; sửa chữa m&aacute;y t&iacute;nh, hệ thống mạng LAN, WAN, lắp đặt camera,</span><span style="font-family: times new roman,times;">&nbsp;tổng đ&agrave;i điện thoại&hellip;).</span></span></p>\n<p style="text-align: justify;"><span style="font-size: medium;"><span style="font-family: Times New Roman;">&bull;&nbsp;&nbsp;&nbsp;</span>Đ&agrave;m ph&aacute;n, thương lượng, x&uacute;c tiến v&agrave; thực hiện c&aacute;c thủ tục k&yacute; kết hợp đồng với kh&aacute;ch h&agrave;ng.</span></p>\n<p style="text-align: justify;"><span style="font-size: medium;"><span style="font-family: Times New Roman;">&bull;&nbsp;&nbsp; </span>Chăm s&oacute;c kh&aacute;ch h&agrave;ng sau b&aacute;n h&agrave;ng.</span></p>\n<p style="text-align: justify;"><span style="font-size: medium;"><strong><span style="text-decoration: underline;"><span style="font-family: Times New Roman;">Y&ecirc;u cầu:&nbsp; </span></span></strong></span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp;&nbsp;Nam, Nữ tuổi đời kh&ocirc;ng&nbsp;qu&aacute; 25t. </span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp; Nh&acirc;n vi&ecirc;n kinh doanh từ 20 tuổi đến 30 tuổi.</span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp; Tốt nghiệp Trung cấp trở l&ecirc;n. </span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp;Chấp nhận sinh vi&ecirc;n mới ra trương vừa&nbsp;học&nbsp;vừa l&agrave;m.</span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp; <strong>Giao tiếp tốt, lịch sự, chịu kh&oacute;, nhiệt t&igrave;nh.</strong><strong></strong></span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp; Đam m&ecirc; kinh doanh, nhanh nhẹn, c&oacute; sức khỏe </span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp; C&oacute; khả năng l&agrave;m việc dưới &aacute;p lực cao.&nbsp; </span></p>\n<p style="text-align: justify;"><span style="font-size: medium;"><span style="font-family: Times New Roman;">&bull;&nbsp;&nbsp;&nbsp;&nbsp; Ưu ti&ecirc;n c&aacute;c ứng vi&ecirc;n c&oacute; kinh nghiệm v&agrave; hiểu biết về mạng LAN/ WAN/ Internet&nbsp;</span><span style="font-family: Times New Roman;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; học chuy&ecirc;n ng&agrave;nh Điện tử Viễn th&ocirc;ng, C&ocirc;ng nghệ Th&ocirc;ng tin, Kế to&aacute;n, Kinh doanh&nbsp;</span><span style="font-family: Times New Roman;">hoặc đ&atilde; từng l&agrave;m thị trường.</span></span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;"><strong><span style="text-decoration: underline;">Mức lương v&agrave; phụ cấp </span></strong><strong></strong></span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp;&nbsp;Thử việc 1 th&aacute;ng.<strong></strong></span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp; Thoả thuận theo năng lực v&agrave; kinh nghiệm. </span></p>\n<p style="text-align: justify;"><span style="font-size: medium;"><strong><span style="text-decoration: underline;"><span style="font-family: Times New Roman;">Quyền lợi:&nbsp;&nbsp; </span></span></strong></span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp;&nbsp;&nbsp; Lương: mức lương thưởng cực kỳ cạnh tranh. </span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp;&nbsp; Được tham gia c&aacute;c kh&oacute;a đ&agrave;o tạo về sản phẩm v&agrave; kỹ năng.</span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&nbsp;&bull;&nbsp;&nbsp;&nbsp;Bảo hiểm nh&agrave; nước (BHYT) sau khi k&yacute; hợp đồng l&agrave;m việc.</span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp; Tham gia c&aacute;c hoạt động Phong tr&agrave;o. </span></p>\n<p style="text-align: justify;"><span style="font-family: Times New Roman; font-size: medium;">&bull;&nbsp;&nbsp; V&agrave; nhiều quyền lợi kh&aacute;c. </span></p>\n<p><span style="font-family: Times New Roman; font-size: medium;"><strong><span style="text-decoration: underline;">Nộp hồ sơ trực tiếp tại:</span></strong> Số 01, Ng&otilde; 47, H&agrave; Huy Tập, TP.Vinh, Nghệ An.&nbsp;&nbsp;</span></p>\n<p><span style="font-family: Times New Roman; font-size: medium;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Nh&acirc;n vi&ecirc;n kinh doanh l&agrave;m b&aacute;n thời gian th&igrave; t&iacute;nh theo % của hợp đồng.</span></p>\n<p><span style="font-family: Times New Roman; font-size: medium;"><span style="font-family: Times New Roman;">Để biết th&ecirc;m th&ocirc;ng tin chi tiết li&ecirc;n hệ: Chị Hi&ecirc;n : 0944.382.540 or 0972.973.390</span></span></p>', '', '', '', '', '', '', 'Thái Lương Hoài', '', '', '01678121823', 'thaihoaipc@gmail.com', '', '', 'Qua Website', 1324054800, 1326733200, 568, 1, 0, 6);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_job_bad`
--

CREATE TABLE IF NOT EXISTS `tbtt_job_bad` (
  `jba_id` int(10) NOT NULL AUTO_INCREMENT,
  `jba_title` varchar(100) NOT NULL,
  `jba_detail` text NOT NULL,
  `jba_email` varchar(50) NOT NULL,
  `jba_job` int(10) NOT NULL,
  `jba_date` int(11) NOT NULL,
  PRIMARY KEY (`jba_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=2 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_job_favorite`
--

CREATE TABLE IF NOT EXISTS `tbtt_job_favorite` (
  `jof_id` int(10) NOT NULL AUTO_INCREMENT,
  `jof_job` int(10) NOT NULL,
  `jof_user` int(10) NOT NULL,
  `jof_date` int(11) NOT NULL,
  PRIMARY KEY (`jof_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_menu`
--

CREATE TABLE IF NOT EXISTS `tbtt_menu` (
  `men_id` int(3) NOT NULL AUTO_INCREMENT,
  `men_parent` int(11) NOT NULL,
  `men_name` varchar(100) NOT NULL,
  `men_descr` varchar(100) NOT NULL,
  `men_image` varchar(150) NOT NULL,
  `men_category` int(3) NOT NULL,
  `men_order` int(3) NOT NULL,
  `men_status` int(1) NOT NULL,
  PRIMARY KEY (`men_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=120 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_notify`
--

CREATE TABLE IF NOT EXISTS `tbtt_notify` (
  `not_id` int(10) NOT NULL AUTO_INCREMENT,
  `not_title` varchar(150) NOT NULL,
  `not_group` varchar(50) NOT NULL,
  `not_degree` int(1) NOT NULL,
  `not_detail` text NOT NULL,
  `not_begindate` int(11) NOT NULL,
  `not_enddate` int(11) NOT NULL,
  `not_view` text NOT NULL,
  `not_status` int(1) NOT NULL,
  PRIMARY KEY (`not_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=5 ;

--
-- Dumping data for table `tbtt_notify`
--

INSERT INTO `tbtt_notify` (`not_id`, `not_title`, `not_group`, `not_degree`, `not_detail`, `not_begindate`, `not_enddate`, `not_view`, `not_status`) VALUES
(1, 'Khi đăng sản phẩm, đăng rao vặt, tin tuyển dụng, tin tìm việc bạn cần chú ý những gì?', '0,1,2,3', 1, '[justify][b]Khi đăng sản phẩm, đăng rao vặt, tin tuyển dụng, tin tìm việc bạn cần chú ý những điểm sau:[/b]\n\n     •  Sản phẩm được đăng có [red][b]kèm theo ảnh[/b][/red] sản phẩm, có giá bán rõ ràng sẽ được ưu tiên đưa lên trang chủ. Sản phẩm không có ảnh hay có giá bán là [red][b]Call [/b][/red]sẽ không được xuất hiện ở trang chủ.\n     •  Rao vặt được đăng có kèm theo ảnh rao vặt sẽ được khách ghé thăm quan tâm hơn. Vì vậy, bạn nên đăng kèm theo một ảnh rao vặt để làm cho tin đăng của bạn thêm nổi bật.\n     •  Tin tuyển dụng, tin tìm việc bạn cung cấp càng nhiều thông tin thì tin đăng của bạn sẽ càng được nhiều người quan tâm. Đặc biệt, bạn có thể [url=http://thitruong24gio.com/contact]liên hệ[/url] với chúng tôi để yêu cầu đánh dấu tin tuyển dụng hay tin tìm việc của bạn thành tin [red][b]đã được xác thực[/b][/red] (tin đáng tin cậy).\n     •  Khi đăng sản phẩm, rao vặt, tin tuyển dụng, tin tìm việc, bạn nên nhập đầy đủ thông tin, có như vậy sản phẩm hay tin đăng của bạn mới dễ dàng tiếp cận với khách ghé thăm.\n     •  Khi đăng sản phẩm, rao vặt, tin tuyển dụng, tin tìm việc, bạn nên đọc trước những hướng dẫn kèm theo để việc đăng sản phẩm, rao vặt, tin tuyển dụng, tin tìm việc của bạn được nhanh chóng.\n     •  Nếu bạn gặp khó khăn trong quá trình đăng sản phẩm, rao vặt, tin tuyển dụng, tin tìm việc, bạn vui lòng [url=http://thitruong24gio.com/contact]liên hệ[/url] với chúng tôi để được hỗ trợ.[/justify]', 1258390800, 1293642000, '[10][241][452]', 1),
(2, 'Một vài chức năng của website được cải tiến mới', '0,1,2,3', 1, '[justify]Tiếp thu ý kiến đóng góp của khách ghé thăm website [url=http://thitruong24gio.com]http://thitruong24gio.com[/url] về một số nhược điểm của website như mã xác nhận khó đọc, chỉ upload 1 ảnh cho 1 sản phẩm, dung lượng file ảnh bị giới hạn nhỏ (&lt; 50 KB). Hiện nay, chúng tôi đã khắc phục các nhược điểm trên như sau:\n\n[b]1. Mã xác nhận được chỉnh lại dễ đọc.\n2. Cho phép upload tối đa 3 ảnh cho 1 sản phẩm.\n3. Dung lượng file ảnh tối đa được tăng lên là 100 KB/1 ảnh.\n4. Các sản phẩm có 2 hoặc 3 ảnh khi xem chi tiết sản phẩm, các ảnh của sản phẩm sẽ được hiển thị khi click vào ảnh để xem ảnh phóng lớn.[/b][/justify]', 1259427600, 1267376400, '[10][304]', 0),
(4, 'Khởi tạo miễn phí Cửa hàng ảo', '0,1,2,3', 1, '[justify]Hiện nay chúng tôi setup miễn phí tài khoản [red]&quot;Cửa hàng ảo&quot;[/red] trên website [url=http://thitruong37.com]http://thitruong37.com[/url]. Sau khi đăng ký xong tài khoản, bạn vui lòng làm theo 1 trong 2 cách sau để chúng tôi kích hoạt tài khoản của bạn:\n\n♦ [b]Cách 1:[/b]\n\n    1. Cung cấp thông tin cửa hàng theo mẫu ([url=http://thitruong37.com/templates/guide/data/maucungcapthongtin.doc]Tải tại đây[/url]).\n    2. Gởi mẫu thông tin cửa hàng tới Email info@thitruong37.com.\n    3. Gởi kèm theo logo, banner (nếu có). \n\n♦ [b]Cách 2:[/b]\n\n    Cung cấp thông tin cửa hàng qua Yahoo Chat thitruong37 với các thông tin sau: tên tài khoản, tên cửa hàng, địa chỉ cửa hàng, điện thoại, danh mục cửa hàng. \n\n[i]Chúng tôi sẽ cài đặt Cửa hàng ảo trong vòng 24 giờ (trừ thứ bảy và chủ nhật) sau khi tiếp nhận đầy đủ thông tin cửa hàng theo yêu cầu. Bạn có thể chỉnh sửa thông tin Cửa hàng ảo qua chức năng quản lý của website.[/i][/justify]', 1323190800, 1325350800, '', 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_product`
--

CREATE TABLE IF NOT EXISTS `tbtt_product` (
  `pro_id` int(10) NOT NULL AUTO_INCREMENT,
  `pro_name` varchar(100) NOT NULL,
  `pro_descr` varchar(100) NOT NULL,
  `pro_cost` int(10) NOT NULL,
  `pro_currency` varchar(5) NOT NULL,
  `pro_hondle` int(1) NOT NULL,
  `pro_saleoff` int(1) NOT NULL,
  `pro_province` int(3) NOT NULL,
  `pro_category` int(3) NOT NULL,
  `pro_category_shop` int(11) NOT NULL,
  `pro_begindate` int(11) NOT NULL,
  `pro_enddate` int(11) NOT NULL,
  `pro_detail` text NOT NULL,
  `pro_image` varchar(150) NOT NULL,
  `pro_dir` varchar(150) NOT NULL,
  `pro_user` int(10) NOT NULL,
  `pro_poster` varchar(100) NOT NULL,
  `pro_address` varchar(100) NOT NULL,
  `pro_phone` varchar(20) NOT NULL,
  `pro_mobile` varchar(20) NOT NULL,
  `pro_email` varchar(50) NOT NULL,
  `pro_yahoo` varchar(50) NOT NULL,
  `pro_skype` varchar(50) NOT NULL,
  `pro_status` int(1) NOT NULL,
  `pro_view` int(10) NOT NULL,
  `pro_buy` int(10) NOT NULL,
  `pro_comment` int(5) NOT NULL,
  `pro_vote_cost` int(3) NOT NULL,
  `pro_vote_model` int(3) NOT NULL,
  `pro_vote_quanlity` int(3) NOT NULL,
  `pro_vote_service` int(3) NOT NULL,
  `pro_vote_total` int(3) NOT NULL,
  `pro_vote` int(5) NOT NULL,
  `pro_reliable` int(1) NOT NULL,
  `pro_embedcode` varchar(1000) NOT NULL,
  PRIMARY KEY (`pro_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3847 ;

--
-- Dumping data for table `tbtt_product`
--

INSERT INTO `tbtt_product` (`pro_id`, `pro_name`, `pro_descr`, `pro_cost`, `pro_currency`, `pro_hondle`, `pro_saleoff`, `pro_province`, `pro_category`, `pro_category_shop`, `pro_begindate`, `pro_enddate`, `pro_detail`, `pro_image`, `pro_dir`, `pro_user`, `pro_poster`, `pro_address`, `pro_phone`, `pro_mobile`, `pro_email`, `pro_yahoo`, `pro_skype`, `pro_status`, `pro_view`, `pro_buy`, `pro_comment`, `pro_vote_cost`, `pro_vote_model`, `pro_vote_quanlity`, `pro_vote_service`, `pro_vote_total`, `pro_vote`, `pro_reliable`, `pro_embedcode`) VALUES
(3811, 'Áo Cape', '', 1257000, 'VND', 0, 0, 81, 428, 0, 1323363600, 1326042000, '<p>&Aacute;o Cape chất liệu vải cao cấp, giữ ấm v&agrave; t&ocirc;n vinh vẻ đẹp bạn g&aacute;i</p>', '78d17e341b882555643688911a642ee6.jpg', '09122011', 559, 'Phan Thị Ngọc Ánh', '', '', '01669465434', 'thienthandianguc_dt@yahoo.com', '', '', 1, 25, 0, 0, 5, 5, 5, 5, 5, 1, 1, ''),
(3810, 'Sơ mi dáng dài', '', 258000, 'VND', 0, 0, 81, 428, 0, 1323363600, 1326042000, '<p>&Aacute;o sơ mi d&aacute;ng d&agrave;i kẻ caro nhiều mầu, chất liệu cao cấp, chống nhăn v&agrave; kh&ocirc;ng bị sờn khi giặt</p>', 'e5ea33700d74f7c73002b11b751bb370.jpg', '09122011', 559, 'Phan Thị Ngọc Ánh', '', '', '01669465434', 'thienthandianguc_dt@yahoo.com', '', '', 1, 25, 0, 0, 10, 10, 10, 9, 10, 1, 1, ''),
(3813, 'Cần bán chú Nouvo LX 135 Limited 2011./', '', 29000000, 'VND', 1, 0, 81, 423, 7, 1323968400, 1355590800, '<p><span style="font-size: medium;">T&igrave;nh h&igrave;nh l&agrave; em cần b&aacute;n ch&uacute; Nouvo LX 135 Limited 2011 để l&ecirc;n đời.Xe mua cuối th&aacute;ng 8/2011.Xe đ&atilde; d&aacute;n keo trong,tem v&agrave;nh v&agrave; đồ Inox.</span></p>\n<p><span style="font-size: medium;"><span style="font-size: medium;">Biển số hiện tại của n&oacute; l&agrave; 026.36.Gi&aacute; b&aacute;n 29 triệu.Ai thật sự c&oacute; nhu cầu mua th&igrave; li&ecirc;n hệ với em qua số dt 0972 745 777 - 0943 352 356.Ai đi qua đi lại &uacute;p gi&ugrave;m em.Cảm ơn!</span></span></p>', '5f32d367fd1afbd3961f2d72332c262c.png,69c2224878e7b975b2331a1520e62708.jpg,f913697715d266e9dd95c1cf2927748b.jpg', '16122011', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 11, 0, 0, 0, 0, 0, 0, 0, 0, 0, ''),
(3814, 'Bán Lattop Dell N3010 Core I3 ./', '', 8500000, 'VND', 1, 0, 81, 418, 8, 1323968400, 1355590800, '<p><span style="font-size: medium;">H&atilde;ng sản xuất: Dell Inspiron Series<br /> Độ lớn m&agrave;n h&igrave;nh: 13.3 inch<br /> Dung lượng HDD: 500GB<br /> Loại CPU: Intel Core i3-380M<br /> Tốc độ m&aacute;y: 2.53GHz (3MB L3 cache)</span></p>\n<p><span style="font-size: medium;"><span style="font-size: medium;">M&aacute;y m&igrave;nh sử dụng n&ecirc;n rất giữ,pin ngon d&ugrave;ng được cả buổi v&igrave; m&igrave;nh &iacute;t khi d&ugrave;ng đến pin(c&oacute; d&ugrave;ng th&igrave; d&ugrave;ng hết v&agrave; xạc no).Mọi chức năng đầy đủ,m&aacute;y chưa hề x&acirc;y xước.Đ&atilde; hết bảo h&agrave;nh của Hồng h&agrave;,m&aacute;y đầy đủ PK.Gi&aacute; b&aacute;n 8 triệu.Ai quan t&acirc;m th&igrave; call cho em 0972 745 777 - 0943 352 356 ./</span></span></p>', 'c184cecc77c9515dd5d0a0c59b4b1d07.jpg,17ffc91ffb7aafdab5c4086705d2d0de.jpg,b711c7276bbd59a84c5ad5f0f01c0676.jpg', '16122011', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 9, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3815, 'Môi giới và Nhận kí gửi Bất động sản', '', 1, 'VND', 1, 1, 81, 433, 6, 1323968400, 1352998800, '<p>Nhận k&yacute; gửi, mua b&aacute;n, cho thu&ecirc; bất động sản tại TP Vinh v&agrave; c&aacute;c huyện l&acirc;n cận. Qu&yacute; kh&aacute;ch h&agrave;ng c&oacute; bất động sản cần b&aacute;n, cho thu&ecirc; h&atilde;y li&ecirc;n hệ ngay với ch&uacute;ng t&ocirc;i. Qu&yacute; kh&aacute;ch h&agrave;ng sẽ được:</p>\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Tư vấn miến ph&iacute; thủ tục ph&aacute;p l&yacute;, giấy tờ... li&ecirc;n quan đến bất động sản</p>\n<p>-&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Quay phim, chụp h&igrave;nh v&agrave; đăng tải tr&ecirc;n website:&nbsp;http://thitruong37.com/batdongsannghean miễn ph&iacute;.</p>\n<p>Với ti&ecirc;u ch&iacute; hoạt động:&nbsp;<em>"Sự th&agrave;nh c&ocirc;ng v&agrave; ph&aacute;t triển bền vững của kh&aacute;ch h&agrave;ng ch&iacute;nh l&agrave; th&agrave;nh c&ocirc;ng của c&ocirc;ng ty ch&uacute;ng t&ocirc;i"</em>, c&ocirc;ng ty ch&uacute;ng t&ocirc;i lu&ocirc;n đặt quyền lợi của kh&aacute;ch h&agrave;ng l&ecirc;n h&agrave;ng đầu, cam kết mang đến cho kh&aacute;ch h&agrave;ng những Hiệu Quả Tốt Nhất, Gi&aacute; Trị Cao Nhất tr&ecirc;n từng sản phẩm giao dịch.</p>', 'cd31418cf651794d67fe8ec79c81b11c.jpg', '16122011', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 8, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3819, 'Cần bán 100,7M2 tại K5 - P.Lê Lợi - TP Vinh ./', '', 14000000, 'VND', 1, 0, 81, 433, 11, 1324054800, 1355677200, '<p><span class="xanhxam">T&ocirc;i c&oacute; nhu cầu muốn b&aacute;n 100,7 m2 đất tại K5 &ndash; P.L&ecirc; Lợi &ndash; TP Vinh. Rộng trước b&aacute;m đường 5,2m chiều s&acirc;u 20m Hướng ch&iacute;nh nam.Đường d&acirc;n cư rộng 5m. Sổ đỏ ch&iacute;nh chủ. Ai c&oacute; nhu cầu mua li&ecirc;n hệ Mr Tuấn 0972 745 777 - 0943 352 356. Cảm ơn đ&atilde; đọc tin! </span></p>', '02aed228c01e3dfada7adb405c4c1259.jpg', '17122011', 569, 'Trần Anh Tuấn', '', '', '0972.745.777', 'tuantran.qh590@yahoo.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3818, 'Bán đất tại K.Xuân Bắc – P.Hưng Dũng - TP Vinh./', '', 1250000000, 'VND', 1, 0, 81, 433, 11, 1324054800, 1355677200, '<p>&ocirc;i c&oacute; nhu cầu b&aacute;n 1 l&ocirc; đất diện t&iacute;ch 70,8m2 tại K.Xu&acirc;n Bắc &ndash; P.Hưng Dũng ( Chưa đến BV Nhi th&igrave; rẽ phải v&agrave;o khoảng 300m,từ ng&atilde; rẽ đến BV Nhi khoảng 200m) k&iacute;ch thước 4m x 17m, hướng t&acirc;y bắc, đường trước mặt rộng 15m. Mảnh đất th&ocirc;ng tho&aacute;ng thuận tiện đi lại.<br /><br /> Sổ đỏ ch&iacute;nh chủ, gi&aacute; 1 tỷ250triệu.<br /><br /> Ai thật sự c&oacute; nhu cầu li&ecirc;n hệ với Mr Tuấn qua số điện thoại 0972 745 777 &ndash; 0943 352 356 ./</p>', 'd1e0b26a3861df0a01bd1d630bb8c498.jpg', '17122011', 569, 'Trần Anh Tuấn', '', '', '0972745777', 'tuantran.qh590@yahoo.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3820, 'Bán 319M2(10,5x30) đã có nhà 1,5Tầng lối 2 đường Hồng Bàng giá 2tỷ ./', '', 2000000000, 'VND', 1, 0, 81, 433, 13, 1324054800, 1355677200, '<p><strong><span style="color: black;"><span style="color: black;"><span style="font-size: small;">Vị tr&iacute; khối T&acirc;n Phong - L&ecirc; Mao - TP Vinh (lối 2 đường Hồng B&agrave;ng,tam gi&aacute;c quỷ đi lại 70m rẽ v&agrave;o ng&otilde;), gần chợ C4, Quang Trung.Đất rộng 320m2 c&oacute; b&igrave;a đỏ ch&iacute;nh chủ. K&iacute;ch thước: 10,5 m x 30m(Nở hậu sau). Hiện tại đ&atilde; c&oacute; nh&agrave; 1,5 tầng rộng 80m2 đầy đủ tiện nghi.Nh&agrave; hướng nam chếch t&acirc;y,c&oacute; thể chia th&agrave;nh 2 l&ocirc; đất để b&aacute;n(Rộng 5m v&agrave; rộng 6m,d&agrave;i 30m).Trong lối chỉ c&oacute; 3 ng&ocirc;i nh&agrave; n&ecirc;n rất y&ecirc;n tĩnh,vườn rộng.V&igrave; đang cần b&aacute;n gấp n&ecirc;n để tr&aacute;nh mất thời gian cho kh&aacute;ch h&agrave;ng hiện trạng đường trước mặt chỉ đủ cho d&ograve;ng xe matiz hay Yaris v&agrave;o được,từ ngo&agrave;i đường HB đi v&agrave;o nh&agrave; khoảng 50m.Mong muốn li&ecirc;n hệ với kh&aacute;ch c&oacute; nhu cầu mua thật sự.Gi&aacute; b&aacute;n hữu nghị 2tỷ.Cần b&aacute;n gấp!<br /> Li&ecirc;n hệ: Mr Tuấn 0972 745 777 &ndash; 0943 352 356./</span></span></span></strong></p>', '1ee4326d2555d2468a514bceec77b6cd.jpg', '17122011', 569, 'Trần Anh Tuấn', '', '', '0972.745.777', 'tuantran.qh590@yahoo.com', '', '', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3821, 'MÀN HÌNH LCD SAMSUNG AS350', '', 2365000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>Kích Thước Màn Hình 18,5 inch</p>\n<p>Đ&ocirc;̣ Ph&acirc;n Giải 1366x768 pixel</p>\n<p>H&ocirc;̃ Trợ 16,7 Tri&ecirc;̣u Màu</p>\n<p>SẢN PH&Acirc;̉M GIÁ CẠNH TRANH</p>\n<p><span style="text-decoration: line-through;">GIÁ 2.450.000</span></p>\n<p>KM 2.365.000 vnd</p>', 'd75682d4edf7b09431f6a9703f771c3e.jpg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 5, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3822, 'MÀN HÌNH LCD SAMSUNG 19A100N', '', 2435000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>MÀN HÌNH LCD 18"5 INCH</p>\n<p><span style="text-decoration: line-through;">GIÁ BÁN 2.435.000 VND</span></p>\n<p>KHUY&Ecirc;́N MÃI: 2.325.000VND</p>', 'eb8612b2fa855562d8818606af6e5c26.jpg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 5, 0, 1, 0, 0, 0, 0, 0, 0, 1, ''),
(3823, 'MÀN HÌNH LCD AOC N950SW', '', 1980000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>AOC LCD MONITOR 18"5 INCH WIDE N950SW</p>\n<p>KÍCH THƯỚC : 18"5 INCH</p>\n<p>THỜI GIAN ÁP ỨNG 5MS</p>\n<p>Đ&Ocirc;̣ PH&Acirc;N GIẢI 1366X768</p>\n<p>GÓC NHÌN 170 Đ&Ocirc;̣</p>\n<p>SẢN PH&Acirc;̉M GIÁ CẠNH TRANH</p>\n<p><span style="text-decoration: line-through;">GIÁ BÁN:2.120.000 VND</span></p>\n<p>KHUY&Ecirc;́N MẠI:1980.000</p>', 'cf8df56bda1cd9c277db1fb175a859bc.jpg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3824, 'MÀN HÌNH LCD HKC 9809A', '', 1920000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1326733200, '<p>MÀN HÌNH LCD 18"5 INCH</p>\n<p>Đ&Ocirc;̣ PH&Acirc;N GIẢI 1360X768</p>\n<p>THỜI GIAN ÁP ỨNG 5MS</p>\n<p>Đ&Ocirc;̣ TƯƠNG PHẢN 700:1</p>\n<p>Đ&Ocirc;̣ SÁNG 250CD/M2</p>\n<p>ĐAT TI&Ecirc;U CHU&Acirc;̉N EC/ROHS</p>\n<p>BẢO HÀNH 24 THÁNG</p>\n<p><span style="text-decoration: line-through;">NY:2040000 VND</span></p>\n<p>KM:1920000 VND</p>\n<p>SẢN PH&Acirc;̉M GIÁ CẠNH TRANH</p>', '8d729038f6c184c23737d42600b23c2f.jpg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3840, 'PHẦN MỀM DIỆT VIRUS PC VIM', '', 215000, 'VND', 1, 1, 81, 419, 0, 1324054800, 1325264400, '<p>PH&Acirc;̀N M&Ecirc;̀M VIRUS PC VIM</p>\n<p>AN TOÀN TỪNG CLICK</p>\n<p><span style="text-decoration: line-through;">NY: 330 000 VND</span></p>\n<p>KM: 215.000 VND</p>', 'e35194d0c5f8b4e233d6b8faf019a580.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3841, 'PHẦN MỀM DIỆT VIRUS PANDA', '', 105000, 'VND', 0, 1, 81, 419, 0, 1324054800, 1326733200, '<p>PH&Acirc;̀N M&Ecirc;̀M VIRUS PANDA</p>\n<p>AN TOÀN TỪNG CKICK</p>\n<p><span style="text-decoration: line-through;">NY: 150000 VND</span></p>\n<p>KM:105000 VND</p>', '22b052d0f989db6712057ebf4ec8707b.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3826, 'MÀN HÌNH LCD AOC N1650', '', 1680000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>MÀN HÌNH AOC 15"6 INCH</p>\n<p>Đ&Ocirc;̣ PH&Acirc;N GIẢI 1366X768</p>\n<p>Đ&Ocirc;̣ SÁNG:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 259CD/M2</p>\n<p>Đ&Ocirc;̣ TƯƠNG PHẢN 500</p>\n<p>THỜI GIAN ĐÁP ỨNG 8MS</p>\n<p>BẢO HÀNH 24 THÁNG</p>\n<p><span style="text-decoration: line-through;">NY:1720000 VND</span></p>\n<p>KM:1680000 VND</p>', '6f0ca5afddf77aab1540d909afd77aec.jpg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3843, 'Nano chống nước bám kính ô tô', '', 659000, 'VND', 0, 1, 81, 423, 0, 1324054800, 1355677200, '<p><span>Bao gồm: 1 chất l&agrave;m sạch, 1 nano phủ bảo vệ v&agrave; 2 miếng bọt xốp l&agrave;m sạch. D&ugrave;ng chất l&agrave;m sạch để l&agrave;m sạch bề mặt k&iacute;nh ho&agrave;n to&agrave;n. Sau khi l&agrave;m sạch, d&ugrave;ng Nano phủ l&ecirc;n bề mặt k&iacute;nh, lớp nano tạo th&agrave;nh 1 m&agrave;ng cứng trong suốt sau khi kh&ocirc;.</span></p>\n<p>&nbsp;</p>\n<p><span>Kết quả: H&igrave;nh th&agrave;nh 1 cấu tr&uacute;c vững chắc l&agrave;m tăng độ cứng bề mặt k&iacute;nh, gi&uacute;p lấp đầy</span></p>\n<p><span>những lỗ nhỏ li ti tr&ecirc;n bề mặt, tạo sự trơn l&aacute;ng v&agrave; trong suốt.</span></p>\n<p><span>Lợi &iacute;ch: Tăng tầm nh&igrave;n gi&uacute;p quan s&aacute;t được r&otilde; r&agrave;ng. Kh&ocirc;ng b&aacute;m nước, gi&uacute;p hạn chế sử dụng gạt nước khi đi với tốc độ 60km/h trở l&ecirc;n. Chống b&aacute;m nước, b&aacute;m bẩn. Dễ d&agrave;ng l&agrave;m sạch b&ugrave;n đất, nhựa c&acirc;y, tuyết b&aacute;m tr&ecirc;n bề mặt k&iacute;nh.</span></p>\n<p>&nbsp;</p>\n<p><span>Sản phẩm c&oacute; độ bền tối thiểu 12 th&aacute;ng sau khi phủ.</span></p>\n<p><span>Testresults: TUV &ndash; chứng nhận chất lượng.</span></p>\n<p>&nbsp;</p>\n<p><span>M&atilde; h&agrave;ng: 73905</span></p>\n<p><span>Xuất xứ: NIGRIN/ Germany</span></p>', 'c0e8e1d299094c68a7339c2925b8ff24.jpg', '17122011', 573, 'Hồ Sỹ Châu', '', '', '0917310655', 'chauhs87@gmail.com', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3842, 'CASE PC(INTEL DUALCORE E3400)', '', 3540000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>B&Ocirc;̣ CASE PC</p>\n<p>CHIPSET INTEL G41</p>\n<p>DDRAM III 2GB</p>\n<p>HDD 80GB</p>\n<p>PHÍM + CHU&Ocirc;̣T</p>\n<p>HÀNG CẠNH TRANH</p>', '4f65b3b6c218926164acee1e6e596bbf.png', '18122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 6, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3831, 'MÁY IN CANON 2900', '', 2740000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>MÁY IN CANON LASER LBP 2900</p>\n<p>LOAI IN LASER ĐEM TRÁNG CỠ GI&Acirc;́Y A4</p>\n<p>Đ&Ocirc;̣ PH&Acirc;N GIẢI 600DPI</p>\n<p>T&Ocirc;́C Đ&Ocirc;̣ IN TỜ/PHÚT</p>\n<p>BẢO HÀNH 24 THÁNG</p>\n<p><span style="text-decoration: line-through;">NY: 2850000 VND</span></p>\n<p>KM:2740000VND</p>', '5d9913659df8973dd6980ab5d09ddc2c.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3832, 'MAY IN BROTHER HL2130', '', 2050000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p style="text-align: center;">MÁY IN LASER BROTHER HL_2130</p>\n<ul>\n<li>IN KH&Ocirc;̉&nbsp; A4</li>\n<li>T&Ocirc;́C Đ&Ocirc;̣ 20 TRANG/PHÚT</li>\n<li>Đ&Ocirc;̣ PH&Acirc;N GIẢI 2400X600 DPI</li>\n<li>B&Ocirc;̣ XỬ LÝ 200 HZ</li>\n<li>B&Ocirc; NHỚ TRONG 8MB</li>\n<li>C&Ocirc;NG SU&Acirc;T 421W</li>\n<li>KH&Ocirc;́I LƯƠNG 5,5KG</li>\n<li style="text-align: left;">BẢO&nbsp; HÀNH 24 THÁNG</li>\n<li style="text-align: left;"><span style="text-decoration: line-through;">NY:2250000 VND</span></li>\n<li style="text-align: left;">KM:2050000VND</li>\n</ul>', 'none.gif', 'default', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3833, 'MAY IN BROTHER HL2130', '', 2050000, 'VND', 1, 1, 81, 418, 0, 1324054800, 1325264400, '<p style="text-align: center;">MAY IN BROTHER HL_2130</p>\n<ul>\n<li>IN KH&Ocirc;̉ GI&Acirc;́Y A4</li>\n<li>T&Ocirc;́C Đ&Ocirc;̣ 20TRANG/PHÚT</li>\n<li>Đ&Ocirc;̣ PH&Acirc;N GIẢI 2400/600 DPI</li>\n<li>B&Ocirc;̣ VI XỬ LÝ 200MHZ</li>\n<li>B&Ocirc;̣ NHỚ TRONG 8MB</li>\n<li>C&Ocirc;NG SU&Acirc;́T 41W</li>\n<li>KH&Ocirc;́I LƯỢNG 5,5 KG</li>\n<li>BẢO HANH 24 THANG</li>\n<li><span style="text-decoration: line-through;">NY: 2250000 VND</span></li>\n<li>KM 2050000VND</li>\n</ul>', '2c92546c7baeb530c53066e597b30021.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3834, 'MÁY IN BRTHER HL 2240D', '', 2545000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>MÁY IN BROTHER HL 2240D</p>\n<p>LOẠI MÁY LASER ĐEN TRẮNG CỠ GI&Acirc;́Y A4</p>\n<p>Đ&Ocirc;̣ PH&Acirc;N GIẢI 2400/600DPI</p>\n<p>T&Ocirc;́C Đ&Ocirc;̣ IN 26 TỜ/PHUT</p>\n<p>KHAY ĐỰNG GI&Acirc;́Y 260 TỜ</p>\n<p>CHƯC NĂNG IN 2 MẶT IN TRỰC TI&Ecirc;́P</p>\n<p>B&Ocirc;̣ NHỚ TRONG 32MB</p>\n<p>BẢO HÀNH 3 NĂM</p>\n<p><span style="text-decoration: line-through;">NY:2650 000 VND</span></p>\n<p>KM:2545000 VND</p>', '1fb32726ed4f25175f2d6f4621d8c751.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3835, 'MAY IN BROTHER HL 2270D', '', 2865000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>MAÝ IN BROTHER 2270D</p>\n<p>CHƯC NĂNG IN LASER ĐEN TRẮNG, KH&Ocirc;̉ GI&Acirc;́Y MA4</p>\n<p>T&Ocirc;C Đ&Ocirc;̣ IN 22 TỜ/PHÚT</p>\n<p>Đ&Ocirc;̣ PH&Acirc;N GIẢI 2400/600 DPI</p>\n<p>KHAY ĐỰNG GI&Acirc;́Y 260 TỜ</p>\n<p>CHỨC NĂNG IN HAI MẶT</p>\n<p>BẢO HÀNH 36 THÁNG</p>\n<p><span style="text-decoration: line-through;"><span style="text-decoration: underline;">NY:3000000 VND</span></span></p>\n<p>KM:2865000 VND</p>', '8c6058a42cf6a6ac03997fde003c7604.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3836, 'MAY IN SANSUNG 1640', '', 1765000, 'VND', 0, 1, 81, 418, 0, 1324054800, 1325264400, '<p>MÁY IN SAMSUNG LASER ML 1640</p>\n<p>LOẠI MÁY IN LASER DDEN TRẮNG CỠ GI&Acirc;́Y A4</p>\n<p>Đ&Ocirc;̣ PH&Acirc;N GIẢI 1200/600DPI</p>\n<p>B&Ocirc;̣ XỬ&nbsp; LÝ&nbsp; 150MHZ</p>\n<p>K&Ecirc;́T N&Ocirc;́I C&Ocirc;̉NG USB</p>\n<p>KH&Ocirc;́I LƯƠNG TỊNH 5,7 KG</p>\n<p>BÁO HÀNH 3 NĂM</p>\n<p><span style="text-decoration: line-through;">NY:1980000 VND</span></p>\n<p>KM:1765000 VND</p>', '2121072bd0206d3b22fd2208ec927e8a.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3837, 'PHẦN MỀM DIỆT VIRUS KASPERSKY', '', 160000, 'VND', 0, 1, 81, 419, 0, 1324054800, 1325264400, '<p>PH&Acirc;̀N M&Ecirc;̀M DI&Ecirc;T VIRUS KASPERSKY</p>\n<p>AN TOÀN TƯNG CLICK</p>\n<p><span style="text-decoration: line-through;">NY:185000 VND</span></p>\n<p>KM:16000</p>', 'eacb6e96fc06aa4504bef29da6d9c924.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3838, 'PHÂN MỀN DIỆT VIUS BKAV PRO', '', 185000, 'VND', 0, 1, 81, 419, 0, 1324054800, 1325264400, '<p>PH&Acirc;̀N M&Ecirc;̀M DI&Ecirc;̣T VIRUS BKAV PRO</p>\n<p>AN TOÀN TỪNG CLICK</p>\n<p><span style="text-decoration: line-through;">NY: 199000 VND</span></p>\n<p>KM: 185000 VND</p>', '712379b2095761d6750826fbb41bf6d2.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3839, 'PHẦN MỀM DIỆT VIRUS CMC', '', 120000, 'VND', 0, 1, 81, 419, 0, 1324054800, 1325264400, '<p>PH&Acirc;̀N M&Ecirc;̀M DI&Ecirc;T VIRUS CMC</p>\n<p>AN TOÀN TỪNG CLICK</p>\n<p><span style="text-decoration: line-through;">NY:200000 VND</span></p>\n<p>KM:120000 VND</p>', '6f24bd1456409380da77d344d863cc6c.jpeg', '17122011', 565, 'Nguyễn Quang Huy', '', '', '0943357668', 'dananganhtai@gmail.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3844, 'Sản phẩm tiết kiệm gas Ecogas', '', 450000, 'VND', 0, 1, 81, 434, 0, 1324054800, 1355677200, '<p>THIẾT BỊ TIẾT KIỆM GAS - ECOGAS của ch&uacute;ng t&ocirc;i gi&uacute;p bạn tiết kiệm đến 25% lượng gas ti&ecirc;u thụ. Một bước tiến vượt bậc trong giải ph&aacute;p tiết kiệm năng lượng v&agrave; mang đến lợi &iacute;ch cực lớn về kinh tế.&nbsp;<br />Sản phẩm TIẾT KIỆM GAS - ECOGAS đ&atilde; được ch&uacute;ng t&ocirc;i sản xuất với một ti&ecirc;u ch&iacute; &ldquo;Tiết kiệm chi ph&iacute;, giảm ng&acirc;n s&aacute;ch chi ti&ecirc;u&rdquo; của từng gia đ&igrave;nh, nh&agrave; h&agrave;ng , kh&aacute;ch sạn, bếp ăn C&ocirc;ng nghiệp &hellip;đặc biệt trong thời kỳ khủng hoảng kinh tế &amp; vật gi&aacute; leo thang hiện nay.&nbsp;<br />* Sản phẩm đảm bảo g&igrave; cho người ti&ecirc;u d&ugrave;ng - Tiết kiệm đến 30&permil; chi ph&iacute; cho việc sử dụng gas. - Sản phẩm của ch&uacute;ng t&ocirc;i đ&atilde; được Cục sở hữu tr&iacute; tuệ cấp bằng độc quyền s&aacute;ng chế tr&ecirc;n to&agrave;n l&atilde;nh thổ Việt Nam, đạt được giải 3 cuộc thi S&aacute;ng tạo Việt 2009.&nbsp;<br />- Đặc biệt sản phẩm TIẾT KIỆM GAS được bảo h&agrave;nh l&ecirc;n đến 02 năm v&agrave; được bảo hiểm chống ch&aacute;y, nổ l&ecirc;n đến 100 triệu đồng/vụ.&nbsp;<br />- Quan trọng hơn, khi bạn d&ugrave;ng ECOGAS được thiết kế đặc biệt chống ch&aacute;y ngược; việc n&agrave;y đồng nghĩa với giảm tối đa khả năng ch&aacute;y nổ n&ecirc;n rất an to&agrave;n khi sử dụng.&nbsp;<br />- Đồng thời khi bạn sử dụng sản phẩm tiết kiệm gas l&agrave; đ&atilde; đồng nghĩa với việc bạn đ&atilde; bảo vệ được sức khỏe cho gia đ&igrave;nh m&igrave;nh g&oacute;p phần bảo vệ m&ocirc;i trường sống của ch&uacute;ng ta. Bởi sản phẩm tiết kiệm gas Ecogas hạn chế tối đa lượng &ldquo;gas sống&rdquo; ra m&ocirc;i trường, l&agrave;m giảm thiểu kh&iacute; thải độc hại. * Tại sao lại c&oacute; thể gi&uacute;p được tiết kiệm lớn đến như thế? - Thiết bị được lắp đặt v&agrave;o bếp gas c&ocirc;ng nghiệp hoặc bếp gas gia đ&igrave;nh nhằm t&aacute;c động trực tiếp l&ecirc;n chuỗi ph&acirc;n tử Hydrocacbon trong gas l&agrave;m cho Oxy dễ tiếp x&uacute;c với gas hơn, qua đ&oacute; gas trong b&igrave;nh dễ ch&aacute;y v&agrave; ch&aacute;y triệt để hơn. Đặc biệt khi sử dụng sản phẩm tiết kiệm Gas th&igrave;:&nbsp;<br />+ Ngọn lửa ch&aacute;y xanh v&agrave; đều hơn. (Tăng 20&permil; hiệu suất nhiệt của bếp gas) + Giảm thời gian đun nấu. (Tương đương giảm 10&permil; thời gian nấu )  Ưu điểm vượt trội khi d&ugrave;ng sản phẩm: - L&agrave;m mất m&ugrave;i gas sống. Giảm kh&iacute; thải độc hại. - Gi&uacute;p sử dụng gas an to&agrave;n hơn với bộ phận chống ch&aacute;y ngược được thiết kế b&ecirc;n trong. - Được chế tạo bằng Inox 100&permil; kh&ocirc;ng rỉ. Bền bỉ theo thời gian. - Tiết kiệm từ 20&permil; - 30&permil; lượng gas ti&ecirc;u thụ h&agrave;ng th&aacute;ng.<br />Kh&aacute;ch h&agrave;ng ti&ecirc;u biểu: - Nh&agrave; m&aacute;y gạch Viglacera<br />- Nh&agrave; m&aacute;y gạch S&oacute;c sơn - Nh&agrave; m&aacute;y gạch Thạch B&agrave;n<br />- Gốm sứ B&aacute;t Tr&agrave;ng<br />- Si&ecirc;u thị Coopmart B&igrave;nh Dương<br />- Nh&agrave; m&aacute;y Kinh Đ&ocirc; (KCN Việt Nam &ndash; Singapore)<br />- C&ocirc;ng ty TNHH Mabuchi Motor Vietnam (KCN Bi&ecirc;n H&ograve;a 2)<br />- KCN Mỹ Xu&acirc;n: Gạch men nh&agrave; &Yacute;, Inax Việt Nam, t&ocirc;n Hoa sen, giấy S&agrave;i G&ograve;n&hellip;<br />- Si&ecirc;u thị Coopmart Đinh Ti&ecirc;n Ho&agrave;ng<br />- Nh&agrave; h&agrave;ng c&ocirc;ng vi&ecirc;n nước Đầm Sen<br />- Nh&agrave; h&agrave;ng Hội qu&aacute;n S&agrave;i G&ograve;n<br />- Chi hội từ thiện Bảo H&ograve;a<br />- C&ocirc;ng ty TNHH GUNZE &ndash; Khu chế xuất T&acirc;n Thuận<br /><br />Hiện tại ch&uacute;ng t&ocirc;i đang cần hợp t&aacute;c ph&acirc;n phối sản phẩm với c&aacute;c đại l&yacute; ở c&aacute;c tỉnh th&agrave;nh, chiết khấu đại l&yacute; hấp dẫn</p>', 'e2bc0e82336a518105aa4b63079b0a17.jpg', '17122011', 573, 'Hồ Sỹ Châu', '', '', '0917310655', 'chauhs87@gmail.com', '', '', 1, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3845, 'Tấm lót mũ bảo hiểm bằng mây', '', 25000, 'VND', 0, 1, 81, 428, 0, 1324054800, 1355677200, '<table style="width: 580px;" border="0" cellspacing="0" cellpadding="0">\n<tbody>\n<tr>\n<td class="main_detail">Sản phẩm được l&agrave;m từ sợi m&acirc;y thi&ecirc;n nhi&ecirc;n, được thợ thủ c&ocirc;ng tết bện tỷ mỉ, c&oacute; lỗ s&agrave;ng nhỏ l&agrave;m tho&aacute;ng, được định h&igrave;nh kiểu d&aacute;ng bằng m&aacute;y c&ocirc;ng nghiệp trước khi sử dụng,<br />Sợi m&acirc;y b&oacute;ng, bền đẹp, th&acirc;n thiện với m&ocirc;i trường, d&eacute; lau ch&ugrave;i khi bẩn.<br />Sợi m&acirc;y l&agrave;m cho t&oacute;c, da đầu kh&ocirc;ng tiếp x&uacute;c trực tiếp với phần th&acirc;n mũ của vải của mũ n&ecirc;n kh&ocirc;ng d&iacute;nh t&oacute;c kể cả khi d&ugrave;ng gel, v&igrave; thế l&agrave;m m&aacute;t da đầu m&ugrave;a hề, ấm v&agrave;o m&ugrave;a đ&ocirc;ng, kh&ocirc;ng đọc hại giảm thiểu tối đa khi tiếp x&uacute;c với phần nhựa cứng của mũ.<br />Bạn sẽ thật thoải m&aacute;i khi mang mũ bảo hiểm tr&ecirc;n đầu c&ugrave;ng sản phẩm miếng l&oacute;t R027 của ch&uacute;ng t&ocirc;i</td>\n</tr>\n</tbody>\n</table>', '8c4e6e9d8d29c993f6a7a86dde25e0c9.Jpeg', '17122011', 573, 'Hồ Sỹ Châu', '', '', '0917310655', 'chauhs87@gmail.com', '', '', 1, 3, 0, 0, 0, 0, 0, 0, 0, 0, 1, ''),
(3846, 'Bảo vệ gầm ô tô', '', 495000, 'VND', 0, 1, 81, 423, 0, 1324054800, 1355677200, '<p><span>Sản phẩm gốc Bitumen c&oacute; chứa PVC. C&oacute; t&iacute;nh đ&agrave;n hồi tốt, độ bền l&acirc;u d&agrave;i, bền bỉ bảo vệ xe khỏi sự ăn m&ograve;n do nước, muối, hơi ẩm v&agrave; đ&aacute; văng. Sản phẩm c&oacute; t&aacute;c dụng c&aacute;ch &acirc;m. Tương th&iacute;ch với nhựa PVC v&agrave; kh&ocirc; nhanh ch&oacute;ng. Rất ph&ugrave; hợp với việc xử l&yacute; chuy&ecirc;n nghiệp. Được sử dụng chung với s&uacute;ng bắn c&oacute; hệ thống kh&iacute; n&eacute;n. L&yacute; tưởng cho việc bảo vệ bổ sung hệ thống khung gầm.</span></p>\n<p>&nbsp;</p>\n<p><span><strong>C&aacute;ch d&ugrave;ng</strong>: Vệ sinh gầm xe sạch sẽ. Đảm bảo gầm xe kh&ocirc; r&aacute;o trước khi xịt. Che chắn ống xả v&agrave; c&aacute;c chi tiết: l&aacute;p, nh&uacute;n, giắc điện. Vặn s&uacute;ng phun d&ugrave;ng hơi v&agrave;o chai phủ gầm v&agrave; tiến h&agrave;nh xịt đều tay l&ecirc;n bề mặt cần xử l&yacute;.</span></p>\n<p>&nbsp;</p>\n<p><span>M&atilde; h&agrave;ng: 74035. Đ&oacute;ng g&oacute;i: 1000ml - 6chai/th&ugrave;ng.</span></p>\n<p><span>M&atilde; h&agrave;ng: 74061. Đ&oacute;ng g&oacute;i: 2,5kg - 6lon/th&ugrave;ng.</span></p>\n<p><span>Xuất xứ: NIGRIN/ Germany</span></p>', 'none.gif', 'default', 573, 'Hồ Sỹ Châu', '', '', '0917310655', 'chauhs87@gmail.com', '', '', 1, 2, 0, 0, 0, 0, 0, 0, 0, 0, 1, '');

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_product_bad`
--

CREATE TABLE IF NOT EXISTS `tbtt_product_bad` (
  `prb_id` int(10) NOT NULL AUTO_INCREMENT,
  `prb_title` varchar(100) NOT NULL,
  `prb_detail` text NOT NULL,
  `prb_email` varchar(50) NOT NULL,
  `prb_product` int(10) NOT NULL,
  `prb_date` int(11) NOT NULL,
  PRIMARY KEY (`prb_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=3 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_product_comment`
--

CREATE TABLE IF NOT EXISTS `tbtt_product_comment` (
  `prc_id` int(10) NOT NULL AUTO_INCREMENT,
  `prc_title` varchar(100) NOT NULL,
  `prc_comment` text NOT NULL,
  `prc_product` int(10) NOT NULL,
  `prc_user` int(10) NOT NULL,
  `prc_date` int(11) NOT NULL,
  PRIMARY KEY (`prc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=21 ;

--
-- Dumping data for table `tbtt_product_comment`
--

INSERT INTO `tbtt_product_comment` (`prc_id`, `prc_title`, `prc_comment`, `prc_product`, `prc_user`, `prc_date`) VALUES
(20, 'Bạn nên đưa thông tin chi tiết nhiều hơn', 'Bạn nên đưa thêm thông tin khác cho người mua tìm hiểu kỹ hơn về sản phẩm của bạn, chẳng hạn như là Hãng sx, chế độ bảo hành, đặc tính kỹ thuật ...\nChúc bạn mua may bán đắt', 3822, 547, 1324091280);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_product_favorite`
--

CREATE TABLE IF NOT EXISTS `tbtt_product_favorite` (
  `prf_id` int(10) NOT NULL AUTO_INCREMENT,
  `prf_product` int(10) NOT NULL,
  `prf_user` int(10) NOT NULL,
  `prf_date` int(11) NOT NULL,
  PRIMARY KEY (`prf_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=26 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_province`
--

CREATE TABLE IF NOT EXISTS `tbtt_province` (
  `pre_id` int(3) NOT NULL AUTO_INCREMENT,
  `pre_name` varchar(50) NOT NULL,
  `pre_order` int(3) NOT NULL,
  `pre_status` int(1) NOT NULL,
  PRIMARY KEY (`pre_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=85 ;

--
-- Dumping data for table `tbtt_province`
--

INSERT INTO `tbtt_province` (`pre_id`, `pre_name`, `pre_order`, `pre_status`) VALUES
(81, 'Toàn Quốc', 1, 1),
(80, 'Bình Dương', 8, 1),
(74, 'TP.HCM', 2, 1),
(75, 'Hà Nội', 3, 1),
(76, 'Hải Phòng', 3, 1),
(77, 'Đà Nẵng', 5, 1),
(78, 'Cần Thơ', 6, 1),
(79, 'Đồng nai', 7, 1);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_session`
--

CREATE TABLE IF NOT EXISTS `tbtt_session` (
  `session_id` varchar(40) NOT NULL DEFAULT '0',
  `ip_address` varchar(16) NOT NULL DEFAULT '0',
  `user_agent` varchar(50) NOT NULL,
  `last_activity` int(10) unsigned NOT NULL DEFAULT '0',
  `user_data` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_shop`
--

CREATE TABLE IF NOT EXISTS `tbtt_shop` (
  `sho_id` int(10) NOT NULL AUTO_INCREMENT,
  `sho_name` varchar(150) NOT NULL,
  `sho_descr` varchar(150) NOT NULL,
  `sho_link` varchar(100) NOT NULL,
  `sho_logo` varchar(150) NOT NULL,
  `sho_dir_logo` varchar(150) NOT NULL,
  `sho_about` text NOT NULL,
  `sho_banner` varchar(150) NOT NULL,
  `sho_dir_banner` varchar(150) NOT NULL,
  `sho_address` varchar(100) NOT NULL,
  `sho_category` int(3) NOT NULL,
  `sho_province` int(3) NOT NULL,
  `sho_phone` varchar(20) NOT NULL,
  `sho_mobile` varchar(20) NOT NULL,
  `sho_email` varchar(50) NOT NULL,
  `sho_yahoo` varchar(50) NOT NULL,
  `sho_skype` varchar(50) NOT NULL,
  `sho_website` varchar(100) NOT NULL,
  `sho_style` varchar(20) NOT NULL,
  `sho_saleoff` int(1) NOT NULL,
  `sho_status` int(1) NOT NULL,
  `sho_popular` tinyint(1) NOT NULL,
  `sho_user` int(10) NOT NULL,
  `sho_view` int(10) NOT NULL,
  `sho_category_shop_home` varchar(200) NOT NULL,
  `sho_quantity_product` int(10) NOT NULL,
  `sho_begindate` int(11) NOT NULL,
  `sho_enddate` int(11) NOT NULL,
  PRIMARY KEY (`sho_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=77 ;

--
-- Dumping data for table `tbtt_shop`
--

INSERT INTO `tbtt_shop` (`sho_id`, `sho_name`, `sho_descr`, `sho_link`, `sho_logo`, `sho_dir_logo`, `sho_about`, `sho_banner`, `sho_dir_banner`, `sho_address`, `sho_category`, `sho_province`, `sho_phone`, `sho_mobile`, `sho_email`, `sho_yahoo`, `sho_skype`, `sho_website`, `sho_style`, `sho_saleoff`, `sho_status`, `sho_popular`, `sho_user`, `sho_view`, `sho_category_shop_home`, `sho_quantity_product`, `sho_begindate`, `sho_enddate`) VALUES
(72, 'Thời Trang', 'Chuyên kinh doanh các sản phẩm thời trang, làm đẹp', 'anphan', '34d2b242fbfdde1ede5223827db4e6f3.jpg', '09122011', '', '5689d4c41f7d507f57295deb30f10087.swf', '09122011', 'Vinh', 428, 81, '01669465434', '', 'thienthandianguc_dt@yahoo.com', '', '', '', 'default', 1, 1, 1, 559, 43, '', 2, 1323363600, 1577811600),
(73, 'Công Ty Cổ Phần Đa Năng Anh Tài', 'Cung cấp, phân phối thiết bị công nghệ. Sản xuất phần mềm ...', 'anhtaijsc', '7be39f4833a74874e865b87b59396b33.jpeg', '16122011', '<p style="text-align: center;">TRI &Acirc;N KH&Aacute;CH H&Agrave;NG. thay lời cảm ơn CTCP&nbsp;đa năng Anh T&agrave;i&nbsp;gửi đến qu&yacute; kh&aacute;ch h&agrave;ng chương tr&igrave;nh khuyến m&atilde;i đ&oacute;n gi&aacute;n sinh an l&agrave;nh &amp; năm mới ấm &aacute;p&nbsp;&nbsp;</p>\n<p><img src="http://ng5.upanh.com/b1.s13.d5/8bc2615d8d5caf1c45235226d6493aec_38842055.thaydoicopy.jpg" alt="" width="572" height="800" /></p>\n<p><img src="http://ng3.upanh.com/b5.s23.d2/97cbd443ac9635cc9a928da1ef4437c1_38842053.filam.jpg[/i[img]http://ng5.upanh.com/b1.s13.d5/8bc2615d8d5caf1c45235226d6493aec_38842055.thaydoicopy.jpg" alt="" width="572" height="800" /></p>', '0182b5b8c42905e1ea1e4f1637a68f4c.jpg', '16122011', 'Lê Viết Thuật Mỹ Trung Hưng Lộc Tp Vinh', 418, 81, '0383578226', '0386668862', 'dananganhtai@gmail.com', 'atmjsc', '', 'anhtaijsc.com', 'default', 1, 1, 1, 565, 65, '', 17, 1323968400, 1590940800),
(74, 'Công Ty Tư Vấn Và Đầu Tư Xây Dựng Hà Anh', 'Môi giới - Nhận kí gửi nhà đất', 'batdongsannghean', '87090d147b465e7d37f7894e8c5c9269.jpeg', '16122011', '<p><strong>HaAnh Joint Company xin gửi tới Qu&yacute; kh&aacute;ch h&agrave;ng lời ch&agrave;o tr&acirc;n trọng c&ugrave;ng lời ch&uacute;c sức khoẻ v&agrave; th&agrave;nh đạt.&nbsp;</strong><strong><strong>HaAnh Joint Company</strong> l&agrave;&nbsp;Trung t&acirc;m giao dịch Bất động sản uy t&iacute;n&nbsp;h&agrave;ng&nbsp;đầu&nbsp;tại Nghệ An. Chuy&ecirc;n mua b&aacute;n, k&yacute; gửi bất động sản.&nbsp;</strong></p>\n<p><strong></strong><strong>&nbsp;Khi&nbsp;bạn&nbsp;cần&nbsp;B&Aacute;N đất,&nbsp;B&Aacute;N nh&agrave; --&gt; h&atilde;y li&ecirc;n hệ ngay ch&uacute;ng t&ocirc;i<br /><em><span style="color: #0000ff;">(Ch&uacute;ng t&ocirc;i sẽ b&aacute;n gi&uacute;p bạn, b&aacute;n xong bạn chỉ mất ph&iacute; 0,2-1% tổng gi&aacute; trị )</span></em></strong></p>\n<p><strong>Khi&nbsp;bạn&nbsp;cần&nbsp;MUA đất,&nbsp;MUA nh&agrave; --&gt; h&atilde;y li&ecirc;n hệ ngay ch&uacute;ng t&ocirc;i<br /><em><span style="color: #0000ff;">(Người mua kh&ocirc;ng phải mất ph&iacute;, v&igrave; ch&uacute;ng t&ocirc;i chỉ thu ph&iacute; b&ecirc;n b&aacute;n 0,2-1% khi&nbsp;giao dịch th&agrave;nh c&ocirc;ng)</span></em></strong></p>\n<p align="center"><strong><em><span style="color: #0000ff;">Gi&aacute; b&aacute;n do chủ nh&agrave; đề ra, n&ecirc;n người mua c&oacute; thể thương lượng với chủ nh&agrave;.</span></em></strong></p>\n<p align="center"><strong></strong><strong>Hotline : <em><span style="font-size: small;">0972.745.777 - 0943.352.356</span></em></strong></p>\n<p align="center"><strong><em><span style="font-size: small;"><strong>ĐC :P.226 - CC Vinh T&acirc;n - P.Vinh T&acirc;n - TP Vinh. <br /></strong></span></em></strong></p>\n<p><strong></strong><strong>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Http://thitruong37.com/batdongsannghean <br /></strong></p>', '0496d10a8f1cfe04631e8b59ff94294c.swf', '16122011', 'Phòng 226 - Chung Cư Tân Phúc - P.vinh Tân - Tp Vinh', 433, 81, '0972.745.777', '0943.352.356', 'tuantran.qh590@yahoo.com', 'tuantran.qh590', '', '', 'default', 1, 1, 1, 569, 26, '', 1, 1323968400, 1590940800),
(75, 'Thái Hoài Computer', 'Máy tính , laptop, sửa chữa thiết bị điện tử', 'maytinhthaihoai', '030a97cfb53c79c0f9857824ebee3e4a.jpg', '17122011', '', '1cf293585bd6dbb4603d5a008fbee8d5.jpg', '17122011', 'Số 1 –ngõ 47 – Hà Huy Tập – Tp.vinh', 418, 81, '0945.911.088', '', 'thaihoaipc@gmail.com', 'thailuonghoai111088', '', '', 'default', 0, 1, 1, 568, 12, '', 0, 1324054800, 1590940800),
(76, 'Công Ty Lửa Xanh Nhân Anh', 'kinh doanh một các sản phẩm tiết kiệm và mới trên thị trường', 'dautunhanh', '72c1f232be98c11d77b547cccae60402.jpg', '17122011', '', '45ef2959a92a403859b5cf3d2a942a47.swf', '17122011', '39 Nguyễn Thái Học - Phường Lê Lợi - Vinh - Nghệ An', 423, 81, '0917310655', '', 'chauhs87@gmail.com', 'hlgmientrung', '', 'dautunhanh.tk', 'default', 1, 1, 0, 573, 6, '', 0, 1324054800, 1590940800);

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_showcart`
--

CREATE TABLE IF NOT EXISTS `tbtt_showcart` (
  `shc_id` int(10) NOT NULL AUTO_INCREMENT,
  `shc_product` int(10) NOT NULL,
  `shc_quantity` int(5) NOT NULL,
  `shc_saler` int(10) NOT NULL,
  `shc_buyer` int(10) NOT NULL,
  `shc_buydate` int(11) NOT NULL,
  `shc_process` int(1) NOT NULL,
  `shc_status` int(1) NOT NULL,
  PRIMARY KEY (`shc_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=31 ;

-- --------------------------------------------------------

--
-- Table structure for table `tbtt_user`
--

CREATE TABLE IF NOT EXISTS `tbtt_user` (
  `use_id` int(10) NOT NULL AUTO_INCREMENT,
  `use_username` varchar(35) NOT NULL,
  `use_password` varchar(35) NOT NULL,
  `use_salt` varchar(10) NOT NULL,
  `use_email` varchar(50) NOT NULL,
  `use_fullname` varchar(100) NOT NULL,
  `use_birthday` int(11) NOT NULL,
  `use_sex` int(1) NOT NULL,
  `use_address` varchar(100) NOT NULL,
  `use_province` int(3) NOT NULL,
  `use_phone` varchar(20) NOT NULL,
  `use_mobile` varchar(20) NOT NULL,
  `use_yahoo` varchar(50) NOT NULL,
  `use_skype` varchar(50) NOT NULL,
  `use_group` int(3) NOT NULL,
  `use_status` int(1) NOT NULL,
  `use_regisdate` int(11) NOT NULL,
  `use_enddate` int(11) NOT NULL,
  `use_key` varchar(150) NOT NULL,
  `use_lastest_login` int(11) NOT NULL,
  PRIMARY KEY (`use_id`)
) ENGINE=MyISAM  DEFAULT CHARSET=utf8 AUTO_INCREMENT=575 ;

--
-- Dumping data for table `tbtt_user`
--

INSERT INTO `tbtt_user` (`use_id`, `use_username`, `use_password`, `use_salt`, `use_email`, `use_fullname`, `use_birthday`, `use_sex`, `use_address`, `use_province`, `use_phone`, `use_mobile`, `use_yahoo`, `use_skype`, `use_group`, `use_status`, `use_regisdate`, `use_enddate`, `use_key`, `use_lastest_login`) VALUES
(546, 'chaupm', '433f1cb649fbc54225f62e7e8c83178d', 'Cdoy5g38', 'webmaster@thitruong37.com', 'Phan Minh Châu', 501267600, 1, 'Hà Nội', 75, '0975965656', '0975965656', '', '', 4, 1, 1322240400, 1637859600, '', 1324184414),
(1, 'administ', 'f89a335fe270091f61d2f3ae439f89a5', 'GuasWRH9', 'administrator@geate.net', 'Lê Trung Hiếu', 345402000, 1, 'Hồ Chí Minh', 74, '0985687885', '0985687885', 'lehieu008', 'lehieu008', 4, 1, 1322240400, 1637859600, '957aa2ea74ca36898df64fb1cb35e65a62bb2cbc6b5e797af47cf8d727d33e95', 1342706934),
(564, 'buon_may_ban_dat', 'a8b7108c65eedb1d4e24608b3d71e042', 'qLlQnINz', 'yeu_mot_bong_hong@yahoo.com', 'Anh Hung', 429897600, 1, 'Vinh.nghe An', 81, '01675173737', '', 'yeu_mot_bong_hong', '', 3, 1, 1323882000, 1323882000, '2ccbb7aadc30cf4cb8fff3e412171c86c1533868d4c487654ce752d5a6e69c56', 1323882000),
(562, 'dinhdatpro1987', '2b50e58ca642c64d9e54a1b08fb6e40f', 'wOp2hcny', 'dinhdatpro1987@gmail.com', 'Dat', 554317200, 1, 'Thanh Pho Vinh', 0, '', '0943357668', '', '', 3, 1, 1323795600, 1323795600, 'ec38ac2a66a2be4fea9d63e127feeb55e7a2378f55813479aa1e2af621b588ef', 1323795600),
(565, 'anhtaijsc', '915c53d2b89f75900cb15f398c857b00', 'iu7EXogS', 'dananganhtai@gmail.com', 'Admin Anhtaijsc', 433270800, 1, 'Mỹ Trung Hưng Lộc Tp Vinh', 0, '', '0943357668', 'atmjsc', '', 3, 1, 1323968400, 1590940800, 'f41ed52f62288ba23747f77f460a4ecb1e3a4fb7d134f949c4f84d81833e393f', 1324180960),
(560, 'matmatkhau', '666ced16237c638b2bce0a94c659b7b6', 'vzROPS9X', 'chauict@gmail.com', 'Pmc', 943894800, 0, '', 0, '', '0123456789', '', '', 1, 1, 1323363600, 1638982800, '2ab713fc45296d7882efe0f116a60dc9c7af4bfbf9ab86679a1920962a956f7e', 1323449729),
(559, 'anhphan', '8ef94ac283a4d01d3f4d9a45ebb2c61a', 'rOJ3O9w8', 'thienthandianguc_dt@yahoo.com', 'Phan Thị Ngọc Ánh', 943894800, 0, 'Vinh - Nghệ An', 0, '', '01669465434', '', '', 3, 1, 1323363600, 1577811600, 'c3f74f3db8ed16b38654b09800eb0279470b573c8a2673629d0ec541963e4626', 1324029691),
(563, 'maytinh4d', 'b05b371c8b4c64ef239be4c14333b536', 'cSoc9Zk.', 'sales4d@maytinh4d.com.vn', 'Nguyễn Quang Chung', 369507600, 1, '165 Nguyễn Du Tp Vinh Nghệ An', 0, '', '0918065408', 'kinhdoanh4d_01', '', 1, 1, 1323882000, 1639501200, 'e38d8154016f5c95c57226cb0dd68e6ab560c53effb37f84aaeb89f72c304fb3', 1323922968),
(566, 'brand_ht', '64e549bcc7e8e2625f37e6220da4605b', '9_mJ2EyW', 'anh.hta85@gmail.com', 'Hoang Anh', 483814800, 1, 'Hưng Nguyên - Nghệ An', 0, '', '0983865222', '', '', 1, 1, 1323968400, 1639587600, '685adffe75782fdd6939c8dfd300b375a1542e7beccfbbaa85c7618c07038091', 1324007116),
(567, 'quang_pru', 'c00a56483acb5f3dcd7f71b2e428c1fa', 'wtyp_jTV', 'quang_pru@yahoo.com', 'Nguyễn Quang', 359053200, 1, 'Tp Vinh', 0, '', '0989294293', 'quang_pru', 'quang_pru', 3, 1, 1323968400, 1323968400, '7d185448de3b673f9da4d63c7b83ec754b691cdeae5f48ef6bf6ec676da2e74f', 1323968400),
(568, 'thaihoai', '7807891da6a3c6de5afd1b46cb9c4659', '1dJET51E', 'thaihoaipc@gmail.com', 'Thái Lương Hoài', 602269200, 0, 'Hà Huy Tập- Tp.vinh –nghệ An', 82, '0945.911.088', '01678121823', '', '', 3, 1, 1323968400, 1590940800, '3cda4e1d3d4fe8921f88227d6e187b82cd999616f1bbc23ffc3185173601b54b', 1324091622),
(569, 'tuanbds590', '17667e9e7c84e4c166ba8dc139429faa', 'QiBzWw3O', 'tuantran.qh590@yahoo.com', 'Trần Anh Tuấn', 652467600, 1, 'Vinh City', 0, '', '0972.745.777', 'tuantran.qh590', '', 3, 1, 1323968400, 1590940800, 'acc3aa1f30c0f38a439be699de15e2598f048b24284ffe2a8c6544c339f143a0', 1324083483),
(570, 'avatarhotel', 'd5aa849c4df91f392e7b00c6e892ae97', 'SjD4CQkI', 'hoangson1020@gmail.com', 'Mr.avatar', 357930000, 1, 'Vinh - Nghe An', 0, '', '0917551199', 'mrdai777', 'mrdai777', 3, 0, 1324054800, 1324054800, 'db0c552058ba563f6f1a9da8ad343bcaa4575f7a4b0075005eee46ab502b0ac6', 1324054800),
(571, 'cuhintran', '925b7404fb234bda8d89e8c37f16bdfe', 'hcjrK3W2', 'cuhintran@gmail.com', 'Tran Linh', 553539600, 1, 'Tp Vinh', 0, '', '0989414078', 'cuhin.tran', '', 3, 0, 1324054800, 1324054800, '102c8af4940ef946f9c94d4c638d37fcad79130ca3c2d62a95c842546e253386', 1324054800),
(572, 'lamnghiep', '0702ef430ffc5a2960e6388ad148ea28', 'XwEtB8OC', 'phamvantam3692@yahoo.com.vn', 'Phạm Văn Tâm', 707504400, 1, 'Vinh', 0, '', '01644366971', 'phamvantam3692', '', 1, 1, 1324054800, 1639674000, '81b55fb4b7f040ec19d9f9e5d21c6eca6b4b5458a03c5ceb77956e53af2c4de6', 1324054800),
(573, 'chauqtkd', '7af53b8d6a48877215a8add1d6116037', 'sUyINlta', 'chauhs87@gmail.com', 'Hồ Sỹ Châu', 557773200, 1, '', 0, '', '0917310655', 'hlgmientrung', '', 3, 1, 1324054800, 1590940800, 'cc042dac9021055c482dbdebc810a3c3a99b984c6befd643a5d08baf507a61b6', 1324122011),
(574, 'khanhtam', '25bef53cbe8465fc5e1194dbbfad025d', 'KQAzqHoc', 'khanh_ak2011@yahoo.com.vn', 'Khanh', 375037200, 1, 'K11 Hht Tpvinh Na', 0, '', '0927237789', '', '', 1, 1, 1324054800, 1639674000, '669b8c9c2296d1d6a2506cc1f6bebd65b21e9bf6c3233cfeb128d4372ae3ad4d', 1324054800);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
