<?php $this->load->view('home/common/header'); ?>
<?php $this->load->view('home/common/left'); ?>
<script language="javascript" src="<?php echo base_url(); ?>templates/home/js/check_email.js"></script>
<!--BEGIN: RIGHT-->
<td width="803" valign="top">
    <table width="803" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td background="<?php echo base_url(); ?>templates/home/images/bg_topkhung_ac.jpg" height="30">
                <div class="tile_modules"><?php echo 'SỬA DANH MỤC CỬA HÀNG'; ?></div>
            </td>
        </tr>
        <tr>
            <td background="<?php echo base_url(); ?>templates/home/images/bg_mainkhung_ac.jpg" valign="top" >
                <?php if($successEditShopCategoryAccount == false){ ?>
                <div class="notepost_account">
                    <img src="<?php echo base_url(); ?>templates/home/images/note_post.gif" border="0" width="20" height="20" />&nbsp;
                    <b><font color="#FD5942"><?php echo $this->lang->line('note_help'); ?>:</font></b>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <font color="#FF0000"><b>*</b></font>&nbsp;&nbsp;<?php echo $this->lang->line('must_input_help'); ?>
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    <img src="<?php echo base_url(); ?>templates/home/images/help_post.gif" />&nbsp;&nbsp;<?php echo $this->lang->line('input_help'); ?>
                </div>
                <?php } ?>
                <table width="585" class="post_main" align="center" cellpadding="0" cellspacing="0" border="0">
                    <tr>
                        <td colspan="2" height="30" class="post_top"></td>
                    </tr>
                    <?php if($successEditShopCategoryAccount == false){ ?>
                    <form name="frmPostShopCategory" method="post">
                    <tr>
                        <td width="150" valign="top" class="list_post"><font color="#FF0000"><b>*</b></font> <?php echo 'Danh mục'; ?>:</td>
                        <td>
                            <input type="text" name="name_category" id="name_category" value="<?php if(isset($name_category)){echo $name_category;} ?>" maxlength="80" class="input_formpost" onkeyup="BlockChar(this,'SpecialChar');" onfocus="ChangeStyle('name_category',1)" onblur="ChangeStyle('name_category',2)" />
                            <?php echo form_error('name_category'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><?php echo 'Mô tả'; ?>:</td>
                        <td>
                            <input type="text" name="descr_category" id="descr_category" value="<?php if(isset($descr_category)){echo $descr_category;} ?>" maxlength="80" class="input_formpost" onkeyup="BlockChar(this,'SpecialChar')" onfocus="ChangeStyle('descr_category',1)" onblur="ChangeStyle('descr_category',2)" />
                            <?php echo form_error('descr_category'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><font color="#FF0000"><b>*</b></font> <?php echo 'Danh mục cha'; ?>:</td>
                        <td>
                            <select name="parent_category" id="parent_category" class="selectcategory_formpost">
                                <option value="0"><?php echo '--Chọn danh mục cha--'; ?></option>
                                <?php foreach($categoryParent as $categoryParentArray){ ?>
                                    <?php if($categoryParentArray->cas_id == $parent_category){ ?>
                                    <option value="<?php echo $categoryParentArray->cas_id; ?>" selected="selected" <?php echo ($categoryParentArray->cas_id == $id_category)?'disabled="disabled"':''; ?>><?php echo $categoryParentArray->cas_name; ?></option>
                                    <?php }else{ ?>
                                    <option value="<?php echo $categoryParentArray->cas_id; ?>" <?php echo ($categoryParentArray->cas_id == $id_category)?'disabled="disabled"':''; ?>><?php echo $categoryParentArray->cas_name; ?></option>
                                    <?php } ?>
                                    
                                    <?php foreach($categoryChild as $categoryChildArray){ ?>
                                        <?php if($categoryParentArray->cas_id == $categoryChildArray->cas_parent){ ?>
                                        
                                            <?php if($categoryChildArray->cas_id == $parent_category){ ?>
                                            <option value="<?php echo $categoryChildArray->cas_id; ?>" selected="selected" disabled="disabled">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $categoryChildArray->cas_name; ?></option>
                                            <?php }else{ ?>
                                            <option value="<?php echo $categoryChildArray->cas_id; ?>" disabled="disabled">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<?php echo $categoryChildArray->cas_name; ?></option>
                                            <?php } ?>
                                        
                                        <?php } ?>
                                    <?php } ?>
                                <?php } ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" valign="top" class="list_post"><?php echo 'Thứ tự'; ?>:</td>
                        <td>
                            <input type="text" name="order_category" id="order_category" value="<?php if(isset($order_category)){echo $order_category;} ?>" maxlength="80" class="input_formpost" style="width:40px; font-weight:bold; text-align:center;" onkeyup="BlockChar(this,'SpecialChar')" onfocus="ChangeStyle('order_category',1)" onblur="ChangeStyle('order_category',2)" />
                            <?php echo form_error('order_category'); ?>
                        </td>
                    </tr>
                    <tr>
                        <td width="150" class="list_post"><?php echo 'Kích hoạt'; ?>:</td>
                        <td>
                            <input type="checkbox" name="status_category" id="status_category" value="1" <?php if(isset($status_category) && (int)$status_category == 1){echo 'checked="checked"';} ?> />
                        </td>
                    </tr>
                    <?php if(isset($imageCaptchaEditShopCategoryAccount)){ ?>
                    <tr>
                        <td width="150" valign="middle" class="list_post"><font color="#FF0000"><b>*</b></font> <?php echo $this->lang->line('captcha_main'); ?>:</td>
                        <td align="left" style="padding-top:7px;">
                            <img src="<?php echo base_url().$imageCaptchaEditShopCategoryAccount; ?>" width="151" height="30" /><br />
                            <input type="text" name="captcha_category" id="captcha_category" value="" maxlength="10" class="inputcaptcha_form" onfocus="ChangeStyle('captcha_category',1);" onblur="ChangeStyle('captcha_category',2);" />
                            <?php echo form_error('captcha_category'); ?>
                        </td>
                    </tr>
                    <?php } ?>
                    <tr>
                        <td width="150"></td>
                        <td height="30" valign="bottom" align="center">
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td colspan="3" height="25"></td>
                                </tr>
                                <tr>
                                    <td><input type="submit" onclick="" name="submit_postshopcategory" value="<?php echo 'Cập nhật'; ?>" class="button_form" /></td>
                                    <td width="15"></td>
                                    <td><input type="button" name="cancle_postshopcategory" value="<?php echo 'Hủy bỏ'; ?>" onclick="ActionLink('<?php echo base_url(); ?>account/shop/category')" class="button_form" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <input type="hidden" name="isEditShopCategoryAccount" value="1" />
                    </form>
                    <?php }else{ ?>
                    <tr>
                        <td class="success_post" style="padding-top: 10px;">
                            <meta http-equiv=refresh content="5; url=<?php echo base_url(); ?>account/shop/category/post">
                            <?php echo 'Danh mục cửa hàng đã được cập nhật thành công'; ?>
						</td>
					</tr>
                    <?php } ?>
                    <tr>
                        <td colspan="2" height="30" class="post_bottom"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td background="<?php echo base_url(); ?>templates/home/images/bg_bottomkhung_ac.jpg" height="16" ></td>
        </tr>
    </table>	
</td>					
<!--END RIGHT-->
<?php $this->load->view('home/common/footer'); ?>