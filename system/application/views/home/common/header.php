<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<link rel="icon" href="<?php echo base_url(); ?>templates/home/images/favicon.ico" type="image/x-icon" />
<link rel="shortcut icon" href="<?php echo base_url(); ?>templates/home/images/favicon.ico" type="image/x-icon" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta name="verify-v1" content="dB3XFWbltsbW3HIthO/CG5i0KA4MUed7n1kxQwtoUUA=" />
<meta name="keywords" content="<?php echo Setting::settingKeyword; ?>" />
<meta name="description" content="<?php if(isset($descrSiteGlobal)){echo $descrSiteGlobal;}else{echo Setting::settingDescr;} ?>" />
<title><?php if(isset($titleSiteGlobal)){echo $this->lang->line('title_detail_header').$titleSiteGlobal;}else{echo Setting::settingTitle;} ?></title>
<script language="javascript">var baseUrl = '<?php echo base_url(); ?>';</script>
<script language="javascript" src="<?php echo base_url(); ?>templates/home/js/jquery.js"></script>
<script language="javascript" src="<?php echo base_url(); ?>templates/home/js/general.js"></script>
<script language="javascript" src="<?php echo base_url(); ?>templates/home/js/tooltips.js"></script>
<script language="javascript" src="<?php echo base_url(); ?>templates/home/js/swfobject.js" type="text/javascript"></script>
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>templates/home/css/templates.css" />
<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>templates/home/css/tooltips.css" />
<!--[if ie]>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>templates/home/css/ie.css" />
<![endif]-->
<!--[if ie 6]>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>templates/home/css/ie6.css" />
<![endif]-->
<!--[if ie 8]>
	<link type="text/css" rel="stylesheet" href="<?php echo base_url(); ?>templates/home/css/ie8.css" />
<![endif]-->
</head>
<body id="body">
<table align="center" width="1004" border="0" cellpadding="0" cellspacing="0">
	<tr >
		<td colspan="2" background="<?php echo base_url(); ?>templates/home/images/bg_logo.jpg">
        	<table border="0" width="100%" height="90" cellpadding="0" cellspacing="0">
            	<tr>
                	<td><a href="<?php echo base_url(); ?>"><img src="<?php echo base_url(); ?>templates/home/images/logo.png" border="0" style="max-width:240px;" /></a></td>
                    <td>
                    	<div class="h_search">
                        	<select name="SelectSearch" id="SelectSearch" class="h_select">
                            	<option value="1"><?php echo $this->lang->line('product_search_header'); ?></option>
                                <option value="2"><?php echo $this->lang->line('ads_search_header'); ?></option>
                                <option value="3"><?php echo $this->lang->line('job_search_header'); ?></option>
                                <option value="4"><?php echo $this->lang->line('employ_search_header'); ?></option>
                                <option value="5"><?php echo $this->lang->line('shop_search_header'); ?></option>
                            </select>
                        	<input type="text" name="KeywordSearch" id="KeywordSearch" class="h_input" value="<?php if(isset($nameKeyword)){echo $nameKeyword;}elseif(isset($titleKeyword)){echo $titleKeyword;} ?>" />
                            <input type="button" name="" id="" class="h_button" onclick="Search('<?php echo base_url(); ?>')" value="<?php echo $this->lang->line('search_search_header'); ?>" />
                        </div>
                        <?php if(trim(strtolower($this->uri->segment(1))) == 'search' && trim(strtolower($this->uri->segment(2))) == 'ads'){ ?>
						<script>sel=SelectSearch(2);</script>
                        <?php }elseif(trim(strtolower($this->uri->segment(1))) == 'search' && trim(strtolower($this->uri->segment(2))) == 'job'){ ?>
                        <script>sel=SelectSearch(3);</script>
                        <?php }elseif(trim(strtolower($this->uri->segment(1))) == 'search' && trim(strtolower($this->uri->segment(2))) == 'employ'){ ?>
                        <script>sel=SelectSearch(4);</script>
                        <?php }elseif(trim(strtolower($this->uri->segment(1))) == 'search' && trim(strtolower($this->uri->segment(2))) == 'shop'){ ?>
                        <script>sel=SelectSearch(5);</script>
                        <?php }else{ ?>
                        <script>sel=SelectSearch(1);</script>
                        <?php } ?>
                        <div class="h_pa_icon">
							<a class="h_product menu" title="<?php echo $this->lang->line('post_product_header'); ?>" href="<?php echo base_url(); ?>product/post"><?php echo $this->lang->line('post_product_header'); ?></a>
							<a class="h_ads menu" title="<?php echo $this->lang->line('post_ads_header'); ?>" href="<?php echo base_url(); ?>ads/post"><?php echo $this->lang->line('post_ads_header'); ?></a>
						</div>
                    </td>
                    <td style="padding-left:10px;">
                    	<div class="h_icon" onclick="location.href='<?php echo base_url(); ?>product'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_product.png) no-repeat;">
                        	<?php echo $this->lang->line('product_menu_header'); ?>
                        </div>
                        <div class="h_icon" onclick="location.href='<?php echo base_url(); ?>ads'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_ads.png) no-repeat;">
                        	<?php echo $this->lang->line('ads_menu_header'); ?>
                        </div>
                        <div class="h_icon" onclick="location.href='<?php echo base_url(); ?>shop'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_shop.png) no-repeat;">
                        	<?php echo $this->lang->line('shop_menu_header'); ?>
                        </div>
                        <div class="h_icon" onclick="location.href='<?php echo base_url(); ?>job'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_job.png) no-repeat;">
                        	<?php echo $this->lang->line('job_menu_header'); ?>
                        </div>
                        <?php if(!$this->check->is_logined($this->session->userdata('sessionUser'), $this->session->userdata('sessionGroup'), 'home') || trim(strtolower($this->uri->segment(1))) == 'logout'){ ?>
                            <div class="h_icon" onclick="location.href='<?php echo base_url(); ?>register'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_register.png) no-repeat;">
                                <?php echo $this->lang->line('register_menu_header'); ?>
                            </div>
                            <div class="h_icon" onclick="location.href='<?php echo base_url(); ?>login'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_login.png) no-repeat;">
                                <?php echo $this->lang->line('login_menu_header'); ?>
                            </div>
                        <?php }else{ ?>
                        	<div class="h_icon" onclick="location.href='<?php echo base_url(); ?>account'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_register.png) no-repeat;">
								<?php echo $this->lang->line('account_menu_header'); ?>
                            </div>
                            <div class="h_icon" onclick="location.href='<?php echo base_url(); ?>logout'" style="background:url(<?php echo base_url(); ?>templates/home/images/h_icon_login.png) no-repeat;">
                                <?php echo $this->lang->line('logout_menu_header'); ?>
                            </div>
                        <?php } ?>
                    </td>
                </tr>
            </table>
        </td>
	</tr>
	<tr>
		<td style="background:url(<?php echo base_url(); ?>templates/home/images/bg_header_banner.jpg) repeat-x;">
        	<table width="502" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="3" class="header_banner_top header_banner_top_left"></td>
				</tr>
				<tr>
					<td class="header_banner_left header_banner_left_left"></td>
					<td width="490" height="100" class="header_banner_center_left" style="background:#FFF;"><?php $this->load->view('home/advertise/header1'); ?></td>
					<td class="header_banner_right header_banner_right_left"></td>
				</tr>
				<tr>
					<td colspan="3" class="header_banner_bottom header_banner_bottom_left"></td>
				</tr>
			</table>
		</td>
		<td style="background:url(<?php echo base_url(); ?>templates/home/images/bg_header_banner.jpg) repeat-x;">
			<table width="502" border="0" cellpadding="0" cellspacing="0">
				<tr>
					<td colspan="3" class="header_banner_top"></td>
				</tr>
				<tr>
					<td class="header_banner_left"></td>
					<td class ="header_phai" width="490" height="100" style="background:#FFF;"><?php $this->load->view('home/advertise/header2'); ?></td>
					<td class="header_banner_right"></td>
				</tr>
				<tr>
					<td colspan="3" class="header_banner_bottom"></td>
				</tr>
			</table>
		</td>
	</tr>
	<tr>
		<td style="border:1px #00CCFF solid; border-right:0px;" background="<?php echo base_url(); ?>templates/home/images/index_18.jpg">
        	<table width="502" border="0" cellpadding="0" cellspacing="0">
				<tr height="21">
                	<td width="10">&nbsp;</td>
					<td width="90" style="background:url(<?php echo base_url(); ?>templates/home/images/show_cart.gif) no-repeat; padding-left:32px;">
                    	<a class="menu" href="<?php echo base_url(); ?>showcart"><?php echo $this->lang->line('showcart_header'); ?><span class="showcart_header">(<?php if($this->session->userdata('sessionProductShowcart')){echo count(explode(',', trim($this->session->userdata('sessionProductShowcart'), ',')));}else{echo '0';} ?>)</span></a>
                    </td>
                   <td background="<?php echo base_url(); ?>templates/home/images/index_19.jpg">
                        <?php if(!$this->check->is_logined($this->session->userdata('sessionUser'), $this->session->userdata('sessionGroup'), 'home') || trim(strtolower($this->uri->segment(1))) == 'logout'){ ?>
                            <a class="menu" href="<?php echo base_url(); ?>register" style="padding:2px 0px 0px 22px; background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_regis.gif) no-repeat left center;"><?php echo $this->lang->line('register_menu_header'); ?></a>
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a class="menu" href="<?php echo base_url(); ?>login" style="padding:2px 0px 0px 22px; background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_log.gif) no-repeat left center;"><?php echo $this->lang->line('login_menu_header'); ?></a>
                        <?php }else{ ?>
                            <span style="font-weight:bold; color:#004B7A; padding:2px 0px 0px 22px; background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_regis.gif) no-repeat left center;">Xin chào</span>
                            <a class="menu" href="<?php echo base_url(); ?>account"><?php echo $this->session->userdata('sessionUsername'); ?></a>
                            (<a class="menu" href="<?php echo base_url(); ?>account" style="font-weight:normal;"><?php echo 'Tài khoản của tôi'; ?></a>)
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <a class="menu" href="<?php echo base_url(); ?>logout" style="padding:2px 0px 0px 22px; background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_log.gif) no-repeat left center;"><?php echo 'Thoát'; ?></a>
                        <?php } ?>
                    </td>
				</tr>
			</table>
        </td>
		<td style="border:1px #00CCFF solid; border-left:0px;">
			<table width="502" border="0" cellpadding="0" cellspacing="0">
				<tr height="21">
					<td width="70" background="<?php echo base_url(); ?>templates/home/images/index_19.jpg">&nbsp;</td>
                    <td style="text-align:right; padding-right:26px;" background="<?php echo base_url(); ?>templates/home/images/index_19.jpg"><a class="menu" href="<?php echo base_url(); ?>" title="" style="padding:2px 0px 0px 22px; background:url(<?php echo base_url(); ?>templates/home/images/home_icon.gif) no-repeat left center;">Trang chủ</a></td>
                    <td width="60" background="<?php echo base_url(); ?>templates/home/images/contact.jpg" style="padding-left:33px;"><a class="menu" href="<?php echo base_url(); ?>contact" title="<?php echo $this->lang->line('contact_header'); ?>"><?php echo $this->lang->line('contact_header'); ?></a></td>
					<td width="114" background="<?php echo base_url(); ?>templates/home/images/kythuat.jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="menu" href="ymsgr:SendIM?<?php echo Setting::settingYahoo_1; ?>" title="<?php echo Setting::settingYahoo_1; ?>"><?php echo $this->lang->line('yahoo1_header'); ?></a></td>
					<td width="100" background="<?php echo base_url(); ?>templates/home/images/kinhdoanh.jpg">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<a class="menu" href="ymsgr:SendIM?<?php echo Setting::settingYahoo_2; ?>" title="<?php echo Setting::settingYahoo_2; ?>"><?php echo $this->lang->line('yahoo2_header'); ?></a></td>
				</tr>
			</table>
		</td>
	</tr>
    <tr>
        <td height="7" colspan="2"></td>
    </tr>
    <!--END HEADER-->
    <tr id="DivGlobalSite" style="display:none;">
		<td colspan="2">
			<table width="1004" border="0" cellpadding="0" cellspacing="0">
				<tr>