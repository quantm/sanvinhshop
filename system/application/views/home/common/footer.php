				</tr>
			</table>
		</td>
	</tr>
	<script>WaitingLoadPage();</script>
    <!--BEGIN: FOOTER-->
	<tr>
		<td colspan="2" align="center" bgcolor="#f3f3f3">
        	<table width="100%" border="0" cellpadding="0" cellspacing="0">
            	<tr>
                	<td width="201" id="counter"><?php echo $this->lang->line('counter_footer'); ?>: <?php echo $counter->cou_counter; ?></td>
                    <td id="footer" align="center">
                    	<?php echo $this->lang->line('info_1_footer'); ?><br />
                    	<?php echo $this->lang->line('info_2_footer'); ?><br />
                    	<?php echo $this->lang->line('info_3_footer'); ?><br />
                    	<?php echo $this->lang->line('info_4_footer'); ?>
                    </td>
                    <td width="201" align="center">
                    	<script type="text/javascript" src="<?php echo base_url(); ?>templates/home/js/bookmart.js"></script>
                    	<a onclick="bookmark('http://thitruong24gio.com','<?php echo Setting::settingTitle; ?>')" style="cursor:pointer;">
                        	<img src="<?php echo base_url(); ?>templates/home/images/bookmart.gif" border="0" title="<?php echo $this->lang->line('bookmark_tip_footer'); ?>" />
                        </a>&nbsp;
                    	<a href="#">
                    		<img src="<?php echo base_url(); ?>templates/home/images/top.gif" border="0" title="<?php echo $this->lang->line('top_tip_footer'); ?>" />
                        </a>
                    </td>
                </tr>
            </table>
        </td>
	</tr>
</table>
<!--BEGIN: Taskbar-->
<?php if(isset($notifyTaskbarGlobal) && count($notifyTaskbarGlobal) > 0){ ?>
<div id="DivTaskbarNotify" class="border_content_taskbar_notify">
	<table border="0" width="100%" cellpadding="0" cellspacing="0">
    	<tr>
        	<td height="25" valign="top">
            	<table width="100%" border="0" cellpadding="0" cellspacing="0">
                	<tr>
                    	<td width="10" height="25" style="background:url(<?php echo base_url(); ?>templates/home/images/bg_title_taskbar_notify_left.gif) no-repeat left;"></td>
                        <td class="title_taskbar_notify" style="background:url(<?php echo base_url(); ?>templates/home/images/bg_title_taskbar_notify_middle.gif) repeat-x;"><?php echo $this->lang->line('title_notify_taskbar'); ?></td>
                        <td align="right" style="background:url(<?php echo base_url(); ?>templates/home/images/bg_title_taskbar_notify_middle.gif) repeat-x;"><span onclick="TabTaskbarNotify()" style="cursor:pointer; color:#FFF; font-weight:bold; font-size:12px;">X</span></td>
                        <td width="10" height="25" style="background:url(<?php echo base_url(); ?>templates/home/images/bg_title_taskbar_notify_right.gif) no-repeat right;"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
        	<td class="content_taskbar_notify" valign="top">
            	<table border="0" width="100%" cellpadding="0" cellspacing="0">
            	    <?php $isFirstNotifyTaskbarGlobal = false; ?>
            	    <?php foreach($notifyTaskbarGlobal as $notifyTaskbarGlobalArray){ ?>
                    <tr>
                        <td width="5" valign="top" style="padding-top:4px;"><img src="<?php echo base_url(); ?>templates/home/images/list_ten.gif" border="0" /></td>
                        <td align="left" class="list_2" valign="top">
                            <a class="menu_1" href="<?php echo base_url() ?>notify/<?php echo $notifyTaskbarGlobalArray->not_id; ?>" title="<?php echo $this->lang->line('detail_tip'); ?>"><?php echo sub($notifyTaskbarGlobalArray->not_title, 100); ?></a>
                            <span class="date_view">(<?php echo date('d-m-Y', $notifyTaskbarGlobalArray->not_begindate); ?>)</span>
                        </td>
                    </tr>
					<?php if($isFirstNotifyTaskbarGlobal == false){ ?>
					<?php $firstNotifyTaskbarGlobal = $notifyTaskbarGlobalArray->not_id; ?>
					<?php $isFirstNotifyTaskbarGlobal = true; ?>
					<?php } ?>
                    <?php } ?>
                    <tr>
                        <td valign="top" class="view_all" colspan="2"><a class="menu_2" href="<?php echo base_url() ?>notify/<?php echo $firstNotifyTaskbarGlobal; ?>"><?php echo $this->lang->line('view_all_right'); ?></a></td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
</div>
<?php } ?>
<div class="taskbar">
	<div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_guide.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>guide')" href="#"><?php echo $this->lang->line('guide_taskbar'); ?></a>
    </div>
   	<div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_post_product.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>product/post')" href="#"><?php echo $this->lang->line('post_product_taskbar'); ?></a>
    </div>
    <div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_post_ads.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>ads/post')" href="#"><?php echo $this->lang->line('post_ads_taskbar'); ?></a>
    </div>
    <div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_post_job.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>job/post')" href="#"><?php echo $this->lang->line('post_job_taskbar'); ?></a>
    </div>
    <div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_post_employ.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>employ/post')" href="#"><?php echo $this->lang->line('post_employ_taskbar'); ?></a>
    </div>
    <?php if(!$this->check->is_logined($this->session->userdata('sessionUser'), $this->session->userdata('sessionGroup'), 'home') || trim(strtolower($this->uri->segment(1))) == 'logout'){ ?>
    <div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_regis.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>register')" href="#"><?php echo $this->lang->line('register_taskbar'); ?></a>
    </div>
    <div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_log.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>login')" href="#"><?php echo $this->lang->line('login_taskbar'); ?></a>
    </div>
    <?php }else{ ?>
    <div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_regis.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>account')" href="#"><?php echo $this->lang->line('account_taskbar'); ?></a>
    </div>
    <div class="taskbar_title" style="background:url(<?php echo base_url(); ?>templates/home/images/icon_taskbar_log.gif) no-repeat left;">
    	<a class="menu_taskbar" onclick="ActionLink('<?php echo base_url(); ?>logout')" href="#"><?php echo $this->lang->line('logout_taskbar'); ?></a>
    </div>
    <?php } ?>
    <?php if(isset($notifyTaskbarGlobal) && count($notifyTaskbarGlobal) > 0){ ?>
    <div class="taskbar_notify" onclick="TabTaskbarNotify(1)"><?php echo $this->lang->line('notify_taskbar'); ?></div>
    <?php } ?>
    <?php if(isset($adsTaskbarGlobal) && count($adsTaskbarGlobal) > 0){ ?>
    <div class="taskbar_vip">
    	<div style="margin-left:40px; margin-right:3px;">
        	<marquee onmouseover="this.stop();" onmouseout="this.start();" behavior="scroll" scrollamount="1" scrolldelay="1">
				<?php foreach($adsTaskbarGlobal as $adsTaskbarGlobalArray){ ?>
				<a class="menu_taskbar" href="<?php echo base_url() ?>ads/category/<?php echo $adsTaskbarGlobalArray->ads_category; ?>/detail/<?php echo $adsTaskbarGlobalArray->ads_id; ?>" title="<?php echo $adsTaskbarGlobalArray->ads_descr; ?>">&bull; <?php echo $adsTaskbarGlobalArray->ads_title; ?></a>
				<?php } ?>
            </marquee>
        </div>
    </div>
    <?php } ?>
</div>
<!--END Taskbar-->
<div class="bannerLeft">
    <?php foreach($advertise as $advertiseArray){ ?>
    <?php if((int)$advertiseArray->adv_position == 7 && trim($advertiseArray->adv_page) != '' && stristr($advertiseArray->adv_page, $advertisePage)){ ?>
    <tr>
    	<td align="center">
    	    <a href="<?php echo prep_url($advertiseArray->adv_link); ?>" target="_blank" title="<?php echo $advertiseArray->adv_title; ?>">
    	        <?php if(strtolower(substr($advertiseArray->adv_banner, -4)) == '.swf'){ ?>
    	        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" class="flash" id="FlashID_<?php echo $advertiseArray->adv_id; ?>" title="<?php echo $advertiseArray->adv_title; ?>">
    			  <param name="movie" value="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" />
    			  <param name="quality" value="high" />
    			  <param name="wmode" value="opaque" />
    			  <param name="swfversion" value="6.0.65.0" />
    			  <param name="expressinstall" value="<?php echo base_url(); ?>templates/home/images/expressInstall.swf" />
    			  <!--[if !IE]>-->
    			  <object type="application/x-shockwave-flash" data="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" class="advertise_left_flash">
    			    <!--<![endif]-->
    			    <param name="quality" value="high" />
    			    <param name="wmode" value="opaque" />
    			    <param name="swfversion" value="6.0.65.0" />
    			    <param name="expressinstall" value="<?php echo base_url(); ?>templates/home/images/expressInstall.swf" />
    			    <div>
    			      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
    			      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
    			    </div>
    			    <!--[if !IE]>-->
    			  </object>
    			  <!--<![endif]-->
    			</object>
    			<script type="text/javascript">
    			<!--
    			swfobject.registerObject("FlashID_<?php echo $advertiseArray->adv_id; ?>");
    			//-->
    			</script>
    	        <?php }else{ ?>
    			<img src="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" class="" />
    			<?php } ?>
    		</a>
    	</td>
    </tr>
    <?php } ?>
    <?php } ?>
</div>
<div class="bannerRight">
    <?php foreach($advertise as $advertiseArray){ ?>
    <?php if((int)$advertiseArray->adv_position == 8 && trim($advertiseArray->adv_page) != '' && stristr($advertiseArray->adv_page, $advertisePage)){ ?>
    <tr>
    	<td align="center">
    	    <a href="<?php echo prep_url($advertiseArray->adv_link); ?>" target="_blank" title="<?php echo $advertiseArray->adv_title; ?>">
    	        <?php if(strtolower(substr($advertiseArray->adv_banner, -4)) == '.swf'){ ?>
    	        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" class="flash" id="FlashID_<?php echo $advertiseArray->adv_id; ?>" title="<?php echo $advertiseArray->adv_title; ?>">
    			  <param name="movie" value="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" />
    			  <param name="quality" value="high" />
    			  <param name="wmode" value="opaque" />
    			  <param name="swfversion" value="6.0.65.0" />
    			  <param name="expressinstall" value="<?php echo base_url(); ?>templates/home/images/expressInstall.swf" />
    			  <!--[if !IE]>-->
    			  <object type="application/x-shockwave-flash" data="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" class="advertise_left_flash">
    			    <!--<![endif]-->
    			    <param name="quality" value="high" />
    			    <param name="wmode" value="opaque" />
    			    <param name="swfversion" value="6.0.65.0" />
    			    <param name="expressinstall" value="<?php echo base_url(); ?>templates/home/images/expressInstall.swf" />
    			    <div>
    			      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
    			      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
    			    </div>
    			    <!--[if !IE]>-->
    			  </object>
    			  <!--<![endif]-->
    			</object>
    			<script type="text/javascript">
    			<!--
    			swfobject.registerObject("FlashID_<?php echo $advertiseArray->adv_id; ?>");
    			//-->
    			</script>
    	        <?php }else{ ?>
    			<img src="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" class="" />
    			<?php } ?>
    		</a>
    	</td>
    </tr>
    <?php } ?>
    <?php } ?>
</div>
<?php if(count($advertise) > 0){ ?>
<div class="bannerCorner">
    <div class="close" title="Đóng"></div>
    <?php $showClose = false; ?>
    <?php foreach($advertise as $advertiseArray){ ?>
    <?php if((int)$advertiseArray->adv_position == 9 && trim($advertiseArray->adv_page) != '' && stristr($advertiseArray->adv_page, $advertisePage)){ ?>
    <tr>
    	<td align="center">
    	    <a href="<?php echo prep_url($advertiseArray->adv_link); ?>" target="_blank" title="<?php echo $advertiseArray->adv_title; ?>">
    	        <?php if(strtolower(substr($advertiseArray->adv_banner, -4)) == '.swf'){ ?>
    	        <object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" class="flash" id="FlashID_<?php echo $advertiseArray->adv_id; ?>" title="<?php echo $advertiseArray->adv_title; ?>">
    			  <param name="movie" value="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" />
    			  <param name="quality" value="high" />
    			  <param name="wmode" value="opaque" />
    			  <param name="swfversion" value="6.0.65.0" />
    			  <param name="expressinstall" value="<?php echo base_url(); ?>templates/home/images/expressInstall.swf" />
    			  <!--[if !IE]>-->
    			  <object type="application/x-shockwave-flash" data="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" class="advertise_left_flash">
    			    <!--<![endif]-->
    			    <param name="quality" value="high" />
    			    <param name="wmode" value="opaque" />
    			    <param name="swfversion" value="6.0.65.0" />
    			    <param name="expressinstall" value="<?php echo base_url(); ?>templates/home/images/expressInstall.swf" />
    			    <div>
    			      <h4>Content on this page requires a newer version of Adobe Flash Player.</h4>
    			      <p><a href="http://www.adobe.com/go/getflashplayer"><img src="http://www.adobe.com/images/shared/download_buttons/get_flash_player.gif" alt="Get Adobe Flash player" /></a></p>
    			    </div>
    			    <!--[if !IE]>-->
    			  </object>
    			  <!--<![endif]-->
    			</object>
    			<script type="text/javascript">
    			<!--
    			swfobject.registerObject("FlashID_<?php echo $advertiseArray->adv_id; ?>");
    			//-->
    			</script>
    	        <?php }else{ ?>
    			<img src="<?php echo base_url(); ?>media/banners/<?php echo $advertiseArray->adv_dir; ?>/<?php echo $advertiseArray->adv_banner; ?>" class="" />
    			<?php } ?>
    		</a>
    	</td>
    </tr>
    <?php $showClose = true; ?>
    <?php } ?>
    <?php } ?>
</div>
<?php if($showClose){ ?>
<script language="javascript">showCloseBannerCorner();</script>
<?php } ?>
<?php } ?>
<!--BEGIN: Google Analytics-->
<!--END Google Analytics-->
</body>
</html>